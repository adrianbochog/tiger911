import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
// import store from './store'
import configureStore from './store';
const { store, persistor } = configureStore();

import AppNavigation from './src/Navigation';
import Splash from './src/Screens/Splash';

// temporary disable irritating yellow box
// console.disableYellowBox = true;

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          {bootstrapped => <Splash bootstrapped={bootstrapped} />}
        </PersistGate>
      </Provider>
    );
  }
}
