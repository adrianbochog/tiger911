/**
 * Created by adrianperez on 18/03/2018.
 */

const shoe = {
  brand: 'Adidas',
  modelType: 'Ez Yeezy',
  colorVersion: 'Yellow-Pink',
  soldAmount: '123',
  priceAmount: '12345',
};

const bid = {
  brand: 'Adidas',
  modelType: 'Ez Yeezy',
  colorVersion: 'Yellow-Pink',
  marketDetailLabel: 'BID',
  marketDetailAmount: '12,345',
  status: 'TEST-STATUS',
};

const shoeRowData = [shoe, shoe, shoe];

const shoeGridData = [
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
  shoe,
];

const showListData = [
  {
    name: 'Trending',
    rowData: shoeRowData,
  },
  {
    name: 'Just In',
    rowData: shoeRowData,
  },
];

export { shoe, shoeRowData, showListData, shoeGridData, bid };
