import { NavigationActions } from 'react-navigation';

import {
  ACCEPT_BID_ACCEPTED,
  ACCEPT_BID_REJECTED,
  ASK_ACCEPTED,
  ASK_NOW_ACCEPTED,
  ASK_NOW_REJECTED,
  ASK_REJECTED,
  BID_ACCEPTED,
  BID_REJECTED,
  BUY_NOW_ACCEPTED,
  BUY_NOW_REJECTED,
  FETCHING_ALL_VARIANTS_DATA,
  FETCHING_ALL_VARIANTS_DATA_FAILURE,
  FETCHING_ALL_VARIANTS_DATA_SUCCESS,
  FETCHING_VARIANTS_BY_ID,
  FETCHING_VARIANTS_BY_ID_FAILURE,
  FETCHING_VARIANTS_BY_ID_SUCCESS,
  FETCHING_VARIANTS_BY_NAME,
  FETCHING_VARIANTS_BY_NAME_FAILURE,
  FETCHING_VARIANTS_BY_NAME_SUCCESS,
  FETCHING_VARIANTS_BY_COLOR,
  FETCHING_VARIANTS_BY_COLOR_FAILURE,
  FETCHING_VARIANTS_BY_COLOR_SUCCESS,
  FETCHING_VARIANTS_BY_SIZE,
  FETCHING_VARIANTS_BY_SIZE_FAILURE,
  FETCHING_VARIANTS_BY_SIZE_SUCCESS,
  FETCHING_VARIANTS_BY_SHOE_SIZE,
  FETCHING_VARIANTS_BY_SHOE_SIZE_FAILURE,
  FETCHING_VARIANTS_BY_SHOE_SIZE_SUCCESS,
  FETCHING_VARIANTS_BY_MOVEMENT,
  FETCHING_VARIANTS_BY_MOVEMENT_FAILURE,
  FETCHING_VARIANTS_BY_MOVEMENT_SUCCESS,
  FETCHING_VARIANTS_BY_CONDITION,
  FETCHING_VARIANTS_BY_CONDITION_FAILURE,
  FETCHING_VARIANTS_BY_CONDITION_SUCCESS,
  FETCHING_ASK_DATA,
  FETCHING_ASK_DATA_FAILURE,
  FETCHING_ASK_DATA_SUCCESS,
  FETCHING_ASK_HISTORY_DATA,
  FETCHING_ASK_HISTORY_DATA_FAILURE,
  FETCHING_ASK_HISTORY_DATA_SUCCESS,
  FETCHING_BID_DATA,
  FETCHING_BID_DATA_FAILURE,
  FETCHING_BID_DATA_SUCCESS,
  FETCHING_BID_HISTORY_DATA,
  FETCHING_BID_HISTORY_DATA_FAILURE,
  FETCHING_BID_HISTORY_DATA_SUCCESS,
  FETCHING_PENDING_ASK_DATA,
  FETCHING_PENDING_ASK_DATA_FAILURE,
  FETCHING_PENDING_ASK_DATA_SUCCESS,
  FETCHING_USER_BID_DATA,
  FETCHING_USER_BID_DATA_FAILURE,
  FETCHING_USER_BID_DATA_SUCCESS,
  FETCHING_USER_ACCEPTED_BID_DATA,
  FETCHING_USER_ACCEPTED_BID_DATA_FAILURE,
  FETCHING_USER_ACCEPTED_BID_DATA_SUCCESS,
  FETCHING_PRODUCT_BID_DATA,
  FETCHING_PRODUCT_BID_DATA_FAILURE,
  FETCHING_PRODUCT_BID_DATA_SUCCESS,
  FETCHING_PROFILE_DATA,
  FETCHING_PROFILE_DATA_FAILURE,
  FETCHING_PROFILE_DATA_SUCCESS,
  FETCHING_PROFILE_DETAILS,
  FETCHING_PROFILE_DETAILS_FAILURE,
  FETCHING_PROFILE_DETAILS_SUCCESS,
  FETCHING_PAYMENT_DATA,
  FETCHING_PAYMENT_DATA_FAILURE,
  FETCHING_PAYMENT_DATA_SUCCESS,
  FETCHING_SHOE_DATA,
  FETCHING_SHOE_DATA_FAILURE,
  FETCHING_SHOE_DATA_SUCCESS,
  FETCHING_SHOE_SIZE_DATA,
  FETCHING_SHOE_SIZE_DATA_FAILURE,
  FETCHING_SHOE_SIZE_DATA_SUCCESS,
  FETCHING_FAVORITES,
  FETCHING_FAVORITES_SUCCESS,
  FETCHING_FAVORITES_FAILURE,
  FETCHING_SEARCH,
  FETCHING_SEARCH_SUCCESS,
  FETCHING_SEARCH_FAILURE,
  Login,
  LoginSuccess,
  Logout,
  NavigateToLogoutScreen,
  NavigateToProfile,
  NavigateToProfileBuying,
  NavigateToProfileBuyingBids,
  NavigateToProfileBuyingHistory,
  NavigateToProfileBuyingPending,
  Register,
  RegisterPrimary,
  RegisterSecondary,
  RegisterSuccess,
  SENDING_ACCEPT_BID_DATA,
  SENDING_ACCEPT_BID_DATA_FAILURE,
  SENDING_ASK_DATA,
  SENDING_ASK_DATA_FAILURE,
  SENDING_ASK_NOW_DATA,
  SENDING_ASK_NOW_DATA_FAILURE,
  SENDING_BID_DATA,
  SENDING_BID_DATA_FAILURE,
  SENDING_BUY_NOW_DATA,
  SENDING_BUY_NOW_DATA_FAILURE,
  SENDING_LOGIN_DATA,
  SENDING_LOGIN_DATA_FAILURE,
  SENDING_LOGIN_DATA_SUCCESS,
  SENDING_REGISTRATION_DATA,
  SENDING_REGISTRATION_DATA_FAILURE,
  SENDING_REGISTRATION_DATA_SUCCESS,
  SENDING_ADDRESS_DATA,
  SENDING_ADDRESS_DATA_FAILURE,
  SENDING_ADDRESS_DATA_SUCCESS,
  SENDING_PAYMENT_DATA,
  SENDING_PAYMENT_DATA_FAILURE,
  SENDING_PAYMENT_DATA_SUCCESS,
  TOGGLE_BROWSE_FILTER_MODAL,
  UPDATE_BROWSE_FILTER,
  UPDATE_REGISTRATION_DATA_PRIMARY,
  UPDATE_RELEASE_YEAR_BROWSE_FILTER,
  Welcome,
} from './actionTypes';
import * as ProfileService from '../Services/ProfileService';
import * as TransactionService from '../Services/TransactionService';
import * as BrowseService from '../Services/BrowseService';
import { Alert } from 'react-native';

const welcome = () => ({
  type: Welcome,
});

const login = () => ({
  type: Login,
});

const loginSuccess = () => ({
  type: LoginSuccess,
});

const logout = () => ({
  type: Logout,
});

const registerPrimary = () => ({
  type: RegisterPrimary,
});

const registerSecondary = data => ({
  type: RegisterSecondary,
  data,
});

const registerSuccess = () => ({
  type: RegisterSuccess,
});

const navigateToLogoutScreen = () => ({
  type: NavigateToLogoutScreen,
});

const getAllVariantsData = () => ({
  type: FETCHING_ALL_VARIANTS_DATA,
});

const getAllVariantsDataSuccess = data => ({
  type: FETCHING_ALL_VARIANTS_DATA_SUCCESS,
  data,
});

const getAllVariantsDataFailure = () => ({
  type: FETCHING_ALL_VARIANTS_DATA_FAILURE,
  data,
});

export function fetchAllVariantsData(variantsData) {
  return dispatch => {
    dispatch(getAllVariantsData());
    BrowseService.fetchAllVariantsData(variantsData)
      .then(data => {
        dispatch(getAllVariantsDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getVariantsById = () => ({
  type: FETCHING_VARIANTS_BY_ID,
});

const getVariantsByIdSuccess = data => ({
  type: FETCHING_VARIANTS_BY_ID_SUCCESS,
  data,
});

const getVariantsByIdFailure = () => ({
  type: FETCHING_VARIANTS_BY_ID_FAILURE,
  data,
});

export function fetchVariantsDataById(variantsData) {
  return dispatch => {
    dispatch(getVariantsById());
    BrowseService.fetchVariantsDataById(variantsData)
      .then(data => {
        dispatch(getVariantsByIdSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getVariantsByName = () => ({
  type: FETCHING_VARIANTS_BY_NAME,
});

const getVariantsByNameSuccess = data => ({
  type: FETCHING_VARIANTS_BY_NAME_SUCCESS,
  data,
});

const getVariantsByNameFailure = () => ({
  type: FETCHING_VARIANTS_BY_NAME_FAILURE,
  data,
});

export function fetchVariantsDataByName(variantsData) {
  return dispatch => {
    dispatch(getVariantsByName());
    BrowseService.fetchVariantsDataByName(variantsData)
      .then(data => {
        dispatch(getVariantsByNameSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getVariantsByColor = () => ({
  type: FETCHING_VARIANTS_BY_COLOR,
});

const getVariantsByColorSuccess = data => ({
  type: FETCHING_VARIANTS_BY_COLOR_SUCCESS,
  data,
});

const getVariantsByColorFailure = () => ({
  type: FETCHING_VARIANTS_BY_COLOR_FAILURE,
  data,
});

export function fetchVariantsDataByColor(variantsData) {
  return dispatch => {
    dispatch(getVariantsByColor());
    BrowseService.fetchVariantsDataByColor(variantsData)
      .then(data => {
        dispatch(getVariantsByColorSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getVariantsBySize = () => ({
  type: FETCHING_VARIANTS_BY_SIZE,
});

const getVariantsBySizeSuccess = data => ({
  type: FETCHING_VARIANTS_BY_SIZE_SUCCESS,
  data,
});

const getVariantsBySizeFailure = () => ({
  type: FETCHING_VARIANTS_BY_SIZE_FAILURE,
  data,
});

export function fetchVariantsDataBySize(variantsData) {
  return dispatch => {
    dispatch(getVariantsBySize());
    BrowseService.fetchVariantsDataBySize(variantsData)
      .then(data => {
        dispatch(getVariantsBySizeSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getVariantsByShoeSize = () => ({
  type: FETCHING_VARIANTS_BY_SHOE_SIZE,
});

const getVariantsByShoeSizeSuccess = data => ({
  type: FETCHING_VARIANTS_BY_SHOE_SIZE_SUCCESS,
  data,
});

const getVariantsByShoeSizeFailure = () => ({
  type: FETCHING_VARIANTS_BY_SHOE_SIZE_FAILURE,
  data,
});

export function fetchVariantsDataByShoeSize(variantsData) {
  return dispatch => {
    dispatch(getVariantsByShoeSize());
    BrowseService.fetchVariantsDataByShoeSize(variantsData)
      .then(data => {
        dispatch(getVariantsByShoeSizeSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getVariantsByMovement = () => ({
  type: FETCHING_VARIANTS_BY_MOVEMENT,
});

const getVariantsByMovementSuccess = data => ({
  type: FETCHING_VARIANTS_BY_MOVEMENT_SUCCESS,
  data,
});

const getVariantsByMovementFailure = () => ({
  type: FETCHING_VARIANTS_BY_MOVEMENT_FAILURE,
  data,
});

export function fetchVariantsDataByMovement(variantsData) {
  return dispatch => {
    dispatch(getVariantsByMovement());
    BrowseService.fetchVariantsDataByMovement(variantsData)
      .then(data => {
        dispatch(getVariantsByMovementSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getVariantsByCondition = () => ({
  type: FETCHING_VARIANTS_BY_CONDITION,
});

const getVariantsByConditionSuccess = data => ({
  type: FETCHING_VARIANTS_BY_CONDITION_SUCCESS,
  data,
});

const getVariantsByConditionFailure = () => ({
  type: FETCHING_VARIANTS_BY_CONDITION_FAILURE,
  data,
});

export function fetchVariantsDataByCondition(variantsData) {
  return dispatch => {
    dispatch(getVariantsByCondition());
    BrowseService.fetchVariantsDataByCondition(variantsData)
      .then(data => {
        dispatch(getVariantsByConditionSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

export function fetchProfileThenVariantsData(profileData) {
  return dispatch => {
    dispatch(getProfileData());
    ProfileService.fetchProfileData(profileData)
      .then(data => {
        dispatch(getProfileDataSuccess(data));
        return data;
      })
      .then(data => {
        dispatch(getAllVariantsData());
        return { shoeSize: data.shoe_size };
      })
      .then(data => {
        return BrowseService.fetchAllVariantsData(data);
      })
      .then(data => {
        dispatch(getAllVariantsDataSuccess(data));
        return data;
      })
      .catch(err => console.log('err:', err));
  };
}
//
// function getShoeData() {
//   return {
//     type: FETCHING_SHOE_DATA
//   }
// }
//
// function getShoeDataSuccess(data) {
//   return {
//     type: FETCHING_SHOE_DATA_SUCCESS,
//     data,
//   }
// }
//
// function getShoeDataFailure() {
//   return {
//     type: FETCHING_SHOE_DATA_FAILURE
//   }
// }
//
// export function fetchShoeData(id) {
//   return (dispatch) => {
//     dispatch(getShoeData())
//     ShoeService.fetchShoeData(id)
//       .then((data) => {
//         dispatch(getShoeDataSuccess(data))
//       })
//       .catch((err) => console.log('err:', err))
//   }
// }

function sendRegistrationData() {
  return {
    type: SENDING_REGISTRATION_DATA,
  };
}

function sendRegistrationDataSuccess(data) {
  return {
    type: SENDING_REGISTRATION_DATA_SUCCESS,
    data,
  };
}

function sendAddressDataSuccess(data) {
  return {
    type: SENDING_ADDRESS_DATA_SUCCESS,
    data,
  };
}

function sendAddressData() {
  return {
    type: SENDING_ADDRESS_DATA,
  };
}

function sendAddressDataFailure() {
  return {
    type: SENDING_ADDRESS_DATA_FAILURE,
  };
}

function sendPaymentDataSuccess(data) {
  return {
    type: SENDING_PAYMENT_DATA_SUCCESS,
    data,
  };
}

function sendPaymentData() {
  return {
    type: SENDING_PAYMENT_DATA,
  };
}

function sendPaymentDataFailure() {
  return {
    type: SENDING_PAYMENT_DATA_FAILURE,
  };
}

function sendRegistrationDataFailure() {
  return {
    type: SENDING_REGISTRATION_DATA_FAILURE,
  };
}

export function postRegistrationData(registrationData) {
  return dispatch => {
    dispatch(sendRegistrationData());
    ProfileService.postRegistrationData(registrationData)
      .then(data => {
        dispatch(sendRegistrationDataSuccess(data));
        dispatch(
          NavigationActions.navigate({
            routeName: 'registrationSuccess',
          })
        );
      })
      .catch(err => {
        try {
          const errors = Object.values(err || {});
          // got valid error
          const error =
            errors.length > 0
              ? errors[0]
              : 'Invalid registration details';
          const errorMessage = Array.isArray(error)
            ? error[0]
            : Object.values(error)[0][0];
          Alert.alert('Error', errorMessage);
        } catch (e) {
          Alert.alert('Error', 'Invalid registration details');
        }
        console.log('postRegistrationData error:', err);
      });
  };
}

export function putPaymentData(paymentData) {
  return dispatch => {
    dispatch(sendPaymentData());
    ProfileService.putPaymentData(paymentData)
      .then(data => {
        dispatch(sendPaymentDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

export function postPaymentData(paymentData) {
  return dispatch => {
    dispatch(sendPaymentData());
    ProfileService.postPaymentData(paymentData)
      .then(data => {
        dispatch(sendPaymentDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getPaymentData = () => ({
  type: FETCHING_PAYMENT_DATA,
});

const getPaymentDataSuccess = data => ({
  type: FETCHING_PAYMENT_DATA_SUCCESS,
  data,
});

const getPaymentDataFailure = () => ({
  type: FETCHING_PAYMENT_DATA_FAILURE,
  data,
});

export function fetchPaymentData(paymentData) {
  return dispatch => {
    dispatch(getPaymentData());
    ProfileService.fetchPaymentData(paymentData)
      .then(data => {
        dispatch(getPaymentDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

function sendLoginData() {
  return {
    type: SENDING_LOGIN_DATA,
  };
}

export function sendLoginDataSuccess(data) {
  return dispatch => {
    const type = data.token
      ? LoginSuccess
      : SENDING_LOGIN_DATA_SUCCESS;

    dispatch({
      type,
      data,
    });

    if (data.token) {
      dispatch(
        NavigationActions.navigate({ routeName: 'mainScreens' })
      );
    }
  };
}

function sendLoginDataFailure() {
  return {
    type: SENDING_LOGIN_DATA_FAILURE,
  };
}

export function postLoginData(loginData) {
  return dispatch => {
    dispatch(sendLoginData());
    ProfileService.postLoginData(loginData)
      .then(data => {
        dispatch(
          sendLoginDataSuccess({
            ...data,
            token: `Token ${data.token}`,
          })
        );
      })
      .catch(err => {
        dispatch(sendLoginDataFailure());
      })
      .catch(err => {
        console.log('err:', err);
        dispatch(sendLoginDataFailure());
      });
  };
}

export function toggleBrowseFilterModal() {
  return {
    type: TOGGLE_BROWSE_FILTER_MODAL,
  };
}

export function updateBrowseFilter(data) {
  return {
    type: UPDATE_BROWSE_FILTER,
    data,
  };
}

function sendBidData() {
  return {
    type: SENDING_BID_DATA,
  };
}

function sendBidDataSuccess(data) {
  return dispatch => {
    const type = BID_ACCEPTED;

    dispatch({ type, data });
    dispatch(NavigationActions.navigate({ routeName: 'bidSuccess' }));
  };
}

function sendBidDataFailure() {
  return {
    type: SENDING_BID_DATA_FAILURE,
  };
}

export function postBidData(bidData) {
  return dispatch => {
    dispatch(sendBidData());
    TransactionService.postBidData(bidData)
      .then(data => {
        dispatch(sendBidDataSuccess(data));
      })
      .catch(err => {
        const msg = err.msg || "Can't post bid";
        Alert.alert(
          'Error',
          msg,
          [
            {
              text: 'OK',
              onPress: () => dispatch(NavigationActions.pop()),
            },
          ],
          { onDismiss: () => dispatch(NavigationActions.pop()) }
        );
        console.log('postBidData error:', err);
      });
  };
}

function sendAskData() {
  return {
    type: SENDING_ASK_DATA,
  };
}

function sendAskDataSuccess(data) {
  return dispatch => {
    const type = ASK_REJECTED;

    dispatch({ type, data });
    dispatch(NavigationActions.navigate({ routeName: 'askSuccess' }));
  };
}

function sendAskDataFailure() {
  return {
    type: SENDING_ASK_DATA_FAILURE,
  };
}

export function postAskData(askData) {
  return dispatch => {
    dispatch(sendAskData());
    TransactionService.postAskData(askData)
      .then(data => {
        dispatch(sendAskDataSuccess(data));
      })
      .catch(err => {
        Alert.alert(
          'Error',
          "Can't post ask",
          [
            {
              text: 'OK',
              onPress: () => dispatch(NavigationActions.pop()),
            },
          ],
          { onDismiss: () => dispatch(NavigationActions.pop()) }
        );
        console.log('postAskData error:', err);
      });
  };
}

const getAskData = () => ({
  type: FETCHING_ASK_DATA,
});

const getAskDataSuccess = data => ({
  type: FETCHING_ASK_DATA_SUCCESS,
  data,
});

const getAskDataFailure = () => ({
  type: FETCHING_ASK_DATA_FAILURE,
  data,
});

export function fetchAskData(askData) {
  return dispatch => {
    dispatch(getAskData());
    TransactionService.fetchUserAskData(askData)
      .then(data => {
        dispatch(getAskDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getPendingAskData = () => ({
  type: FETCHING_PENDING_ASK_DATA,
});

const getPendingAskDataSuccess = data => ({
  type: FETCHING_PENDING_ASK_DATA_SUCCESS,
  data,
});

const getPendingAskDataFailure = data => ({
  type: FETCHING_PENDING_ASK_DATA_FAILURE,
  data,
});

export function fetchPendingAskData(askData) {
  return dispatch => {
    dispatch(getPendingAskData());
    TransactionService.fetchUserAskData(askData)
      .then(data => {
        dispatch(getPendingAskDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getAskHistoryData = () => ({
  type: FETCHING_ASK_HISTORY_DATA,
});

const getAskHistoryDataSuccess = data => ({
  type: FETCHING_ASK_HISTORY_DATA_SUCCESS,
  data,
});

const getAskHistoryDataFailure = () => ({
  type: FETCHING_ASK_HISTORY_DATA_FAILURE,
  data,
});

export function fetchAskHistoryData(askData) {
  return dispatch => {
    dispatch(getAskHistoryData());
    TransactionService.fetchUserAskData(askData)
      .then(data => {
        dispatch(getAskHistoryDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getBidData = () => ({
  type: FETCHING_BID_DATA,
});

const getBidDataSuccess = data => ({
  type: FETCHING_BID_DATA_SUCCESS,
  data,
});

const getBidDataFailure = () => ({
  type: FETCHING_BID_DATA_FAILURE,
  data,
});

export function fetchBidData(bidData) {
  return dispatch => {
    dispatch(getBidData());
    TransactionService.fetchBidData(bidData)
      .then(data => {
        dispatch(getBidDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getUserBidData = () => ({
  type: FETCHING_USER_BID_DATA,
});

const getUserBidDataSuccess = data => ({
  type: FETCHING_USER_BID_DATA_SUCCESS,
  data,
});

const getUserBidDataFailure = () => ({
  type: FETCHING_USER_BID_DATA_FAILURE,
  data,
});

export function fetchUserBidData(bidData) {
  return dispatch => {
    dispatch(getUserBidData());
    TransactionService.fetchUserBidData(bidData)
      .then(data => {
        dispatch(getUserBidDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getUserAcceptedBidData = () => ({
  type: FETCHING_USER_ACCEPTED_BID_DATA,
});

const getUserAcceptedBidDataSuccess = data => ({
  type: FETCHING_USER_ACCEPTED_BID_DATA_SUCCESS,
  data,
});

const getUserAcceptedBidDataFailure = () => ({
  type: FETCHING_USER_ACCEPTED_BID_DATA_FAILURE,
  data,
});

export function fetchUserAcceptedBidData(bidData) {
  return dispatch => {
    dispatch(getUserAcceptedBidData());
    TransactionService.fetchUserBidData(bidData)
      .then(data => {
        dispatch(getUserAcceptedBidDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getPendingBidData = () => ({
  type: FETCHING_PENDING_BID_DATA,
});

const getPendingBidDataSuccess = data => ({
  type: FETCHING_PENDING_BID_DATA_SUCCESS,
  data,
});

const getPendingBidDataFailure = () => ({
  type: FETCHING_PENDING_BID_DATA_FAILURE,
  data,
});

export function fetchPendingBidData(bidData) {
  return dispatch => {
    dispatch(getPendingBidData());
    TransactionService.fetchBidData(bidData)
      .then(data => {
        dispatch(getPendingBidDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getBidHistoryData = () => ({
  type: FETCHING_BID_HISTORY_DATA,
});

const getBidHistoryDataSuccess = data => ({
  type: FETCHING_BID_HISTORY_DATA_SUCCESS,
  data,
});

const getBidHistoryDataFailure = () => ({
  type: FETCHING_BID_HISTORY_DATA_FAILURE,
  data,
});

export function fetchBidHistoryData(bidData) {
  return dispatch => {
    dispatch(getBidHistoryData());
    TransactionService.fetchBidData(bidData)
      .then(data => {
        dispatch(getBidHistoryDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getProductBidData = () => ({
  type: FETCHING_PRODUCT_BID_DATA,
});

const getProductBidDataSuccess = data => ({
  type: FETCHING_PRODUCT_BID_DATA_SUCCESS,
  data,
});

const getProductBidDataFailure = () => ({
  type: FETCHING_PRODUCT_BID_DATA_FAILURE,
  data,
});

export function fetchProductBidData(bidData) {
  return dispatch => {
    dispatch(getProductBidData());
    TransactionService.fetchUserMatchingBidData(bidData)
      .then(data => {
        dispatch(getProductBidDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

const getProfileData = () => ({
  type: FETCHING_PROFILE_DATA,
});

const getProfileDataSuccess = data => ({
  type: FETCHING_PROFILE_DATA_SUCCESS,
  data,
});

const getProfileDataFailure = () => ({
  type: FETCHING_PROFILE_DATA_FAILURE,
  data,
});

const getProfileDetails = () => ({
  type: FETCHING_PROFILE_DETAILS,
});

const getProfileDetailsSuccess = data => ({
  type: FETCHING_PROFILE_DETAILS_SUCCESS,
  data,
});

const getProfileDetailsFailure = () => ({
  type: FETCHING_PROFILE_DETAILS_FAILURE,
  data,
});

export function fetchProfileData(profileData) {
  return dispatch => {
    dispatch(getProfileData());
    ProfileService.fetchProfileData(profileData)
      .then(data => {
        dispatch(getProfileDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

export function fetchProfileDetails(profileData) {
  return dispatch => {
    dispatch(getProfileDetails());
    ProfileService.fetchProfileDetails(profileData)
      .then(data => {
        dispatch(getProfileDetailsSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

export function fetchProfileAddresses(addressData) {
  return dispatch => {
    dispatch(getProfileData());
    ProfileService.fetchProfileAddresses(addressData)
      .then(data => {
        dispatch(getProfileDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

export function postProfileAddresses(addressData) {
  return dispatch => {
    dispatch(sendAddressData());
    ProfileService.postProfileAddresses(addressData)
      .then(data => {
        dispatch(sendAddressDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

export function putProfileAddresses(addressData) {
  return dispatch => {
    dispatch(sendAddressData());
    ProfileService.putProfileAddresses(addressData)
      .then(data => {
        dispatch(sendAddressDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

function sendBuyNowData() {
  return {
    type: SENDING_BUY_NOW_DATA,
  };
}

function sendBuyNowDataSuccess(data) {
  return dispatch => {
    let type = BUY_NOW_REJECTED;
    if (data.booking_reference) {
      type = BUY_NOW_ACCEPTED;
    }

    dispatch({ type, data });
    dispatch(
      NavigationActions.navigate({ routeName: 'buyNowSuccess' })
    );
  };
}

function sendBuyNowDataFailure() {
  return {
    type: SENDING_BUY_NOW_DATA_FAILURE,
  };
}

export function postBuyNowData(bidData) {
  return dispatch => {
    dispatch(sendBuyNowData());
    TransactionService.postBuyNowData(bidData)
      .then(data => {
        dispatch(sendBuyNowDataSuccess(data));
      })
      .catch(err => {
        const message = err.msg || 'Unable to buy now';
        Alert.alert('Error', err.msg);
      });
  };
}

function sendAskNowData() {
  return {
    type: SENDING_ASK_NOW_DATA,
  };
}

function sendAskNowDataSuccess(data) {
  let type = ASK_NOW_REJECTED;

  if (data.product) {
    type = ASK_NOW_ACCEPTED;
  }

  return {
    type,
    data,
  };
}

function sendAskNowDataFailure() {
  return {
    type: SENDING_ASK_NOW_DATA_FAILURE,
  };
}

export function postAskNowData(bidData) {
  return dispatch => {
    dispatch(sendAskNowData());
    TransactionService.postAskNowData(bidData)
      .then(data => {
        dispatch(sendAskNowDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

function sendAcceptBidData() {
  return {
    type: SENDING_ACCEPT_BID_DATA,
  };
}

function sendAcceptBidDataSuccess(data) {
  let type = ACCEPT_BID_REJECTED;

  if (data.booking_reference) {
    type = ACCEPT_BID_ACCEPTED;
  }

  return {
    type,
    data,
  };
}

function sendAcceptBidDataFailure() {
  return {
    type: SENDING_ACCEPT_BID_DATA_FAILURE,
  };
}

export function postAcceptBidData(bidData) {
  return dispatch => {
    dispatch(sendAcceptBidData());
    TransactionService.postAcceptBidData(bidData)
      .then(data => {
        dispatch(
          fetchPendingAskData({
            status: 'Open',
            token: bidData.token,
            role: 's',
          })
        );
        dispatch(sendAcceptBidDataSuccess(data));
      })
      .catch(err => console.log('err:', err));
  };
}

// export function fetchShoeThenSizeData(id) {
//   return (dispatch) => {
//     dispatch(getShoeData())
//     ShoeService.fetchShoeData(id)
//       .then((data) => {
//         dispatch(getShoeDataSuccess(data))
//         return data
//       })
//       .then((data) => {
//         dispatch(getShoeSizeData())
//         return {name: data.name}
//       })
//       .then((data) => {
//         return ShoeService.fetchBrowseData(data)
//       })
//       .then((data) => {
//         dispatch(getShoeSizeDataSuccess(data))
//         return data
//       })
//       .catch((err) => console.log('err:', err))
//   }
// }

function getFavorites() {
  return {
    type: FETCHING_FAVORITES,
  };
}

function getFavoritesSuccess(favorites) {
  return {
    type: FETCHING_FAVORITES_SUCCESS,
    favorites,
  };
}

function getFavoritesFailure(error) {
  return {
    type: FETCHING_FAVORITES_FAILURE,
    error,
  };
}

export function fetchFavorites(token) {
  return dispatch => {
    dispatch(getFavorites());
    ProfileService.fetchFavorites(token)
      .then(favorites => {
        dispatch(getFavoritesSuccess(favorites));
      })
      .catch(err => console.log(err));
  };
}

export function addToFavorites(favorites, token) {
  return dispatch => {
    ProfileService.putFavorites(favorites, token).then(() => {
      dispatch(fetchFavorites(token));
    });
  };
}

export function removeFromFavorites(favorites, token) {
  return dispatch => {
    ProfileService.removeFavorites(favorites, token).then(() => {
      dispatch(fetchFavorites(token));
    });
  };
}

const getVariantsByQuery = () => ({
  type: FETCHING_SEARCH,
});

const getVariantsByQuerySuccess = data => ({
  type: FETCHING_SEARCH_SUCCESS,
  data,
});

const getVariantsByQueryFailure = data => ({
  type: FETCHING_SEARCH_FAILURE,
  data,
});

export function fetchVariantsDataByQuery(query) {
  return dispatch => {
    dispatch(getVariantsByQuery());
    BrowseService.fetchVariantsDataByQuery(query)
      .then(data => {
        dispatch(getVariantsByQuerySuccess(data));
      })
      .catch(err => {
        dispatch(getVariantsByQueryFailure(err));
        console.log('err:', err);
      });
  };
}

export function fbLoginSuccesss(data) {
  return dispatch => {
    ProfileService.fetchProfileData({ token: data.token }).then(
      profile => {
        if (profile.shoe_size && profile.user && profile.user.email) {
          dispatch(sendLoginDataSuccess(data));
        } else {
          dispatch(
            NavigationActions.navigate({
              routeName: 'emailShoeSize',
              params: {
                token: data.token,
              },
            })
          );
        }
      }
    );
  };
}

export {
  welcome,
  login,
  loginSuccess,
  logout,
  registerPrimary,
  registerSecondary,
  registerSuccess,
  navigateToLogoutScreen,
};
