/**
 * Created by adrianperez on 25/04/2018.
 */
import {
  SENDING_BID_DATA,
  SENDING_BID_DATA_FAILURE,
  SENDING_BID_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: {},
  dataSent: false,
  isSending: false,
  error: false,
};

const bidReducer = (state = initialState, action) => {
  switch (action.type) {
    case SENDING_BID_DATA:
      return {
        ...state,
        data: {},
        isFetching: true,
      };
    case SENDING_BID_DATA_SUCCESS:
      return {
        ...state,
        dataSent: true,
        isFetching: false,
        data: action.data,
      };
    case SENDING_BID_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default bidReducer;
