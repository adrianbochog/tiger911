/**
 * Created by adrianperez on 10/05/2018.
 */
import {
  SENDING_ACCEPT_BID_DATA,
  SENDING_ACCEPT_BID_DATA_FAILURE,
  SENDING_ACCEPT_BID_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: {},
  dataSent: false,
  isSending: false,
  error: false,
};

const acceptBidReducer = (state = initialState, action) => {
  switch (action.type) {
    case SENDING_ACCEPT_BID_DATA:
      return {
        ...state,
        data: {},
        isFetching: true,
      };
    case SENDING_ACCEPT_BID_DATA_SUCCESS:
      return {
        ...state,
        dataSent: true,
        isFetching: false,
        data: action.data,
      };
    case SENDING_ACCEPT_BID_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default acceptBidReducer;
