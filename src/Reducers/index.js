import { combineReducers } from 'redux';
import NavigationReducer from './navigationReducer';
import BrowseDataReducer from './browseDataReducer';
import ShoeDataReducer from './shoeDataReducer';
import RegistrationReducer from './registrationReducer';
import BrowseDataFilterReducer from './browseDataFilterReducer';
import AskDataReducer from './askDataReducer';
import PendingAskDataReducer from './pendingAskDataReducer';
import AskHistoryDataReducer from './askHistoryDataReducer';
import BidDataReducer from './bidDataReducer';
import PendingBidDataReducer from './pendingBidDataReducer';
import BidHistoryDataReducer from './bidHistoryDataReducer';
import ProductBidDataReducer from './productBidDataReducer';
import ProfileDataReducer from './profileDataReducer';
import BuyNowReducer from './buyNowReducer';
import AcceptBidReducer from './acceptBidReducer';
import ShoeSizeDataReducer from './shoeSizeDataReducer';
import PaymentDataReducer from './paymentDataReducer';

const AppReducer = combineReducers({
  NavigationReducer,
  BrowseDataReducer,
  ShoeDataReducer,
  RegistrationReducer,
  BrowseDataFilterReducer,
  AskDataReducer,
  BidDataReducer,
  PendingBidDataReducer,
  BidHistoryDataReducer,
  PendingAskDataReducer,
  AskHistoryDataReducer,
  ProductBidDataReducer,
  ProfileDataReducer,
  BuyNowReducer,
  AcceptBidReducer,
  ShoeSizeDataReducer,
  PaymentDataReducer,
});

export default AppReducer;
