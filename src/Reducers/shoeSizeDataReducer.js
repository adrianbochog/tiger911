/**
 * Created by adrianperez on 12/04/2018.
 */

import {
  FETCHING_SHOE_SIZE_DATA,
  FETCHING_SHOE_SIZE_DATA_FAILURE,
  FETCHING_SHOE_SIZE_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false,
};

const shoeSizeDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_SHOE_SIZE_DATA:
      return {
        ...state,
        data: [],
        isFetching: true,
      };
    case FETCHING_SHOE_SIZE_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
      };
    case FETCHING_SHOE_SIZE_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default shoeSizeDataReducer;
