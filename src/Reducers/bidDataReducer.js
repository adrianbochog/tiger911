import {
  FETCHING_BID_DATA,
  FETCHING_BID_DATA_FAILURE,
  FETCHING_BID_DATA_SUCCESS,
  FETCHING_USER_ACCEPTED_BID_DATA,
  FETCHING_USER_ACCEPTED_BID_DATA_FAILURE,
  FETCHING_USER_ACCEPTED_BID_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false,
  accepted: {
    data: [],
    dataFetched: false,
    isFetching: false,
    error: false,
  },
};

const bidDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_BID_DATA:
      return {
        ...state,
        data: [],
        isFetching: true,
      };
    case FETCHING_BID_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
      };
    case FETCHING_BID_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case FETCHING_USER_ACCEPTED_BID_DATA:
      return {
        ...state,
        accepted: {
          ...state.accepted,
          data: [],
          isFetching: true,
        },
      };
    case FETCHING_USER_ACCEPTED_BID_DATA_SUCCESS:
      return {
        ...state,
        accepted: {
          ...state.accepted,
          isFetching: false,
          data: action.data,
        },
      };
    case FETCHING_USER_ACCEPTED_BID_DATA_FAILURE:
      return {
        ...state,
        accepted: {
          ...state.accepted,
          isFetching: false,
          error: true,
        },
      };
    default:
      return state;
  }
};

export default bidDataReducer;
