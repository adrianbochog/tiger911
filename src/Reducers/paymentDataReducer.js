/**
 * Created by jeunesseburce on 15/06/2018.
 */
import {
  FETCHING_PAYMENT_DATA,
  FETCHING_PAYMENT_DATA_FAILURE,
  FETCHING_PAYMENT_DATA_SUCCESS,
  SENDING_PAYMENT_DATA,
  SENDING_PAYMENT_DATA_FAILURE,
  SENDING_PAYMENT_DATA_SUCCESS,
} from '../Actions/actionTypes';

const initialState = {
  data: {},
  primaryData: {},
  dataSent: false,
  isSending: false,
  isFetched: false,
  error: false,
};

const paymentDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_PAYMENT_DATA:
      return {
        ...state,
        data: null,
        isFetching: true,
      };
    case FETCHING_PAYMENT_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isFetched: true,
        data: action.data,
      };
    case FETCHING_PAYMENT_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case SENDING_PAYMENT_DATA:
      return {
        ...state,
        data: {},
        isFetching: true,
      };
    case SENDING_PAYMENT_DATA_SUCCESS:
      return {
        ...state,
        dataSent: true,
        isFetching: false,
        data: action.data,
      };
    case SENDING_PAYMENT_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default paymentDataReducer;
