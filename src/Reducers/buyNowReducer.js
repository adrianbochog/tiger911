/**
 * Created by adrianperez on 25/04/2018.
 */
import {
  SENDING_BUY_NOW_DATA,
  SENDING_BUY_NOW_DATA_FAILURE,
  SENDING_BUY_NOW_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: {},
  dataSent: false,
  isSending: false,
  error: false,
};

const buyNowReducer = (state = initialState, action) => {
  switch (action.type) {
    case SENDING_BUY_NOW_DATA:
      return {
        ...state,
        data: {},
        isFetching: true,
      };
    case SENDING_BUY_NOW_DATA_SUCCESS:
      return {
        ...state,
        dataSent: true,
        isFetching: false,
        data: action.data,
      };
    case SENDING_BUY_NOW_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default buyNowReducer;
