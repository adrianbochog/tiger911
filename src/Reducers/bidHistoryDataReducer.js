import {
  FETCHING_BID_HISTORY_DATA,
  FETCHING_BID_HISTORY_DATA_FAILURE,
  FETCHING_BID_HISTORY_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false,
};

const bidHistoryDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_BID_HISTORY_DATA:
      return {
        ...state,
        data: [],
        isFetching: true,
      };
    case FETCHING_BID_HISTORY_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
      };
    case FETCHING_BID_HISTORY_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default bidHistoryDataReducer;
