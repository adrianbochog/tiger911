import { NavigationActions } from 'react-navigation';
import AppNavigator from '../Navigation/navigationStack';

const { router } = AppNavigator;

const initialState = router.getStateForAction(
  NavigationActions.navigate({ routeName: 'welcomeScreens' })
);

const navReducer = (state = initialState, action) => {
  const { routeName, type } = action;

  const newState = router.getStateForAction(action, state);

  return newState || state;
};

export default navReducer;
