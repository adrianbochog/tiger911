import {
  LoginSuccess,
  Logout,
  RegisterSuccess,
  SENDING_LOGIN_DATA,
  SENDING_LOGIN_DATA_FAILURE,
  SENDING_LOGIN_DATA_SUCCESS,
} from '../Actions/actionTypes';

const initialState = {
  isSending: false,
  isLoggedIn: false,
  token: '',
  isError: false,
  errorMessages: [],
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LoginSuccess:
      return {
        ...state,
        isLoggedIn: true,
        isSending: false,
        token: action.data.token,
        isError: false,
        errorMessage: [],
      };

    case Logout:
      return { ...state, isLoggedIn: false, token: '' };

    case RegisterSuccess:
      return { ...state, isLoggedIn: false };

    case SENDING_LOGIN_DATA:
      return {
        ...state,
        isSending: true,
        errorMessages: [],
        isError: false,
      };

    case SENDING_LOGIN_DATA_SUCCESS:
      const response = action.data;
      const isLoggedIn = false;
      let errorMessages = action.data.non_field_errors;

      if (errorMessages === null || errorMessages.length === 0) {
        errorMessages = ['Login Error'];
      }

      return {
        ...state,
        isLoggedIn,
        isSending: false,
        isError: true,
        errorMessages,
      };

    case SENDING_LOGIN_DATA_FAILURE:
      return { ...state, isSending: false };

    default:
      return state;
  }
};

export default loginReducer;
