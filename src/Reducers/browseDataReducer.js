/**
 * Created by adrianperez on 12/04/2018.
 */

import {
  FETCHING_BROWSE_DATA,
  FETCHING_BROWSE_DATA_FAILURE,
  FETCHING_BROWSE_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false,
};

const browseDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_BROWSE_DATA:
      return {
        ...state,
        data: [],
        isFetching: true,
      };
    case FETCHING_BROWSE_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
      };
    case FETCHING_BROWSE_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default browseDataReducer;
