import {
  FETCHING_PRODUCT_BID_DATA,
  FETCHING_PRODUCT_BID_DATA_FAILURE,
  FETCHING_PRODUCT_BID_DATA_SUCCESS,
  ACCEPT_BID_ACCEPTED,
  ACCEPT_BID_REJECTED,
} from '../Actions/actionTypes';
const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false,
  visible: false,
};

const productBidDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_PRODUCT_BID_DATA:
      return {
        ...state,
        data: [],
        isFetching: true,
        visible: true,
      };
    case FETCHING_PRODUCT_BID_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
      };
    case FETCHING_PRODUCT_BID_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case ACCEPT_BID_ACCEPTED:
      return {
        ...state,
        visible: false,
      };
    case ACCEPT_BID_REJECTED:
      return {
        ...state,
        visible: false,
      };
    default:
      return state;
  }
};

export default productBidDataReducer;
