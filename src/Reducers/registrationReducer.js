/**
 * Created by adrianperez on 18/04/2018.
 */
import {
  RegisterSecondary,
  SENDING_REGISTRATION_DATA,
  SENDING_REGISTRATION_DATA_FAILURE,
  SENDING_REGISTRATION_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: {},
  primaryData: {},
  dataSent: false,
  isSending: false,
  error: false,
};

const registrationReducer = (state = initialState, action) => {
  switch (action.type) {
    case SENDING_REGISTRATION_DATA:
      return {
        ...state,
        data: {},
        isFetching: true,
      };
    case SENDING_REGISTRATION_DATA_SUCCESS:
      return {
        ...state,
        dataSent: true,
        isFetching: false,
        data: action.data,
      };
    case SENDING_REGISTRATION_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case RegisterSecondary:
      return {
        ...state,
        primaryData: action.data,
      };
    default:
      return state;
  }
};

export default registrationReducer;
