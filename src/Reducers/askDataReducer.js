import {
  FETCHING_ASK_DATA,
  FETCHING_ASK_DATA_FAILURE,
  FETCHING_ASK_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false,
};

const askDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_ASK_DATA:
      return {
        ...state,
        data: [],
        isFetching: true,
      };
    case FETCHING_ASK_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
      };
    case FETCHING_ASK_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default askDataReducer;
