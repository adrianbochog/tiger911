/**
 * Created by adrianperez on 21/04/2018.
 */
import {
  UPDATE_BROWSE_FILTER,
  TOGGLE_BROWSE_FILTER_MODAL,
} from '../Actions/actionTypes';

const initialState = {
  isModalVisible: false,
  filter: {
    shoeBrandFilter: [],
    shoeStyleFilter: [],
    shoeSizeFilter: [],
    releasedYearFilter: [],
  },
};

const browseDataFilterReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_BROWSE_FILTER_MODAL:
      return {
        ...state,
        isModalVisible: !state.isModalVisible,
      };
    case UPDATE_BROWSE_FILTER: {
      return {
        ...state,
        filter: action.data,
      };
    }
    default:
      return state;
  }
};

export default browseDataFilterReducer;
