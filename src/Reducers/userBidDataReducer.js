import {
  FETCHING_USER_BID_DATA,
  FETCHING_USER_BID_DATA_FAILURE,
  FETCHING_USER_BID_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false,
};

const userBidDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_USER_BID_DATA:
      return {
        ...state,
        data: [],
        isFetching: true,
      };
    case FETCHING_USER_BID_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
      };
    case FETCHING_USER_BID_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default userBidDataReducer;
