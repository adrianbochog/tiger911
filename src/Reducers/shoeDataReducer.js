/**
 * Created by adrianperez on 15/04/2018.
 */
import {
  FETCHING_VARIANTS_BY_ID,
  FETCHING_VARIANTS_BY_ID_FAILURE,
  FETCHING_VARIANTS_BY_ID_SUCCESS,
  FETCHING_SEARCH,
  FETCHING_SEARCH_SUCCESS,
  FETCHING_SEARCH_FAILURE,
} from '../Actions/actionTypes';

const initialState = {
  data: {},
  dataFetched: false,
  isFetching: false,
  error: false,
  search: {
    isFetching: false,
    error: null,
    success: false,
    data: [],
  },
};

const shoeDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_VARIANTS_BY_ID:
      return {
        ...state,
        data: {},
        isFetching: true,
      };
    case FETCHING_VARIANTS_BY_ID_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
      };
    case FETCHING_VARIANTS_BY_ID_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    case FETCHING_SEARCH:
      return {
        ...state,
        search: {
          ...state.search,
          isFetching: true,
          data: [],
        },
      };
    case FETCHING_SEARCH_SUCCESS:
      return {
        ...state,
        search: {
          ...state.search,
          isFetching: false,
          success: true,
          error: null,
          data: action.data,
        },
      };
    case FETCHING_SEARCH_FAILURE:
      return {
        ...state,
        search: {
          ...state.search,
          isFetching: false,
          success: false,
          error: action.data,
          data: [],
        },
      };
    default:
      return state;
  }
};

export default shoeDataReducer;
