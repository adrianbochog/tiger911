/**
 * Created by adrianperez on 08/05/2018.
 */
/**
 * Created by adrianperez on 12/04/2018.
 */

import {
  FETCHING_PROFILE_DATA,
  FETCHING_PROFILE_DATA_FAILURE,
  FETCHING_PROFILE_DATA_SUCCESS,
  FETCHING_PROFILE_DETAILS,
  FETCHING_PROFILE_DETAILS_FAILURE,
  FETCHING_PROFILE_DETAILS_SUCCESS,
  FETCHING_FAVORITES,
  FETCHING_FAVORITES_SUCCESS,
  FETCHING_FAVORITES_FAILURE,
} from '../Actions/actionTypes';
const initialState = {
  data: null,
  userDetails: null,
  userDetailsFetched: false,
  isFetchingUserDetails: false,
  dataFetched: false,
  isFetching: false,
  error: false,
  favorites: {
    isFetching: false,
    data: [],
    error: null,
  },
};

const profileDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_PROFILE_DATA:
      return {
        ...state,
        data: null,
        isFetching: true,
      };
    case FETCHING_PROFILE_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
        dataFetched: true,
      };
    case FETCHING_PROFILE_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCHING_PROFILE_DETAILS:
      return {
        ...state,
        userDetails: null,
        isFetchingUserDetails: true,
      };
    case FETCHING_PROFILE_DETAILS_SUCCESS:
      return {
        ...state,
        isFetchingUserDetails: false,
        userDetails: action.data,
        userDetailsFetched: true,
      };
    case FETCHING_PROFILE_DETAILS_FAILURE:
      return {
        ...state,
        isFetchingUserDetails: false,
        error: true,
      };
    case FETCHING_FAVORITES:
      return {
        ...state,
        favorites: {
          ...state.favorites,
          isFetching: true,
          error: null,
        },
      };
    case FETCHING_FAVORITES_SUCCESS:
      return {
        ...state,
        favorites: {
          ...state.favorites,
          isFetching: false,
          data: action.favorites,
          error: null,
        },
      };
    case FETCHING_FAVORITES_FAILURE:
      return {
        ...state,
        favorites: {
          ...state.favorites,
          isFetching: false,
          error: action.error,
        },
      };
    default:
      return state;
  }
};

export default profileDataReducer;
