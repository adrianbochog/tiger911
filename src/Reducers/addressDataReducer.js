/**
 * Created by jeunesseburce on 15/06/2018.
 */
import {
  SENDING_ADDRESS_DATA,
  SENDING_ADDRESS_DATA_FAILURE,
  SENDING_ADDRESS_DATA_SUCCESS,
} from '../Actions/actionTypes';
const initialState = {
  data: {},
  primaryData: {},
  dataSent: false,
  isSending: false,
  error: false,
};

const addressDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case SENDING_ADDRESS_DATA:
      return {
        ...state,
        data: {},
        isFetching: true,
      };
    case SENDING_ADDRESS_DATA_SUCCESS:
      return {
        ...state,
        dataSent: true,
        isFetching: false,
        data: action.data,
      };
    case SENDING_ADDRESS_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };
    default:
      return state;
  }
};

export default addressDataReducer;
