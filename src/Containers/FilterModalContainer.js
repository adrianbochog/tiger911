/**
 * Created by adrianperez on 21/04/2018.
 */
import React, { Component } from 'react';
import {
  Text,
  TouchableHighlight,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Slider,
} from 'react-native';
import { connect } from 'react-redux';
import {
  toggleBrowseFilterModal,
  updateBrowseFilter,
} from '../Actions/actionCreator';
import Modal from 'react-native-modal';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';
import BrandAndStyleFilterSelector from '../Components/BrowseFilter/BrandAndStyleFilterSelector';
import ReleaseYearFilterSelector from '../Components/BrowseFilter/ReleaseYearFilterSelector';
import ShoeSizeFilterSelector from '../Components/BrowseFilter/ShoeSizeFilterSelector';
import * as BrowseFilterData from '../Config/Data/BrowseFilterData';

class FilterModalContainer extends Component {
  initialState = {
    shoeBrandFilter: [],
    shoeStyleFilter: [],
    shoeSizeFilter: [],
    releasedYearFilter: [],
  };

  resetFilters = () => {
    this.setState(this.initialState);
    this.props.updateBrowseFilter(this.state);
    this.props.toggleBrowseFilterModal();
  };

  constructor(props) {
    super(props);
    this.state = {
      shoeBrandFilter: this.props.browseFilterData.filter
        .shoeBrandFilter,
      shoeStyleFilter: this.props.browseFilterData.filter
        .shoeStyleFilter,
      shoeSizeFilter: this.props.browseFilterData.filter
        .shoeSizeFilter,
      releasedYearFilter: this.props.browseFilterData.filter
        .releasedYearFilter,
    };
  }

  shoeStyleToggleAdd = value => {
    const newFilter = this.state.shoeStyleFilter;
    newFilter.push(value);
    this.setState({
      ...this.state,
      shoeStyleFilter: newFilter,
    });
  };

  shoeStyleToggleRemove = value => {
    const newFilter = this.state.shoeStyleFilter;
    const index = newFilter.indexOf(value);
    if (index !== -1) {
      newFilter.splice(index, 1);
    }

    this.setState({
      ...this.state,
      shoeStyleFilter: newFilter,
    });
  };

  shoeBrandToggleAdd = value => {
    const newFilter = this.state.shoeBrandFilter;
    newFilter.push(value);
    this.setState({
      ...this.state,
      shoeBrandFilter: newFilter,
    });
  };

  shoeBrandToggleRemove = value => {
    const newFilter = this.state.shoeBrandFilter;
    const index = newFilter.indexOf(value);
    if (index !== -1) {
      newFilter.splice(index, 1);
    }

    this.setState({
      ...this.state,
      shoeBrandFilter: newFilter,
    });
  };

  shoeSizeToggleAdd = value => {
    const newFilter = this.state.shoeSizeFilter;
    newFilter.push(value);
    this.setState({
      ...this.state,
      shoeSizeFilter: newFilter,
    });
  };

  shoeSizeToggleRemove = value => {
    const newFilter = this.state.shoeSizeFilter;
    const index = newFilter.indexOf(value);
    if (index !== -1) {
      newFilter.splice(index, 1);
    }

    this.setState({
      ...this.state,
      shoeSizeFilter: newFilter,
    });
  };

  releaseYearToggleAdd = value => {
    const newFilter = this.state.releasedYearFilter;
    newFilter.push(value);
    this.setState({
      ...this.state,
      releasedYearFilter: newFilter,
    });
  };

  releaseYearToggleRemove = value => {
    const newFilter = this.state.releasedYearFilter;
    const index = newFilter.indexOf(value);
    if (index !== -1) {
      newFilter.splice(index, 1);
    }

    this.setState({
      ...this.state,
      releasedYearFilter: newFilter,
    });
  };

  updateFilters = () => {
    this.props.updateBrowseFilter(this.state);
  };

  onClickApplyButton = () => {
    this.updateFilters();
    this.props.toggleBrowseFilterModal();
  };

  render() {
    return (
      <Modal
        animationType="fade"
        transparent
        isVisible={this.props.browseFilterData.isModalVisible}
        backdropOpacity={0.6}
        backdropColor={RhypeColors.darkBlue}>
        <View style={styles.container}>
          <View>
            <Text style={styles.headerText}>FILTER/SORT</Text>
            <BrandAndStyleFilterSelector
              data={BrowseFilterData.brandAndModelData}
              brandFilter={this.state.shoeBrandFilter}
              styleFilter={this.state.shoeStyleFilter}
              shoeBrandToggleAdd={this.shoeBrandToggleAdd}
              shoeBrandToggleRemove={this.shoeBrandToggleRemove}
              shoeStyleToggleAdd={this.shoeStyleToggleAdd}
              shoeStyleToggleRemove={this.shoeStyleToggleRemove}
            />
            <ShoeSizeFilterSelector
              data={BrowseFilterData.shoeSizeData}
              filter={this.state.shoeSizeFilter}
              toggleAdd={this.shoeSizeToggleAdd}
              toggleRemove={this.shoeSizeToggleRemove}
            />
            <ReleaseYearFilterSelector
              data={BrowseFilterData.yearData}
              filter={this.state.releasedYearFilter}
              toggleAdd={this.releaseYearToggleAdd}
              toggleRemove={this.releaseYearToggleRemove}
            />
            <View style={styles.buttonContainer}>
              <TouchableHighlight
                style={styles.resetButton}
                onPress={() => {
                  this.resetFilters();
                }}>
                <Text style={styles.settingsButtonText}>RESET</Text>
              </TouchableHighlight>
              <TouchableHighlight
                style={styles.applyButton}
                onPress={() => {
                  this.onClickApplyButton();
                }}>
                <Text style={styles.settingsButtonText}>APPLY</Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: RhypeColors.white,
    padding: 10,
    borderRadius: 15,
  },
  headerText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 36,
    color: RhypeColors.darkBlue,
  },

  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    marginTop: 20,
    marginBottom: 5,
  },
  resetButton: {
    flex: 1,
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'center',
    alignContent: 'center',
    paddingVertical: 6,
    borderRadius: 8,
    marginHorizontal: 4,
  },
  applyButton: {
    flex: 1,
    backgroundColor: RhypeColors.copper,
    justifyContent: 'center',
    alignContent: 'center',
    paddingVertical: 6,
    borderRadius: 8,
    marginHorizontal: 4,
  },
  settingsButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 24,
    color: RhypeColors.white,
    alignSelf: 'center',
  },
});

const mapStateToProps = state => ({
  browseFilterData: state.BrowseDataFilterReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    toggleBrowseFilterModal: () =>
      dispatch(toggleBrowseFilterModal()),
    updateBrowseFilter: data => dispatch(updateBrowseFilter(data)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilterModalContainer);
