/**
 * Created by adrianperez on 08/05/2018.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  View,
} from 'react-native';
import { SearchBar } from 'react-native-elements';
import { fetchProfileData } from '../Services/ProfileService';
import {
  fetchProfileData as fetchProfile,
  fetchVariantsDataByQuery,
} from '../Actions/actionCreator';
import {
  fetchAllVariantsData,
  fetchVariantsDataByShoeSize,
} from '../Services/BrowseService';
import ProfileShoeItem from '../Components/Shoe/ProfileShoeItem';
import * as RhypeColors from '../Commons/RhypeColors';

class SearchContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      isRefreshing: false,
      shoeSize: null,
      searchString: '',
      query: '',
    };

    this.loadProducts = this.loadProducts.bind(this);
  }

  componentDidMount() {
    // this.fetchData()
    // this.props.fetchAllVariantsData()
    fetchProfileData({
      token: this.props.loginData.token,
    }).then(data => {
      this.setState({ shoeSize: data.shoe_size }, () => {
        this.loadProducts('');
      });
    });
  }

  onRefresh() {
    this.props.fetchProfile({
      token: this.props.loginData.token,
    });
  }

  loadProducts(query) {
    this.setState({ query }, () => {
      const { shoeSize } = this.state;
      const token = this.props.loginData.token;
      this.props.fetchVariantsDataByQuery({
        search: query,
        shoeSize,
        token,
      });
    });
  }

  render() {
    const { query, shoeSize } = this.state;
    const { loginData, variants } = this.props;
    const { token } = loginData;
    const { data, isFetching, success } = variants;
    // console.log("SearchContainer::render() data: ", data)
    // const fetchedData = searchString ? data.filter(item => item.name.toUpperCase().includes(searchString.toUpperCase())) : data;
    if (token != null && shoeSize != null) {
      let containedViews;
      if (
        !isFetching &&
        success &&
        data != null &&
        data.length >= 0
      ) {
        containedViews = (
          <FlatList
            data={data}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('shoeDetail', {
                    itemId: item.id,
                  })
                }>
                <ProfileShoeItem
                  brand={item.product.brand.name}
                  modelType={item.product.name}
                  colorVersion={item.color}
                  status={item.status}
                  marketDetailLabel="PRICE"
                  marketDetailAmount={item.retail_price}
                  imageUri={item.images.image}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => '' + item.id}
            onRefresh={() => this.onRefresh()}
            refreshing={isFetching}
          />
        );
      } else {
        containedViews = (
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        );
      }
      return (
        <View style={{ backgroundColor: 'white', flex: 1 }}>
          <SearchBar
            lightTheme
            placeholder="Type Here..."
            onChangeText={this.loadProducts}
          />
          {containedViews}
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'white',
          }}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }
  }
}

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  // browseData: state.BrowseDataReducer,
  variants: state.ShoeDataReducer.search,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchProfile: data => dispatch(fetchProfile(data)),
    fetchAllVariantsData: () => dispatch(fetchAllVariantsData()),
    fetchVariantsDataByQuery: data =>
      dispatch(fetchVariantsDataByQuery(data)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchContainer);
