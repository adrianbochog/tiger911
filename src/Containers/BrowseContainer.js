/**
 * Created by adrianperez on 07/04/2018.
 */
import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ShoeCard from '../Components/Shoe/ShoeCard';
import { connect } from 'react-redux';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';
import { NavigationActions } from 'react-navigation';
import FilterModalContainer from './FilterModalContainer';
import { fetchProfileData } from '../Services/ProfileService';
import { fetchVariantsDataByShoeSize } from '../Services/BrowseService';
import { fetchProfileData as fetchProfile } from '../Actions/actionCreator';

class BrowseContainer extends Component {
  state = {
    data: null,
    isRefreshing: false,
    shoeSize: null,
  };

  componentDidMount() {
    this.fetchData();
    this.props.fetchProfile({
      token: this.props.loginData.token,
    });
  }

  fetchData() {
    fetchProfileData({
      token: this.props.loginData.token,
    })
      .then(data => {
        return { shoeSize: data.shoe_size };
      })
      .then(data => {
        this.setState({
          isRefreshing: true,
          shoeSize: data.shoeSize,
        });
        return data;
      })
      .then(data => {
        this.onRefresh();
      });
  }

  onRefresh() {
    this.setState({
      isRefreshing: true,
    });
    fetchVariantsDataByShoeSize({
      shoeSize: this.state.shoeSize,
    }).then(products => {
      this.setState({
        data: products,
        isRefreshing: false,
      });
    });
  }

  navigate = id => {
    const navigateToScreen = NavigationActions.navigate({
      routeName: 'shoeDetail',
      params: {
        itemId: id,
      },
    });
    this.props.navigation.dispatch(navigateToScreen);
  };

  render() {
    const { data, isRefreshing } = this.state;
    if (data && !isRefreshing) {
      return (
        <View style={styles.container}>
          <FilterModalContainer {...this.props} />
          <View style={styles.header}>
            <Text style={styles.headerText}>BROWSE</Text>
            {/* <TouchableOpacity */}
            {/* onPress={() => { */}
            {/* this.props.toggleBrowseFilterModal() */}
            {/* }}> */}
            {/* > */}
            {/* <Icon name="filter-list" type="MaterialIcons" size={36} color={RhypeColors.copper}/> */}
            {/* </TouchableOpacity> */}
          </View>
          <FlatList
            columnWrapperStyle={{ justifyContent: 'space-evenly' }}
            numColumns={3}
            data={data.results}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => this.navigate(item.id)}>
                <ShoeCard
                  brand={item.product.brand.name}
                  modelType={item.product.name}
                  colorVersion={item.color}
                  priceAmount={item.retail_price}
                  imageUri={item.images.image}
                  soldAmount={item.sold}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => item.id}
            onRefresh={() => this.onRefresh()}
            refreshing={isRefreshing}
          />
        </View>
        // <View style={{
        //   flex: 1,
        //   justifyContent: 'center',
        //   backgroundColor: 'white'
        // }}>
        //   <ActivityIndicator size="large" color={RhypeColors.darkBlue}/>
        // </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'white',
          }}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
    paddingHorizontal: 5,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 36,
    color: RhypeColors.darkBlue,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchProfile: data => dispatch(fetchProfile(data)),
    // toggleBrowseFilterModal: () => dispatch(toggleBrowseFilterModal())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BrowseContainer);
