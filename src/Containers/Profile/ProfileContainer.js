/**
 * Created by adrianperez on 30/03/2018.
 */
import React, { Component } from 'react';
import {
  ActivityIndicator,
  Alert,
  KeyboardAvoidingView,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import Picker from 'react-native-picker';
import Separator from '../../Components/Profile/Account/Separator';
import AccountDetailsSection from '../../Components/Profile/Account/AccountDetailsSection';
import AddressDetailsSection from '../../Components/Profile/Account/AddressDetailsSection';
import PaymentDetailsSection from '../../Components/Profile/Account/PaymentDetailsSection';
import {
  fetchPaymentData,
  fetchProfileDetails,
} from '../../Actions/actionCreator';
import * as ProfileService from '../../Services/ProfileService';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';

const paymentOptions = ['CARD', 'PAYPAL'];

class ProfileContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editModalVisible: false,
      firstName: '',
      lastName: '',
      contactNumber: '',
      email: '',
    };
  }

  toggleAccountModal = () => {
    this.setState({
      editModalVisible: !this.state.editModalVisible,
    });
  };

  updateAccount = () => {
    const account = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      contactNumber: this.state.contactNumber,
      email: this.state.email,
      token: this.props.loginData.token,
    };

    ProfileService.updateAccountDetails(account)
      .then(() => {
        this.toggleAccountModal();
        this.props.fetchProfileDetails({
          token: this.props.loginData.token,
        });
      })
      .catch(err => {
        Alert.alert('Error', "Can't update profile");
      });
  };

  componentDidMount() {
    this.props.fetchProfileDetails({
      token: this.props.loginData.token,
    });

    this.props.fetchPaymentData({
      token: this.props.loginData.token,
    });
  }

  componentWillReceiveProps(props) {
    const {
      userDetailsFetched,
      userDetails,
      isFetchingUserDetails,
    } = props.userDetails;

    if (userDetailsFetched && !isFetchingUserDetails) {
      const { user, contact_number: contactNumber } = userDetails;
      const {
        first_name: firstName,
        last_name: lastName,
        email,
      } = user;

      this.setState({
        firstName,
        lastName,
        contactNumber,
        email,
      });
    }
  }

  renderAccountDetailsSection = () => {
    const {
      userDetailsFetched,
      userDetails,
      isFetchingUserDetails,
    } = this.props.userDetails;

    if (userDetailsFetched && !isFetchingUserDetails) {
      const {
        user,
        contact_number: contactNumber,
        addresses,
      } = userDetails;
      const {
        first_name: firstName,
        last_name: lastName,
        username,
        email,
      } = user;

      return (
        <AccountDetailsSection
          firstName={firstName}
          lastName={lastName}
          username={username}
          email={email}
          contactNumber={contactNumber}
          toggleModal={this.toggleAccountModal}
          addresses={addresses}
        />
      );
    } else {
      return (
        <ActivityIndicator
          size="large"
          color={RhypeColors.darkBlue}
        />
      );
    }
  };

  isBillingAddress = address => {
    return address.name === 'BILLING';
  };

  isShippingAddress = address => {
    return address.name === 'SHIPPING';
  };

  renderBillingAddressSection = () => {
    const {
      userDetailsFetched,
      isFetchingUserDetails,
      userDetails,
    } = this.props.userDetails;

    if (!userDetailsFetched || isFetchingUserDetails) {
      return null;
    }

    const { addresses } = userDetails;

    const billing = addresses.find(this.isBillingAddress);

    if (billing != null) {
      const {
        particulars,
        street,
        area,
        city,
        region,
        zip_code: zipCode,
      } = billing;

      return (
        <AddressDetailsSection
          label="BILLING"
          particulars={particulars}
          street={street}
          area={area}
          city={city}
          region={region}
          zipCode={zipCode}
          token={this.props.loginData.token}
          navigation={this.props.navigation}
          onSuccess={() => {
            this.props.navigation.pop();
            this.props.fetchProfileDetails({
              token: this.props.loginData.token,
            });
          }}
        />
      );
    }
    return (
      <TouchableOpacity
        style={{ padding: 8 }}
        onPress={() =>
          this.props.navigation.navigate('billingAddress')
        }>
        <Text
          style={{
            ...fonts.UniformCondensed.bold,
            fontSize: 18,
            color: RhypeColors.darkBlue,
          }}>
          Add Billing Address
        </Text>
      </TouchableOpacity>
    );
  };

  renderShippingAddressSection = () => {
    const {
      userDetailsFetched,
      isFetchingUserDetails,
      userDetails,
    } = this.props.userDetails;

    if (!userDetailsFetched || isFetchingUserDetails) {
      return null;
    }

    const { addresses } = userDetails;

    const billing = addresses.find(this.isShippingAddress);

    if (billing != null) {
      const {
        particulars,
        street,
        area,
        city,
        region,
        zip_code: zipCode,
      } = billing;

      return (
        <AddressDetailsSection
          label="SHIPPING"
          particulars={particulars}
          street={street}
          area={area}
          city={city}
          region={region}
          zipCode={zipCode}
          token={this.props.loginData.token}
          navigation={this.props.navigation}
          onSuccess={() => {
            this.props.navigation.pop();
            this.props.fetchProfileDetails({
              token: this.props.loginData.token,
            });
          }}
        />
      );
    }
    return (
      <TouchableOpacity
        style={{ padding: 8 }}
        onPress={() =>
          this.props.navigation.navigate('shippingAddress')
        }>
        <Text
          style={{
            ...fonts.UniformCondensed.bold,
            fontSize: 18,
            color: RhypeColors.darkBlue,
          }}>
          Add Shipping Address
        </Text>
      </TouchableOpacity>
    );
  };

  renderPaymentDetailsSection = () => {
    const {
      userDetailsFetched,
      isFetchingUserDetails,
      userDetails,
    } = this.props.userDetails;

    if (!userDetailsFetched || isFetchingUserDetails) {
      return null;
    }

    const bankDetails = userDetails.bank_details;
    if (bankDetails == null) {
      return (
        <TouchableOpacity
          style={{ padding: 8 }}
          onPress={() =>
            this.props.navigation.navigate('paymentDetails')
          }>
          <Text
            style={{
              ...fonts.UniformCondensed.bold,
              fontSize: 18,
              color: RhypeColors.darkBlue,
            }}>
            Add Payment Details
          </Text>
        </TouchableOpacity>
      );
    }

    return (
      <PaymentDetailsSection
        onPress={() =>
          this.props.navigation.navigate('paymentDetails')
        }
        data={bankDetails}
      />
    );
  };

  updateModalState = field => value => {
    const obj = {};
    obj[field] = value;
    this.setState(obj);
  };

  renderModal = () => {
    let modalContent = (
      <ActivityIndicator size="large" color={RhypeColors.darkBlue} />
    );

    const {
      userDetailsFetched,
      userDetails,
      isFetchingUserDetails,
    } = this.props.userDetails;

    const { firstName, lastName, contactNumber, email } = this.state;

    if (userDetailsFetched && !isFetchingUserDetails) {
      modalContent = (
        <View>
          <View style={styles.contentWrapper}>
            <View style={styles.row}>
              <View style={styles.column}>
                <Text style={styles.label}>FIRST NAME</Text>
                <TextInput
                  style={styles.value}
                  value={firstName}
                  autoCorrect={false}
                  onChangeText={this.updateModalState('firstName')}
                />
              </View>
              <View style={styles.column}>
                <Text style={styles.label}>LAST NAME</Text>
                <TextInput
                  style={styles.value}
                  value={lastName}
                  autoCorrect={false}
                  onChangeText={this.updateModalState('lastName')}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.column}>
                <Text style={styles.label}>CONTACT NUMBER</Text>
                <TextInput
                  style={styles.value}
                  value={contactNumber}
                  autoCorrect={false}
                  onChangeText={this.updateModalState(
                    'contactNumber'
                  )}
                />
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.column}>
                <Text style={styles.label}>EMAIL</Text>
                <TextInput
                  style={styles.value}
                  value={email}
                  autoCorrect={false}
                  onChangeText={this.updateModalState('email')}
                />
              </View>
            </View>
          </View>
        </View>
      );
    }
    return (
      <Modal
        animationType="slide"
        visible={this.state.editModalVisible}
        onRequestClose={() => {
          alert('Are you sure you want to exit?');
        }}>
        <KeyboardAvoidingView
          style={styles.container}
          behavior="padding"
          enabled>
          <View style={styles.header}>
            <Text style={styles.headerText}>Edit Profile</Text>
          </View>
          <View style={styles.content}>{modalContent}</View>
          <View style={styles.footer}>
            <TouchableOpacity
              style={styles.closeButton}
              onPress={() => this.toggleAccountModal()}>
              <Text style={styles.buttonText}>CLOSE</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.updateButton}
              onPress={() => this.updateAccount()}>
              <Text style={styles.buttonText}>UPDATE</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  };

  render() {
    const accountDetailSection = this.renderAccountDetailsSection();
    const paymentDetailSection = this.renderPaymentDetailsSection();
    const billingDetailSection = this.renderBillingAddressSection();
    const shippingDetailSection = this.renderShippingAddressSection();
    const modal = this.renderModal();

    const { showPaymentOptions } = this.state;
    const userDetails = this.props.userDetails.userDetails || {};
    const { payment_preference: paymentOption } = userDetails;

    return (
      <View style={styles.root}>
        {modal}
        <ScrollView style={styles.scroll}>
          <Separator />
          {accountDetailSection}
          <Separator />
          {paymentOption == null ? (
            <ActivityIndicator />
          ) : (
            <TouchableOpacity
              onPress={() => {
                Picker.init({
                  pickerData: paymentOptions,
                  selectedValue: [paymentOption],
                  pickerTitleText: 'Select Payment Option',
                  onPickerConfirm: ([opt]) => {
                    this.setState({ paymentOption: opt }, () => {
                      ProfileService.updateAccount({
                        token: this.props.loginData.token,
                        payment_preference: opt,
                      }).then(() => {
                        this.props.fetchProfileDetails({
                          token: this.props.loginData.token,
                        });
                      });
                    });
                  },
                });
                Picker.show();
              }}>
              <View style={styles.singleRow}>
                <Text style={styles.selectorLabel}>
                  Payment Option
                </Text>
                <Text style={styles.value}>{paymentOption}</Text>
              </View>
            </TouchableOpacity>
          )}
          <Separator />
          {billingDetailSection}
          <Separator />
          {shippingDetailSection}
          <Separator />
          {paymentDetailSection}
          <Separator />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: RhypeColors.white,
  },
  scroll: {
    paddingTop: 5,
  },

  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.darkBlue,
  },
  headerText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 24,
    color: RhypeColors.white,
    marginTop: 20,
    paddingVertical: 10,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    height: 40,
  },
  closeButton: {
    backgroundColor: RhypeColors.copper,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  updateButton: {
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  buttonText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
    color: RhypeColors.white,
  },
  contentWrapper: {
    marginHorizontal: 40,
    marginVertical: 10,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
  },
  column: {
    margin: 5,
    flex: 1,
  },
  rowItemContent: {},
  label: {
    ...fonts.UniformCondensed.bold,
    fontSize: 14,
    color: RhypeColors.darkBlue,
  },
  value: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.copper,
  },
  selectorLabel: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.darkBlue,
  },
  singleRow: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  userDetails: state.ProfileDataReducer,
  paymentData: state.PaymentDataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchProfileDetails: userDetails =>
      dispatch(fetchProfileDetails(userDetails)),
    fetchPaymentData: paymentData =>
      dispatch(fetchPaymentData(paymentData)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileContainer);
