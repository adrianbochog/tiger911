/**
 * Created by adrianperez on 05/04/2018.
 */
import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  View,
} from 'react-native';
import ProfileShoeItem from '../../../Components/Shoe/ProfileShoeItem';
import { connect } from 'react-redux';
import * as RhypeColors from '../../../Commons/RhypeColors';
import { fetchAskHistoryData } from '../../../Actions/actionCreator';

class SellingHistoryContainer extends Component {
  componentDidMount() {
    this.props.fetchAskData({
      token: this.props.loginData.token,
      role: 's',
    });
  }

  onRefresh() {
    this.props.fetchAskData({
      token: this.props.loginData.token,
      role: 's',
    });
  }

  render() {
    const { askData } = this.props;
    const dataAsk = askData.data;
    if (!askData.isFetching && askData.data) {
      return (
        <View style={styles.container}>
          <FlatList
            data={dataAsk}
            renderItem={({ item }) => (
              <ProfileShoeItem
                brand={item.variant.product.brand.name}
                modelType={item.variant.product.name}
                colorVersion={item.variant.product.color}
                status={item.status}
                marketDetailLabel="ASK"
                marketDetailAmount={item.price}
                imageUri={
                  item.variant.images && item.variant.images.preview
                }
              />
            )}
            keyExtractor={(item, index) => item.id.toString()}
            onRefresh={() => this.onRefresh()}
            refreshing={this.props.askData.isFetching}
          />
        </View>
      );
    } else {
      return (
        <View
          style={[styles.container, { justifyContent: 'center' }]}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  askData: state.AskHistoryDataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchAskData: askData => dispatch(fetchAskHistoryData(askData)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SellingHistoryContainer);
