/**
 * Created by adrianperez on 05/04/2018.
 */
import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ProfileShoeItem from '../../../Components/Shoe/ProfileShoeItem';
import { connect } from 'react-redux';
import * as RhypeColors from '../../../Commons/RhypeColors';
import fonts from '../../../Commons/RhypeFonts';
import {
  fetchPendingAskData,
  fetchProductBidData,
  postAcceptBidData,
} from '../../../Actions/actionCreator';
import Modal from 'react-native-modal';

class SellingPendingContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      selectedAsk: null,
    };
  }

  toggleModal = () => {
    const isVisible = this.state.isModalVisible;
    this.setState({
      isModalVisible: !isVisible,
    });
  };

  openModal = ask => {
    this.setState({
      isModalVisible: true,
      selectedAsk: ask,
    });

    this.props.fetchProductBidData({
      id: ask.variant.id,
      token: this.props.loginData.token,
    });
  };

  componentDidMount() {
    this.props.fetchAskData({
      status: 'Open',
      token: this.props.loginData.token,
      role: 's',
    });
  }

  onRefresh() {
    this.props.fetchAskData({
      status: 'Open',
      token: this.props.loginData.token,
      role: 's',
    });
  }

  onModalRefresh = () => {
    this.props.fetchProductBidData({
      id: this.state.selectedAsk.variant.id,
      token: this.props.loginData.token,
    });
  };

  acceptBid = bidData => {
    this.props.postAcceptBidData({
      ask: this.state.selectedAsk.id,
      bid: bidData.id,
      token: this.props.loginData.token,
    });
  };

  render() {
    const { askData } = this.props;
    const dataAsk = askData.data;
    const { productBidData } = this.props;
    const products = productBidData.data || [];
    const sortedProducts = products.sort(
      (a, b) => Number(a.price) - Number(b.price)
    );

    if (!askData.isFetching && askData.data) {
      return (
        <View style={styles.container}>
          <FlatList
            data={dataAsk}
            renderItem={({ item }) => (
              <TouchableOpacity onPress={() => this.openModal(item)}>
                <ProfileShoeItem
                  brand={item.variant.product.brand.name}
                  modelType={item.variant.product.name}
                  colorVersion={item.variant.color}
                  status={item.status}
                  marketDetailLabel="ASK"
                  marketDetailAmount={item.price}
                  viewLabel="VIEW BIDS"
                  imageUri={
                    item.variant.images && item.variant.images.preview
                  }
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => item.id.toString()}
            onRefresh={() => this.onRefresh()}
            refreshing={this.props.askData.isFetching}
          />
          <Modal
            isVisible={
              this.state.isModalVisible &&
              this.props.productBidData.visible
            }>
            <View style={styles.modalContainer}>
              <View style={styles.headerWrapper}>
                <Text style={styles.productName}>
                  {this.state.selectedAsk
                    ? this.state.selectedAsk.variant.product.name.toUpperCase()
                    : 'PRODUCT'}
                </Text>
                <Text style={styles.colorway}>
                  {this.state.selectedAsk
                    ? this.state.selectedAsk.variant.color.toUpperCase()
                    : 'COLORWAY'}
                </Text>
              </View>
              <View style={styles.contentWrapper}>
                <FlatList
                  data={sortedProducts}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      style={styles.rowButton}
                      onPress={() =>
                        this.acceptBid({ id: item.id, ...item })
                      }>
                      <View style={styles.rowItem}>
                        <Text style={styles.bidAmount}>
                          {item.price}
                        </Text>
                        <Text style={styles.acceptBidLabel}>
                          ACCEPT BID
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                  keyExtractor={(item, index) => item.id.toString()}
                  onRefresh={() => this.onModalRefresh()}
                  refreshing={this.props.productBidData.isFetching}
                />
              </View>
              <View style={styles.footerWrapper}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => this.toggleModal()}>
                  <Text style={styles.buttonText}>DONE</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>
      );
    } else {
      return (
        <View
          style={[styles.container, { justifyContent: 'center' }]}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
  },
  headerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.copper,
  },
  contentWrapper: {
    flex: 8,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  footerWrapper: {
    flex: 1,
  },
  button: {
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  buttonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 20,
    color: RhypeColors.white,
  },
  productName: {
    ...fonts.UniformCondensed.bold,
    fontSize: 20,
    color: 'white',
  },
  colorway: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
    color: RhypeColors.white,
  },

  rowButton: {
    height: 30,
  },
  rowItem: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'space-between',
  },
  bidAmount: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.darkBlue,
  },
  bidLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 14,
    color: RhypeColors.darkBlue,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  askData: state.PendingAskDataReducer,
  productBidData: state.ProductBidDataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchAskData: askData => dispatch(fetchPendingAskData(askData)),
    fetchProductBidData: bidData =>
      dispatch(fetchProductBidData(bidData)),
    postAcceptBidData: bidData =>
      dispatch(postAcceptBidData(bidData)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SellingPendingContainer);
