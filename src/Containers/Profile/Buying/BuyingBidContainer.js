/**
 * Created by adrianperez on 05/04/2018.
 */
import React, { Component } from 'react';
import {
  ActivityIndicator,
  Alert,
  FlatList,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Modal,
  WebView,
} from 'react-native';

import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';

import ProfileShoeItem from '../../../Components/Shoe/ProfileShoeItem';
import * as RhypeColors from '../../../Commons/RhypeColors';
import {
  fetchUserAcceptedBidData,
  fetchProfileDetails,
} from '../../../Actions/actionCreator';
import { PaymentOptionPicker } from '../../../PaymentOptions';
import {
  fetchPaymentDetails,
  postPayment,
} from '../../../Services/TransactionService';

class BuyingBidContainer extends Component {
  state = {
    selectedAsk: null,
    buySource: null,
  };

  componentDidMount() {
    const { token } = this.props.loginData;
    this.props.fetchUserBidData({
      token,
      status: 'Accepted',
      role: 'b',
    });

    this.props.fetchProfileDetails({ token });
  }

  onRefresh() {
    this.props.fetchUserBidData({
      status: 'Accepted',
      token: this.props.loginData.token,
      role: 'b',
    });
  }

  render() {
    const { bidData, userDetails } = this.props;
    const data = bidData.data;
    const { buySource } = this.state;
    const { token } = this.props.loginData;

    if (
      data.isFetching ||
      data == null ||
      userDetails.userDetails == null
    ) {
      return (
        <View
          style={[styles.container, { justifyContent: 'center' }]}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }

    const defaultPaymentOption =
      userDetails.userDetails.payment_preference;

    console.log(buySource);
    return (
      <View style={styles.container}>
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                PaymentOptionPicker(defaultPaymentOption).then(
                  async paymentOption => {
                    const buySource = await postPayment({
                      token,
                      paymentOption,
                      paymentId: item.transaction.id,
                    });
                    this.setState({ buySource });
                  }
                )
              }>
              <ProfileShoeItem
                brand={item.variant.product.brand.name}
                modelType={item.variant.product.name}
                colorVersion={item.variant.product.color}
                status={item.status}
                marketDetailLabel="BID"
                marketDetailAmount={item.price}
                imageUri={
                  item.variant.images && item.variant.images.preview
                }
              />
            </TouchableOpacity>
          )}
          keyExtractor={(item, index) => item.id.toString()}
          onRefresh={() => this.onRefresh()}
          refreshing={this.props.bidData.isFetching}
        />
        <Modal
          animationType="slide"
          visible={buySource != null}
          onRequestClose={() => {}}>
          <SafeAreaView
            style={{ backgroundColor: RhypeColors.darkBlue }}>
            <View
              style={{
                backgroundColor: RhypeColors.darkBlue,
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ buySource: null }, () =>
                    setTimeout(() => {
                      Alert.alert('', 'Payment cancelled.');
                    }, 1000)
                  );
                }}
                style={{ padding: 4 }}>
                <Icon
                  name="chevron-left"
                  color={RhypeColors.copper}
                  size={34}
                />
              </TouchableOpacity>
            </View>
          </SafeAreaView>
          {buySource && (
            <WebView
              onNavigationStateChange={e => {
                if (e.url === 'http://uat.rhype.ph/') {
                  this.setState({ buySource: null });
                  setTimeout(() => {
                    Alert.alert('', 'Payment successful.');
                  }, 1000);
                }
              }}
              source={buySource}
            />
          )}
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  bidData: state.BidDataReducer.accepted,
  userDetails: state.ProfileDataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchUserBidData: bidData =>
      dispatch(fetchUserAcceptedBidData(bidData)),
    fetchProfileDetails: userDetails =>
      dispatch(fetchProfileDetails(userDetails)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuyingBidContainer);
