/**
 * Created by adrianperez on 05/04/2018.
 */
import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  View,
} from 'react-native';
import ProfileShoeItem from '../../../Components/Shoe/ProfileShoeItem';
import { connect } from 'react-redux';
import * as RhypeColors from '../../../Commons/RhypeColors';
import { fetchUserBidData } from '../../../Actions/actionCreator';

class BuyingPendingContainer extends Component {
  componentDidMount() {
    this.props.fetchUserBidData({
      token: this.props.loginData.token,
      status: 'Active',
    });
  }

  onRefresh() {
    this.props.fetchUserBidData({
      token: this.props.loginData.token,
      status: 'Active',
    });
  }

  render() {
    const { bidData } = this.props;
    const data = bidData.data;

    if (!bidData.isFetching && bidData) {
      return (
        <View style={styles.container}>
          <FlatList
            data={data}
            renderItem={({ item }) => (
              <ProfileShoeItem
                brand={item.variant.product.brand.name}
                modelType={item.variant.product.name}
                colorVersion={item.variant.product.color}
                status={item.status}
                marketDetailLabel="BID"
                marketDetailAmount={item.price}
                imageUri={
                  item.variant.images && item.variant.images.preview
                }
              />
            )}
            keyExtractor={(item, index) => item.id.toString()}
            onRefresh={() => this.onRefresh()}
            refreshing={this.props.bidData.isFetching}
          />
        </View>
      );
    } else {
      return (
        <View
          style={[styles.container, { justifyContent: 'center' }]}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  bidData: state.UserBidDataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchUserBidData: bidData => dispatch(fetchUserBidData(bidData)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuyingPendingContainer);
