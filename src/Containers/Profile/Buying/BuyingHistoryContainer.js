/**
 * Created by adrianperez on 05/04/2018.
 */
import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  ScrollView,
  Text,
  View,
  StyleSheet,
} from 'react-native';
import ProfileShoeItem from '../../../Components/Shoe/ProfileShoeItem';
import { connect } from 'react-redux';
import * as RhypeColors from '../../../Commons/RhypeColors';
import { fetchUserBidData } from '../../../Actions/actionCreator';

class BuyingHistoryContainer extends Component {
  componentDidMount() {
    this.props.fetchUserBidData({
      status: 'Closed',
      token: this.props.loginData.token,
      role: 'b',
    });
  }

  onRefresh() {
    this.props.fetchUserBidData({
      status: 'Closed',
      token: this.props.loginData.token,
      role: 'b',
    });
  }

  render() {
    const { bidData } = this.props;
    const data = bidData.data.bidding_prices;
    if (!bidData.isFetching && bidData.data) {
      return (
        <View style={styles.container}>
          <FlatList
            data={data}
            renderItem={({ item }) => (
              <ProfileShoeItem
                brand={item.variant.product.brand.name}
                modelType={item.variant.product.name}
                colorVersion={item.variant.product.color}
                status={item.status}
                marketDetailLabel="BID"
                marketDetailAmount={
                  item.variant.market_details.ask.lowest
                }
                imageUri={
                  item.variant.images && item.variant.images.preview
                }
              />
            )}
            keyExtractor={(item, index) => item.id.toString()}
            onRefresh={() => this.onRefresh()}
            refreshing={this.props.bidData.isFetching}
          />
        </View>
      );
    } else {
      return (
        <View
          style={[styles.container, { justifyContent: 'center' }]}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  bidData: state.BidDataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchUserBidData: bidData => dispatch(fetchUserBidData(bidData)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuyingHistoryContainer);
