import React, { Component } from 'react';
import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';
import ShoeConfirmationProductDetailSection from '../../Components/Shoe/ShoeConfirmationProductDetailSection';
import ShoeConfirmationFinancialSection from '../../Components/Shoe/ShoeConfirmationFinancialSection';
import {
  fetchVariantsDataById,
  fetchAskData,
  fetchBidData,
  fetchProfileData,
  postBidData,
  postBuyNowData,
} from '../../Actions/actionCreator';
import { connect } from 'react-redux';
import * as RhypeColors from '../../Commons/RhypeColors';
import { NavigationActions } from 'react-navigation';

class ShoeBidConfirmationContainer extends Component {
  componentDidMount() {
    const { params } = this.props.navigation.state;
    const productId = params ? params.id : null;

    this.props.fetchProfileData({
      token: this.props.loginData.token,
    });

    this.props.fetchVariantsDataById({
      id: productId,
      token: this.props.loginData.token,
    });
  }

  getLowest = array => {
    if (array.length > 0) {
      return array.reduce((l, e) => (e.price < l.price ? e : l));
    } else {
      return null;
    }
  };

  getHighest = array => {
    if (array.length > 0) {
      return array.reduce((l, e) => (e.price > l.price ? e : l));
    } else {
      return null;
    }
  };

  submitBid = bidData => {
    this.props.postBidData({
      variantId: bidData.variantId,
      price: bidData.price,
      duration: bidData.duration,
      token: this.props.loginData.token,
    });
  };

  submitBuyNow = ask => {
    this.props.postBuyNowData({
      variant: ask.variant,
      token: this.props.loginData.token,
    });
  };

  submitSellNow = ask => {};

  navigate = (value, params) => {
    if (value === 'Shipping') {
      const navigateToShippingAddressScreen = NavigationActions.navigate(
        {
          routeName: 'shippingAddress',
        }
      );
      this.props.navigation.dispatch(navigateToShippingAddressScreen);
    } else if (value === 'Payment') {
      const navigateToPaymentMethodScreen = NavigationActions.navigate(
        {
          routeName: 'paymentMethod',
        }
      );
      this.props.navigation.dispatch(navigateToPaymentMethodScreen);
    }
  };

  openBidModal = () => {};

  getDeliveryAddress = () => {
    this.props.fetchProfileData({
      token: this.props.loginData.token,
    });
    return this.props.userData.data.delivery_address;
  };

  render() {
    const { params } = this.props.navigation.state;

    const { data, isFetching, error } = this.props.shoeData;
    const user = this.props.userData.data;
    if (error || isFetching || data == null || user == null) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }

    const {
      id,
      color,
      product,
      images,
      market_details: marketDetails,
    } = data;
    const { image } = images;
    const { name, brand } = product;
    const brandName = brand.name;
    const lowestAsk = marketDetails.ask.lowest;
    const highestBid = marketDetails.bid.highest;

    // console.log('props', this.props)

    // const lowestAsk = this.getLowest(this.props.askData.data)
    // const highestBid = this.getHighest(this.props.bidData.data)

    return (
      <ScrollView style={styles.container}>
        <View style={styles.productDetailsContainer}>
          <ShoeConfirmationProductDetailSection
            id={id}
            brand={brandName}
            name={name}
            colorway={color}
            lowestAsk={lowestAsk}
            highestBid={highestBid}
            imageUri={image}
            // retailPrice={params.retailPrice}
          />
        </View>
        <View style={styles.financialDetailsContainer}>
          <ShoeConfirmationFinancialSection
            id={id}
            submitBid={this.submitBid}
            submitBuyNow={this.submitBuyNow}
            submitSellNow={this.submitSellNow}
            lowestAsk={lowestAsk}
            navigate={this.navigate}
            navigation={this.props.navigation}
            openBidModal={this.openBidModal}
            getDeliveryAddress={this.getDeliveryAddress}
            user={user}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
  },
  productDetailsContainer: {
    flex: 1,
  },
  financialDetailsContainer: {
    flex: 2,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  bidData: state.BidDataReducer,
  askData: state.AskDataReducer,
  userData: state.ProfileDataReducer,
  shoeData: state.ShoeDataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    postBidData: data => dispatch(postBidData(data)),
    postBuyNowData: data => dispatch(postBuyNowData(data)),
    fetchBidData: bidData => dispatch(fetchBidData(bidData)),
    fetchAskData: askData => dispatch(fetchAskData(askData)),
    fetchProfileData: userData =>
      dispatch(fetchProfileData(userData)),
    fetchVariantsDataById: data =>
      dispatch(fetchVariantsDataById(data)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoeBidConfirmationContainer);
