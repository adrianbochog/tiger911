/**
 * Created by adrianperez on 18/04/2018.
 */
import React, { Component } from 'react';
import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';
import ShoeConfirmationProductDetailSection from '../../Components/Shoe/ShoeConfirmationProductDetailSection';
import { connect } from 'react-redux';
import {
  fetchAskData,
  fetchBidData,
  fetchProfileData,
  fetchPaymentData,
  postAcceptBidData,
  postAskData,
  fetchVariantsDataById,
} from '../../Actions/actionCreator';
import ShoeAskConfirmationFinancialSection from '../../Components/Shoe/ShoeAskConfirmationFinancialSection';
import * as RhypeColors from '../../Commons/RhypeColors';
import { NavigationActions } from 'react-navigation';

class ShoeAskConfirmationContainer extends Component {
  componentDidMount() {
    const { params } = this.props.navigation.state;
    const productId = params ? params.id : null;

    this.props.fetchAskData({
      status: 'Open',
      token: this.props.loginData.token,
      productId,
    });

    this.props.fetchAskData({
      status: 'Open',
      token: this.props.loginData.token,
      productId,
    });

    this.props.fetchProfileData({
      token: this.props.loginData.token,
    });

    this.props.fetchPaymentData({
      token: this.props.loginData.token,
    });

    this.props.fetchVariantsDataById({
      id: productId,
      token: this.props.loginData.token,
    });
  }

  getLowest = array => {
    if (array.length > 0) {
      return array.reduce((l, e) => (e.price < l.price ? e : l));
    } else {
      return null;
    }
  };

  getHighest = array => {
    if (array.length > 0) {
      return array.reduce((l, e) => (e.price > l.price ? e : l));
    } else {
      return null;
    }
  };

  validateRequest = data => {
    if (
      data.shippingAddress === null &&
      data.paymentMethod === null
    ) {
      this.setState({ requestComplete: false });
    }
  };

  submitAsk = askData => {
    this.props.postAskData({
      variantId: askData.variantId,
      price: askData.price,
      duration: askData.duration,
      token: this.props.loginData.token,
    });
  };

  acceptBid = bidData => {
    this.props.postAcceptBidData({
      bidId: bidData.id,
      token: this.props.loginData.token,
    });
  };

  navigate = value => {
    if (value === 'Shipping') {
      const navigateToShippingAddressScreen = NavigationActions.navigate(
        {
          routeName: 'shippingAddress',
        }
      );
      this.props.navigation.dispatch(navigateToShippingAddressScreen);
    } else if (value === 'Payment') {
      const navigateToPaymentMethodScreen = NavigationActions.navigate(
        {
          routeName: 'paymentMethod',
        }
      );
      this.props.navigation.dispatch(navigateToPaymentMethodScreen);
    }
  };

  render() {
    const { params } = this.props.navigation.state;
    const bankAccount = this.props.paymentData;
    const user = this.props.userData && this.props.userData.data;
    const lowestAsk = this.getLowest(this.props.askData.data);
    const highestBid = this.getHighest(this.props.bidData.data);
    const paymentMethod = bankAccount || '';

    const { data, isFetching, error } = this.props.shoeData;
    if (error || isFetching || data == null) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }

    const { id, color, product, images } = data;
    const { image } = images;
    const { name, brand } = product;
    const brandName = brand.name;

    return (
      <ScrollView style={styles.container}>
        <View style={styles.productDetailsContainer}>
          <ShoeConfirmationProductDetailSection
            id={id}
            brand={brandName}
            name={name}
            colorway={color}
            lowestAsk={lowestAsk ? lowestAsk.price : null}
            highestBid={highestBid ? highestBid.price : null}
            imageUri={image}
            // retailPrice={params.retailPrice}
          />
        </View>
        <View style={styles.financialDetailsContainer}>
          <ShoeAskConfirmationFinancialSection
            id={id}
            submitAsk={this.submitAsk}
            acceptBid={this.acceptBid}
            highestBid={highestBid}
            navigate={this.navigate}
            navigation={this.props.navigation}
            paymentMethod={paymentMethod}
            token={this.props.loginData.token}
            user={user}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
  },
  productDetailsContainer: {
    flex: 1,
  },
  financialDetailsContainer: {
    flex: 2,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  bidData: state.BidDataReducer,
  askData: state.AskDataReducer,
  userData: state.ProfileDataReducer,
  paymentData: state.PaymentDataReducer,
  shoeData: state.ShoeDataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    postAskData: data => dispatch(postAskData(data)),
    postAcceptBidData: data => dispatch(postAcceptBidData(data)),
    fetchBidData: bidData => dispatch(fetchBidData(bidData)),
    fetchAskData: askData => dispatch(fetchAskData(askData)),
    fetchProfileData: userData =>
      dispatch(fetchProfileData(userData)),
    fetchPaymentData: paymentData =>
      dispatch(fetchPaymentData(paymentData)),
    fetchVariantsDataById: data =>
      dispatch(fetchVariantsDataById(data)),
    // fetchProductBidData: (bidData) => dispatch(fetchProductBidData(bidData))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoeAskConfirmationContainer);
