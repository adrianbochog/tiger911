/**
 * Created by adrianperez on 07/04/2018.
 */
import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';
import { connect } from 'react-redux';
import { fetchBidData } from '../../Services/TransactionService';
import { fetchProfileData } from '../../Services/ProfileService';
import ShoeBidCard from '../../Components/Shoe/ShoeBidCard';

class RecentBidsContainer extends Component {
  state = {
    data: null,
    isRefreshing: false,
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetchProfileData({ token: this.props.loginData.token })
      .then(data => {
        return fetchBidData({ shoeSize: data.shoe_size });
      })
      .then(data => {
        const sorted = data.bidding_prices.sort(
          (a, b) => Date.parse(b.modified) - Date.parse(a.modified)
        );
        this.setState({
          data: sorted,
          isRefreshing: false,
        });
      });
  }

  onRefresh() {
    this.fetchData();
  }

  navigate = itemId => {
    const navigateToScreen = NavigationActions.navigate({
      routeName: 'shoeDetail',
      params: {
        itemId,
      },
    });
    this.props.navigation.dispatch(navigateToScreen);
  };

  render() {
    const data = this.state.data;

    if (data) {
      return (
        <View style={styles.container}>
          <FlatList
            horizontal
            data={data}
            initialNumToRender={4}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={styles.cardButton}
                onPress={() => this.navigate(item.variant.id)}>
                <ShoeBidCard
                  brand={item.variant.product.brand.name}
                  modelType={item.variant.product.name}
                  colorVersion={item.variant.color}
                  priceAmount={item.price}
                  imageUri={item.variant.images.image}
                  status={item.status}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => item.id.toString()}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isRefreshing}
          />
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: RhypeColors.white,
          }}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
    paddingHorizontal: 5,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 36,
    color: RhypeColors.darkBlue,
  },
  cardButton: {
    marginRight: 10,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
});

export default connect(
  mapStateToProps,
  null
)(RecentBidsContainer);
