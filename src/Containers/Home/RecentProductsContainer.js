/**
 * Created by adrianperez on 07/04/2018.
 */
import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';
import ShoeCard from '../../Components/Shoe/ShoeCard';
import { fetchVariantsDataByShoeSize } from '../../Services/BrowseService';
import { fetchProfileData } from '../../Services/ProfileService';
import { connect } from 'react-redux';

class RecentProductsContainer extends Component {
  state = {
    data: null,
    isRefreshing: false,
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetchProfileData({
      token: this.props.loginData.token,
    })
      .then(data => {
        return { shoeSize: data.shoe_size };
      })
      .then(data => {
        this.setState({
          isRefreshing: true,
        });
        return data;
      })
      .then(data => {
        return fetchVariantsDataByShoeSize(data);
      })
      .then(products => {
        const sorted = products.results.sort(
          (a, b) => Date.parse(b.modified) - Date.parse(a.modified)
        );
        this.setState({
          data: sorted,
          isRefreshing: false,
        });
      });
  }

  onRefresh() {
    this.fetchData();
  }

  navigate = itemId => {
    const navigateToScreen = NavigationActions.navigate({
      routeName: 'shoeDetail',
      params: {
        itemId,
      },
    });
    this.props.navigation.dispatch(navigateToScreen);
  };

  render() {
    const data = this.state.data;

    if (data) {
      return (
        <View style={styles.container}>
          <FlatList
            horizontal
            data={data}
            initialNumToRender={4}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={styles.cardButton}
                onPress={() => this.navigate(item.id)}>
                <ShoeCard
                  brand={item.product.brand.name}
                  modelType={item.product.name}
                  colorVersion={item.color}
                  soldAmount={item.sold}
                  priceAmount={item.retail_price}
                  imageUri={item.images.image}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => item.id.toString()}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isRefreshing}
          />
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'white',
          }}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
    paddingHorizontal: 5,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 36,
    color: RhypeColors.darkBlue,
  },
  cardButton: {
    marginRight: 10,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
});

export default connect(
  mapStateToProps,
  null
)(RecentProductsContainer);
