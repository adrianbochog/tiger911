/**
 * Created by jeunesseburce on 30/08/2018.
 */
import React from 'react';
import ShoeDetailPage from '../Components/Shoe/ShoeDetailPage'; // change naming later
import { connect } from 'react-redux';

import {
  addToFavorites,
  removeFromFavorites,
  fetchVariantsDataById,
} from '../Actions/actionCreator';
import {
  ActivityIndicator,
  TouchableOpacity,
  View,
} from 'react-native';
import { Icon } from 'react-native-elements';
import * as RhypeColors from '../Commons/RhypeColors';
import { NavigationActions } from 'react-navigation';

class ProductDetailContainer extends React.Component {
  state = {
    isModalVisible: false,
    shoeSize: '',
    itemId: this.props.params ? this.props.params.itemId : null,
    name: this.props.params ? this.props.params.name : null,
  };

  isModalVisible = () => {
    return this.state.isModalVisible;
  };

  getShoeSize = () => {
    return this.state.shoeSize;
  };

  updateShoeDetails = (itemId, shoeSize) => {
    // console.log('updating shoe size ' + shoeSize + ' ' + itemId)
    this.setState({
      shoeSize,
      itemId,
    });
    this.props.fetchVariantsDataById({
      id: itemId,
      token: this.props.loginData.token,
    });
  };

  toggleModal = () => {
    const isVisible = this.state.isModalVisible;
    this.setState({
      isModalVisible: !isVisible,
    });
  };

  closeModal = () => {
    this.setState({
      isModalVisible: false,
    });
  };

  constructor(props) {
    super(props);

    const isFavorite = this.props.favorites.reduce((acc, curr) => {
      return acc || curr.id === this.props.params.itemId;
    }, false);

    this.props.navigation.setParams({
      isFavorite,
      onFavorite: () => {
        this.props.navigation.setParams({ isFavorite: !isFavorite });
        if (isFavorite) {
          this.props.removeFromFavorites(
            [this.props.params.itemId],
            this.props.loginData.token
          );
        } else {
          this.props.favorite(
            [this.props.params.itemId],
            this.props.loginData.token
          );
        }
      },
    });
  }

  componentDidUpdate(prevProps) {
    const prevIsFavorite = prevProps.favorites.reduce((acc, curr) => {
      return acc || curr.id === prevProps.params.itemId;
    }, false);
    const isFavorite = this.props.favorites.reduce((acc, curr) => {
      return acc || curr.id === this.props.params.itemId;
    }, false);

    if (prevIsFavorite !== isFavorite) {
      this.props.navigation.setParams({
        onFavorite: () => {
          this.props.navigation.setParams({
            isFavorite: !isFavorite,
          });
          if (isFavorite) {
            this.props.removeFromFavorites(
              [this.props.params.itemId],
              this.props.loginData.token
            );
          } else {
            this.props.favorite(
              [this.props.params.itemId],
              this.props.loginData.token
            );
          }
        },
      });
    }
  }

  componentDidMount() {
    this.props.fetchVariantsDataById({
      id: this.state.itemId,
      token: this.props.loginData.token,
    });
  }

  navigateToBidConfirmation = data => {
    // console.log('navigateToBidConfirmation', data)
    const navigateToScreen = NavigationActions.navigate({
      routeName: 'shoeBidConfirmation',
      params: {
        id: data.id,
        brand: data.brand,
        name: data.name,
        colorway: data.colorway,
        retailPrice: data.retailPrice,
        imageUri: data.imageUri,
      },
    });
    this.props.navigation.dispatch(navigateToScreen);
  };

  navigateToAskConfirmation = data => {
    // console.log('navigateToAskConfirmation', data)
    const navigateToScreen = NavigationActions.navigate({
      routeName: 'shoeAskConfirmation',
      params: {
        id: data.id,
        brand: data.brand,
        name: data.name,
        colorway: data.colorway,
        retailPrice: data.retailPrice,
        imageUri: data.imageUri,
      },
    });
    this.props.navigation.dispatch(navigateToScreen);
  };

  render() {
    const { shoeData } = this.props;
    if (!shoeData.isFetching && shoeData.data) {
      // sorry nagmamadali
      const variant = shoeData.data || {};

      return (
        <View>
          <ShoeDetailPage
            {...variant}
            navigateToBidConfirmation={this.navigateToBidConfirmation}
            navigateToAskConfirmation={this.navigateToAskConfirmation}
            toggleModal={this.toggleModal}
            isModalVisible={this.isModalVisible}
            closeModal={this.closeModal}
            getShoeSize={this.getShoeSize}
            updateShoeDetails={this.updateShoeDetails}
            navigation={this.props.navigation}
          />
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }
  }
}

const mapStateToProps = (state, props) => ({
  params: props.navigation.state.params,
  shoeData: state.ShoeDataReducer,
  loginData: state.LoginReducer,
  favorites: state.ProfileDataReducer.favorites.data,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchVariantsDataById: data =>
      dispatch(fetchVariantsDataById(data)),
    favorite: (favorites, token) =>
      dispatch(addToFavorites(favorites, token)),
    removeFromFavorites: (favorites, token) =>
      dispatch(removeFromFavorites(favorites, token)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductDetailContainer);
