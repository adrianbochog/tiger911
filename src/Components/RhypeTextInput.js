/**
 * Created by adrianperez on 13/03/2018.
 */
import React, { Component } from 'react';
import { TextInput, StyleSheet, Platform } from 'react-native';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';

export default class RhypeTextInput extends Component {
  render() {
    if (!this.props.isOnDark) {
      return (
        <TextInput
          placeholderTextColor={
            this.props.placeholderTextColor || RhypeColors.gray
          }
          defaultValue={this.props.defaultValue || null}
          placeholder={this.props.placeholder || ''}
          secureTextEntry={this.props.secureTextEntry || false}
          autoCorrect={false}
          value={this.props.value}
          onChangeText={this.props.onChangeText}
          onEndEditing={this.props.onEndEditing}
          onBlur={this.props.onBlur}
          style={defaultStyle.textInput}
          autoCapitalize={this.props.autoCapitalize || 'sentences'}
          keyboardType={this.props.keyboardType || 'default'}
          underlineColorAndroid="transparent"
        />
      );
    } else {
      return (
        <TextInput
          placeholderTextColor={
            this.props.placeholderTextColor || RhypeColors.gray
          }
          defaultValue={this.props.defaultValue || null}
          placeholder={this.props.placeholder || ''}
          secureTextEntry={this.props.secureTextEntry || false}
          autoCorrect={false}
          value={this.props.value}
          onChangeText={this.props.onChangeText}
          onEndEditing={this.props.onEndEditing}
          onBlur={this.props.onBlur}
          style={lightStyle.textInput}
          autoCapitalize={this.props.autoCapitalize || 'sentences'}
          keyboardType={this.props.keyboardType || 'default'}
          underlineColorAndroid="transparent"
        />
      );
    }
  }
}

RhypeTextInput.defaultProps = {
  isOnDark: false,
};

const defaultStyle = StyleSheet.create({
  textInput: {
    borderBottomColor: RhypeColors.gray,
    borderBottomWidth: 1,
    paddingLeft: 10,
    fontFamily: fonts.UniformCondensed.light,
    ...Platform.select({
      ios: {
        height: 35,
        fontSize: 20,
      },
      android: {
        height: 40,
        fontSize: 18,
      },
    }),
  },
});

const lightStyle = StyleSheet.create({
  textInput: {
    backgroundColor: RhypeColors.white,
    paddingLeft: 10,
    height: 50,
    fontSize: 20,
    fontFamily: fonts.UniformCondensed.light,
    fontWeight: 'normal',
    marginBottom: 10,
  },
});
