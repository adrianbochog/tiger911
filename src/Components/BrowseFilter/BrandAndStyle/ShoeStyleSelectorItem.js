/**
 * Created by adrianperez on 21/04/2018.
 */
import React, { Component } from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';
import * as RhypeColors from '../../../Commons/RhypeColors';
import fonts from '../../../Commons/RhypeFonts';

export default class ShoeStyleSelectorItem extends Component {
  constructor(props) {
    super(props);
    this.state = { isSelected: this.props.isSelected };
  }

  onPress() {
    this.toggleColor();
    this.state.isSelected
      ? this.props.toggleRemove(this.props.value)
      : this.props.toggleAdd(this.props.value);
  }

  toggleColor() {
    this.setState({
      isSelected: !this.state.isSelected,
    });
  }

  render() {
    return (
      <TouchableOpacity
        style={[
          styles.brandSelectorButton,
          {
            backgroundColor: this.state.isSelected
              ? RhypeColors.copper
              : RhypeColors.darkBlue,
          },
        ]}
        onPress={() => this.onPress()}>
        <Text style={styles.brandSelectorButtonText}>
          {this.props.value.toUpperCase()}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  brandItemHeader: {
    ...fonts.UniformCondensed.bold,
    fontSize: 24,
    color: RhypeColors.darkBlue,
  },
  brandItemSelector: {},
  brandSelectorButton: {
    marginHorizontal: 5,
    backgroundColor: RhypeColors.darkBlue,
    paddingHorizontal: 4,
  },
  brandSelectorButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.white,
  },
});
