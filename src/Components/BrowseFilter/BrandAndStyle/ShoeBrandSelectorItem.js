import React, { Component } from 'react';
import { Text, StyleSheet, View, ScrollView } from 'react-native';
import * as RhypeColors from '../../../Commons/RhypeColors';
import fonts from '../../../Commons/RhypeFonts';
import ShoeStyleSelectorItem from './ShoeStyleSelectorItem';

export default class ShoeBrandSelectorItem extends Component {
  constructor(props) {
    super(props);
    this.state = { isSelected: this.props.isSelected };
  }

  onPress() {
    this.toggleColor();
    this.state.isSelected
      ? this.props.toggleRemove(this.props.value)
      : this.props.toggleAdd(this.props.value);
  }

  toggleColor() {
    this.setState({
      isSelected: !this.state.isSelected,
    });
  }

  render() {
    const styleFilter = this.props.styleFilter;
    const data = this.props.data;
    const styleSelectorButtons = data.map(value => (
      <ShoeStyleSelectorItem
        key={value}
        value={value}
        isSelected={styleFilter.includes(value)}
        toggleAdd={this.props.shoeStyleToggleAdd}
        toggleRemove={this.props.shoeStyleToggleRemove}
      />
    ));
    return (
      <View style={styles.brandItem}>
        <Text
          style={[
            styles.brandItemHeader,
            {
              color: this.state.isSelected
                ? RhypeColors.copper
                : RhypeColors.darkBlue,
            },
          ]}
          onPress={() => this.onPress()}>
          {this.props.header.toUpperCase()}
        </Text>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={styles.brandItemSelector}>
          {styleSelectorButtons}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  brandItemHeader: {
    ...fonts.UniformCondensed.bold,
    fontSize: 24,
  },
  brandItemSelector: {},
  brandSelectorButton: {
    marginHorizontal: 5,
    backgroundColor: RhypeColors.darkBlue,
    paddingHorizontal: 4,
  },
  brandSelectorButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.white,
  },
});
