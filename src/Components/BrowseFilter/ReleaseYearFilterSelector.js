/**
 * Created by adrianperez on 21/04/2018.
 */
import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { addBrowseFilter } from '../../Actions/actionCreator';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';
import YearSelectorItem from './Year/YearSelectorItem';

export default class ReleaseYearFilterSelector extends Component {
  render() {
    const filter = this.props.filter;
    const data = this.props.data;
    const yearButtons = data.map(value => (
      <YearSelectorItem
        key={value}
        value={value}
        isSelected={filter.includes(value)}
        toggleAdd={this.props.toggleAdd}
        toggleRemove={this.props.toggleRemove}
      />
    ));

    return (
      <View style={styles.releasedYearContainer}>
        <Text style={styles.filterTypeHeaderText}>
          CHOOSE RELEASE YEAR
        </Text>
        <View style={styles.yearSelectorContainer}>
          {yearButtons}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  filterTypeHeaderText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 28,
    color: RhypeColors.darkBlue,
  },
  yearSelectorContainer: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  yearSelectorRow: {
    flexDirection: 'row',
    marginVertical: 2,
  },
  yearButton: {
    paddingHorizontal: 3,
    marginHorizontal: 3,
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  yearButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.white,
  },
});
