/**
 * Created by adrianperez on 21/04/2018.
 */
import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';
import ShoeSelectorItem from './ShoeSize/ShoeSelectorItem';

export default class ShoeSizeFilterSelector extends Component {
  render() {
    const filter = this.props.filter;
    const data = this.props.data;
    const shoeSizeButtons = data.map(value => (
      <ShoeSelectorItem
        key={value}
        value={value}
        isSelected={filter.includes(value)}
        toggleAdd={this.props.toggleAdd}
        toggleRemove={this.props.toggleRemove}
      />
    ));
    return (
      <View style={styles.sizeContainer}>
        <Text style={styles.filterTypeHeaderText}>CHOOSE SIZE</Text>
        <View style={styles.sizeSelectorContainer}>
          {shoeSizeButtons}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  filterTypeHeaderText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 28,
    color: RhypeColors.darkBlue,
  },
  selectorButton: {
    marginHorizontal: 5,
    backgroundColor: RhypeColors.darkBlue,
    paddingHorizontal: 4,
  },
  selectorButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: 'white',
  },
  sizeSelectorContainer: {
    paddingLeft: 10,
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  sizeSelectorRow: {
    flexDirection: 'row',
    marginVertical: 2,
  },
  sizeButton: {
    width: 32,
    marginHorizontal: 3,
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sizeButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.white,
  },
});
