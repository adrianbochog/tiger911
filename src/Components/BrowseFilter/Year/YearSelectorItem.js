/**
 * Created by adrianperez on 21/04/2018.
 */
import React, { Component } from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';
import * as RhypeColors from '../../../Commons/RhypeColors';
import fonts from '../../../Commons/RhypeFonts';

export default class YearSelectorItem extends Component {
  constructor(props) {
    super(props);
    this.state = { isSelected: this.props.isSelected };
  }

  onPress() {
    this.toggleColor();
    this.state.isSelected
      ? this.props.toggleRemove(this.props.value)
      : this.props.toggleAdd(this.props.value);
  }

  toggleColor() {
    this.setState({
      isSelected: !this.state.isSelected,
    });
  }

  render() {
    return (
      <TouchableOpacity
        style={[
          styles.yearButton,
          {
            backgroundColor: this.state.isSelected
              ? RhypeColors.copper
              : RhypeColors.darkBlue,
          },
        ]}
        onPress={() => this.onPress()}>
        <Text style={styles.yearButtonText}>{this.props.value}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  yearButton: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 4,
    margin: 2,
    width: 50,
  },
  yearButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.white,
  },
});
