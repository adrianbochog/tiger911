/**
 * Created by adrianperez on 21/04/2018.
 */

import React, { Component } from 'react';
import {
  Text,
  TouchableHighlight,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Slider,
} from 'react-native';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';
import ShoeBrandSelectorItem from './BrandAndStyle/ShoeBrandSelectorItem';

export default class BrandAndStyleFilterSelector extends Component {
  render() {
    const brandFilter = this.props.brandFilter;
    const data = this.props.data;
    const brandItems = data.map(value => (
      <ShoeBrandSelectorItem
        key={value.brandName}
        header={value.brandName}
        value={value.brandName}
        data={value.models}
        isSelected={brandFilter.includes(value.brandName)}
        styleFilter={this.props.styleFilter}
        toggleAdd={this.props.shoeBrandToggleAdd}
        toggleRemove={this.props.shoeBrandToggleRemove}
        shoeStyleToggleAdd={this.props.shoeStyleToggleAdd}
        shoeStyleToggleRemove={this.props.shoeStyleToggleRemove}
      />
    ));
    return (
      <View style={styles.brandContainer}>
        <Text style={styles.filterTypeHeaderText}>CHOOSE BRAND</Text>
        <View style={styles.brandSelectorContainer}>
          {brandItems}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  brandContainer: {
    marginVertical: 10,
  },
  filterTypeHeaderText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 28,
    color: RhypeColors.darkBlue,
  },
  brandSelectorContainer: {
    paddingLeft: 10,
  },
});
