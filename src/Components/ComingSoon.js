import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';

export default class ComingSoon extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Icon
          name="gear"
          type="evilicon"
          color={RhypeColors.darkBlue}
          size={200}
        />
        <Text style={styles.messageHeader}>COMING SOON</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  messageHeader: {
    ...fonts.UniformCondensed.bold,
    fontSize: 30,
  },
  messageBody: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
  },
});
