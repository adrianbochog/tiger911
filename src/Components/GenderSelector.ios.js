import React, { Component } from 'react';
import {
  Picker,
  Modal,
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
} from 'react-native';

import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';

export default class GenderSelector extends Component {
  state = {
    gender: 'M',
    visible: false,
  };

  render() {
    const { value, onChange } = this.props;
    let v = null;
    if (value) {
      if (value === 'M') v = 'MALE';
      else if (value === 'F') v = 'FEMALE';
    }
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.setState({ visible: true })}>
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: RhypeColors.gray,
              marginBottom: 80,
            }}>
            <Text
              value={this.props.value}
              style={[
                lightStyle.textInput,
                value
                  ? { color: '#000000' }
                  : { color: RhypeColors.gray },
              ]}
              underlineColorAndroid="transparent">
              {v || 'GENDER'}
            </Text>
          </View>
        </TouchableOpacity>
        <Modal
          animationType="slide"
          visible={this.state.visible}
          transparent>
          <TouchableOpacity
            style={{ flex: 1 }}
            onPress={() => this.setState({ visible: false })}
          />
          <View style={{ backgroundColor: '#FFFFFF' }}>
            <View
              style={{
                backgroundColor: '#F0F0F0',
                padding: 12,
                justifyContent: 'space-between',
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ visible: false });
                }}>
                <Text style={{ fontSize: 16 }}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  if (onChange) {
                    onChange(this.state.gender);
                  }
                  this.setState({ visible: false });
                }}>
                <Text style={{ fontSize: 16 }}>Confirm</Text>
              </TouchableOpacity>
            </View>
            <Picker
              style={{ height: 200 }}
              selectedValue={this.state.gender}
              onValueChange={val => this.setState({ gender: val })}>
              <Picker.Item label="Male" value="M" />
              <Picker.Item label="Female" value="F" />
            </Picker>
          </View>
        </Modal>
      </View>
    );
  }
}

const lightStyle = StyleSheet.create({
  textInput: {
    backgroundColor: RhypeColors.white,
    paddingLeft: 10,
    fontSize: 20,
    fontFamily: fonts.UniformCondensed.light,
    fontWeight: 'normal',
    marginVertical: 4,
  },
});
