import React, { Component } from 'react';
import { Image, StyleSheet, Platform, View } from 'react-native';

export default class LogoTitle extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('../../assets/images/logo.png')}
          style={styles.logo}
          resizeMode="contain"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    height: 20,
    ...Platform.select({
      ios: {},
      android: {
        alignSelf: 'center',
      },
    }),
  },
  container: {
    ...Platform.select({
      ios: {},
      android: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-start',
      },
    }),
  },
});
