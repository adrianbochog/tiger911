import React from 'react';
import {
  Modal,
  View,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';

const AddressInput = ({
  edit,
  label,
  particulars,
  street,
  city,
  area,
  region,
  zipCode,
  onChange,
  onClose,
  onSubmit,
}) => (
  <KeyboardAvoidingView
    style={styles.containerModal}
    behavior="padding"
    enabled>
    <View style={styles.headerModal}>
      <Text style={styles.headerText}>
        {'EDIT ' + label + ' ADDRESS'}
      </Text>
    </View>
    <View style={styles.content}>
      <View style={styles.contentWrapper}>
        <View style={styles.row}>
          <View style={styles.column}>
            <Text style={styles.label}>PARTICULARS</Text>
            <TextInput
              style={styles.value}
              value={particulars}
              autoCorrect={false}
              onChangeText={onChange('particulars')}
            />
          </View>
          <View style={styles.column}>
            <Text style={styles.label}>STREET</Text>
            <TextInput
              style={styles.value}
              value={street}
              autoCorrect={false}
              onChangeText={onChange('street')}
            />
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.column}>
            <Text style={styles.label}>AREA</Text>
            <TextInput
              style={styles.value}
              value={area}
              autoCorrect={false}
              onChangeText={onChange('area')}
            />
          </View>
          <View style={styles.column}>
            <Text style={styles.label}>CITY</Text>
            <TextInput
              style={styles.value}
              value={city}
              autoCorrect={false}
              onChangeText={onChange('city')}
            />
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.column}>
            <Text style={styles.label}>REGION</Text>
            <TextInput
              style={styles.value}
              value={region}
              autoCorrect={false}
              onChangeText={onChange('region')}
            />
          </View>
          <View style={styles.column}>
            <Text style={styles.label}>ZIP CODE</Text>
            <TextInput
              style={styles.value}
              value={zipCode}
              autoCorrect={false}
              onChangeText={onChange('zipCode')}
            />
          </View>
        </View>
      </View>
    </View>
    <View style={styles.footer}>
      <TouchableOpacity style={styles.closeButton} onPress={onClose}>
        <Text style={styles.buttonText}>CLOSE</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.updateButton}
        onPress={onSubmit}>
        <Text style={styles.buttonText}>UPDATE</Text>
      </TouchableOpacity>
    </View>
  </KeyboardAvoidingView>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
    margin: 10,
  },
  containerModal: {
    flex: 1,
    justifyContent: 'space-between',
  },
  header: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.darkBlue,
  },
  contentWrapper: {
    paddingLeft: 10,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
  },
  column: {
    margin: 5,
    flex: 1,
  },
  rowItemContent: {},
  label: {
    ...fonts.UniformCondensed.bold,
    fontSize: 14,
    color: RhypeColors.darkBlue,
  },
  value: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
    color: RhypeColors.copper,
  },
  closeButton: {
    backgroundColor: RhypeColors.copper,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  updateButton: {
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  headerModal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.darkBlue,
  },
  headerText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 24,
    color: RhypeColors.white,
    marginTop: 20,
    paddingVertical: 10,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    height: 40,
  },
  buttonText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
    color: RhypeColors.white,
  },
});

export default AddressInput;
