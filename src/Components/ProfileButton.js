import React, { Component } from 'react';
import { Image, StyleSheet, TouchableOpacity } from 'react-native';

export default class LogoTitle extends Component {
  render() {
    return (
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          this.props.navigation.navigate('DrawerToggle');
        }}>
        <Image
          source={require('../../assets/images/icons/profile_icon.png')}
          style={styles.logo}
          resizeMode="contain"
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    width: 60,
    alignItems: 'center',
  },
  logo: {
    height: 30,
  },
});
