import React, { Component } from 'react';
import { Picker, Modal, View, StyleSheet, Text } from 'react-native';

import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';

export default class GenderSelector extends Component {
  state = {
    gender: '0',
    visible: false,
  };

  render() {
    const { value, onChange } = this.props;
    let v = null;
    if (value) {
      if (value === 'M') v = 'MALE';
      else if (value === 'F') v = 'FEMALE';
    }
    return (
      <View>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: RhypeColors.gray,
            marginBottom: 80,
          }}>
          <Text
            value={this.props.value}
            style={[
              lightStyle.textInput,
              value
                ? { color: '#000000' }
                : { color: RhypeColors.gray },
            ]}
            underlineColorAndroid="transparent">
            {v || 'GENDER'}
          </Text>
        </View>
        <Picker
          ref={c => {
            this.input = c;
          }}
          style={{
            position: 'absolute',
            top: 0,
            width: 1000,
            height: 1000,
          }}
          selectedValue={this.state.gender}
          onValueChange={val => {
            if (val !== '0') {
              if (onChange) {
                onChange(val);
              }
              this.setState({ gender: val });
            }
          }}>
          <Picker.Item label="Please select your gender" value="0" />
          <Picker.Item label="Male" value="M" />
          <Picker.Item label="Female" value="F" />
        </Picker>
      </View>
    );
  }
}

const lightStyle = StyleSheet.create({
  textInput: {
    backgroundColor: RhypeColors.white,
    paddingLeft: 10,
    fontSize: 20,
    fontFamily: fonts.UniformCondensed.light,
    fontWeight: 'normal',
    marginVertical: 8,
  },
});
