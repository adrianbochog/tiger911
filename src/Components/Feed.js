import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import * as RhypeColors from '../Commons/RhypeColors';

export default class Feed extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: RhypeColors.white }}>
        <Text style={{ textAlign: 'center' }}> Feed </Text>
      </View>
    );
  }
}
