/**
 * Created by adrianperez on 05/05/2018.
 */
/**
 * Created by adrianperez on 04/05/2018.
 */

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import * as RhypeColors from '../../../Commons/RhypeColors';
import fonts from '../../../Commons/RhypeFonts';

export default class SellingDetailsSection extends Component {
  render() {
    return (
      <TouchableOpacity style={styles.container}>
        <Text style={styles.header}>SELLING DETAILS</Text>
        <View style={styles.contentWrapper}>
          <View style={styles.row}>
            <View style={styles.column}>
              <Text style={styles.label}>PAYMENT</Text>
              <Text style={styles.value}>
                **** **** **** **** 1234
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    margin: 10,
  },
  header: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.darkBlue,
  },
  contentWrapper: {
    paddingLeft: 10,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
  },
  column: {
    margin: 5,
  },
  rowItemContent: {},
  label: {
    ...fonts.UniformCondensed.bold,
    fontSize: 14,
    color: RhypeColors.darkBlue,
  },
  value: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
    color: RhypeColors.copper,
  },
});
