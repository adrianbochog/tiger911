/**
 * Created by adrianperez on 05/05/2018.
 */
/**
 * Created by adrianperez on 04/05/2018.
 */

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import * as RhypeColors from '../../../Commons/RhypeColors';
import fonts from '../../../Commons/RhypeFonts';

export default class PaymentDetailsSection extends Component {
  render() {
    const data = this.props.data || [];
    // const paymentDetails = data.map((value) =>
    //   <View style={styles.column} key={value.bank_name + value.account_number}>
    //     <Text style={styles.value}>{value.bank_name} {value.account_number} {value.account_name}</Text>
    //   </View>
    // )

    const {
      bank_name: bankName,
      account_name: accountName,
      account_number: accountNumber,
    } = data;

    return (
      <TouchableOpacity
        onPress={this.props.onPress}
        style={styles.container}>
        <Text style={styles.header}>PAYMENT DETAILS</Text>
        <View style={styles.contentWrapper}>
          <View style={styles.row}>
            <View style={styles.column}>
              <Text style={styles.label}>BANK NAME</Text>
              <Text style={styles.value}>{bankName}</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.column}>
              <Text style={styles.label}>ACCOUNT NAME</Text>
              <Text style={styles.value}>{accountName}</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.column}>
              <Text style={styles.label}>ACCOUNT NUMBER</Text>
              <Text style={styles.value}>{accountNumber}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'center',
    margin: 10,
  },
  header: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.darkBlue,
  },
  contentWrapper: {
    paddingLeft: 10,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
  },
  column: {
    margin: 2,
  },
  rowItemContent: {},
  label: {
    ...fonts.UniformCondensed.bold,
    fontSize: 14,
    color: RhypeColors.darkBlue,
  },
  value: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
    color: RhypeColors.copper,
  },
});
