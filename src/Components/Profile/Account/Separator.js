/**
 * Created by adrianperez on 05/05/2018.
 */

import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import * as RhypeColors from '../../../Commons/RhypeColors';

export default class Separator extends Component {
  render() {
    return <View style={styles.root} />;
  }
}

const styles = StyleSheet.create({
  root: {
    height: 1,
    backgroundColor: RhypeColors.copper,
    alignSelf: 'stretch',
  },
});
