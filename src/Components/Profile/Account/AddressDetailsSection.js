import React, { Component } from 'react';
import {
  Alert,
  TextInput,
  KeyboardAvoidingView,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import * as RhypeColors from '../../../Commons/RhypeColors';
import fonts from '../../../Commons/RhypeFonts';
import * as ProfileService from '../../../Services/ProfileService';
import { NavigationActions } from 'react-navigation';

import AddressInput from '../../AddressInput';

export default class AddressDetailsSection extends Component {
  constructor(props) {
    super(props);

    const {
      particulars,
      street,
      area,
      city,
      region,
      zipCode,
    } = props;

    this.state = {
      edit: false,
      particulars,
      street,
      area,
      city,
      region,
      zipCode,
    };

    this.updateState = this.updateState.bind(this);
  }

  componentWillReceiveProps(props) {
    this.setState({
      particulars: props.particulars,
      street: props.street,
      area: props.area,
      city: props.city,
      region: props.region,
      zipCode: props.zipCode,
    });
  }

  updateState(field) {
    return text => {
      const obj = {};
      obj[field] = text;
      this.setState(obj);
    };
  }

  updateAddress() {
    const { label, token } = this.props;
    const {
      particulars,
      street,
      area,
      city,
      region,
      zipCode,
    } = this.state;

    let updateServer = null;

    if (label === 'SHIPPING') {
      updateServer = ProfileService.updateShippingAddressDetails;
    } else if (label === 'BILLING') {
      updateServer = ProfileService.updateBillingAddressDetails;
    } else {
      return;
    }

    updateServer({
      token,
      particulars,
      street,
      area,
      city,
      region,
      zipCode,
    })
      .then(() => this.props.onSuccess())
      .catch(err => {
        console.log(err);
        Alert.alert('Error', "Can't update address");
      });
  }

  render() {
    const {
      label,
      particulars,
      street,
      area,
      city,
      region,
      zipCode,
    } = this.props;

    return (
      <TouchableOpacity
        onPress={() => this.setState({ edit: true })}
        style={styles.container}>
        <Text style={styles.header}>{label + ' ADDRESS'}</Text>
        <View style={styles.contentWrapper}>
          <View style={styles.row}>
            <View style={styles.column}>
              <Text style={styles.label}>PARTICULARS</Text>
              <Text style={styles.value}>{particulars}</Text>
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>STREET</Text>
              <Text style={styles.value}>{street}</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.column}>
              <Text style={styles.label}>AREA</Text>
              <Text style={styles.value}>{area}</Text>
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>CITY</Text>
              <Text style={styles.value}>{city}</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.column}>
              <Text style={styles.label}>REGION</Text>
              <Text style={styles.value}>{region}</Text>
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>ZIP CODE</Text>
              <Text style={styles.value}>{zipCode}</Text>
            </View>
          </View>
        </View>
        <Modal
          animationType="slide"
          visible={this.state.edit}
          onRequestClose={() => {}}>
          <AddressInput
            label={label}
            particulars={this.state.particulars}
            street={this.state.street}
            city={this.state.city}
            region={this.state.region}
            zipCode={this.state.zipCode}
            area={this.state.area}
            onChange={this.updateState}
            onClose={() => this.setState({ edit: false })}
            onSubmit={() => this.updateAddress()}
          />
        </Modal>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
    margin: 10,
  },
  containerModal: {
    flex: 1,
    justifyContent: 'space-between',
  },
  header: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.darkBlue,
  },
  contentWrapper: {
    paddingLeft: 10,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
  },
  column: {
    margin: 5,
    flex: 1,
  },
  rowItemContent: {},
  label: {
    ...fonts.UniformCondensed.bold,
    fontSize: 14,
    color: RhypeColors.darkBlue,
  },
  value: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
    color: RhypeColors.copper,
  },
  closeButton: {
    backgroundColor: RhypeColors.copper,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  updateButton: {
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  headerModal: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.darkBlue,
  },
  headerText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 24,
    color: RhypeColors.white,
    marginTop: 20,
    paddingVertical: 10,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    height: 40,
  },
  buttonText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
    color: RhypeColors.white,
  },
});
