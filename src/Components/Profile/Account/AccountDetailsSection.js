/**
 * Created by adrianperez on 05/05/2018.
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import * as RhypeColors from '../../../Commons/RhypeColors';
import fonts from '../../../Commons/RhypeFonts';

export default class AccountDetailsSection extends Component {
  render() {
    const addresses = this.props.addresses || [];

    // const addressType = addresses.name
    //  ? <Text style={styles.label}> {addresses.name} </Text> : ''

    // const addressList = addresses.map((value, i) =>
    //   <View key={i}>
    //     {
    //       Object.entries(value).map(([k, v]) => {
    //         <Text style={styles.value} key={k}>v</Text>
    //       })
    //     }
    //   </View>
    // )

    return (
      <TouchableOpacity
        onPress={() => this.props.toggleModal()}
        style={styles.container}>
        <Text style={styles.header}>ACCOUNT DETAILS</Text>
        <View style={styles.contentWrapper}>
          <View style={styles.row}>
            <View style={styles.column}>
              <Text style={styles.label}>FIRST NAME</Text>
              <Text style={styles.value}>{this.props.firstName}</Text>
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>LAST NAME</Text>
              <Text style={styles.value}>{this.props.lastName}</Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.column}>
              <Text style={styles.label}>USERNAME</Text>
              <Text style={styles.value}>{this.props.username}</Text>
            </View>
            <View style={styles.column}>
              <Text style={styles.label}>CONTACT NUMBER</Text>
              <Text style={styles.value}>
                {this.props.contactNumber}
              </Text>
            </View>
          </View>
          <View style={styles.row}>
            <View style={styles.column}>
              <Text style={styles.label}>EMAIL</Text>
              <Text style={styles.value}>{this.props.email}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
    margin: 10,
  },
  header: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: RhypeColors.darkBlue,
  },
  contentWrapper: {
    paddingLeft: 10,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
  },
  column: {
    margin: 5,
    flex: 1,
  },
  rowItemContent: {},
  label: {
    ...fonts.UniformCondensed.bold,
    fontSize: 14,
    color: RhypeColors.darkBlue,
  },
  value: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
    color: RhypeColors.copper,
  },
});
