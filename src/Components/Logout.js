import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Modal,
} from 'react-native';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { LoginManager } from 'react-native-fbsdk';

import { logout } from '../Actions/actionCreator';

class LogoutScreen extends Component {
  state = {
    modalVisible: true,
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  cancel = () => {
    this.setState({ modalVisible: false });

    const navigateToHome = NavigationActions.navigate({
      routeName: 'mainScreens',
    });
    this.props.navigation.dispatch(navigateToHome);
  };

  logout = () => {
    this.setState({ modalVisible: false });

    LoginManager.logOut();
    this.props.logout();
    this.props.navigation.navigate('welcomeScreens');
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          alert('Are you sure you want to logout?');
        }}>
        <View style={styles.reminderModal}>
          <View style={styles.smallerModal}>
            <View style={styles.exit}>
              <TouchableOpacity
                onPress={() => {
                  this.cancel();
                }}>
                <Text style={styles.exitText}>X</Text>
              </TouchableOpacity>
            </View>

            <Text style={styles.textModal}>
              {' '}
              Are you sure you want to logout?{' '}
            </Text>

            <TouchableOpacity
              onPress={() => {
                this.cancel();
              }}>
              <View style={styles.buttonWrapper}>
                <View style={styles.okButton}>
                  <Text style={styles.confirm}> CANCEL </Text>
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.logout();
              }}>
              <View style={styles.buttonWrapper}>
                <View style={styles.okButton}>
                  <Text style={styles.confirm}> YES </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  reminderModal: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  smallerModal: {
    width: 350,
    height: 200,
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: RhypeColors.white,
  },
  exit: {
    flex: 0.2,
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
  },
  exitText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.copper,
    marginHorizontal: 5,
    marginVertical: 5,
  },
  textModal: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    textAlign: 'center',
    color: RhypeColors.darkBlue,
  },
  confirm: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    width: 100,
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.white,
    textAlign: 'center',
    paddingVertical: 10,
  },
  okButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.darkBlue,
    zIndex: 0,
  },
  buttonWrapper: {
    flexDirection: 'row',
    height: 50,
    marginVertical: 10,
  },
});

const mapDispatchToProps = {
  logout,
};

export default connect(
  null,
  mapDispatchToProps
)(LogoutScreen);
