/**
 * Created by adrianperez on 13/03/2018.
 */
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';

export default class ShoeConfirmationProductDetailSection extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.productDetailsContainer}>
          <View style={styles.detailsWrapper}>
            <Text numberOfLines={1} style={styles.brand}>
              {this.props.brand ? this.props.brand : '###'}
            </Text>
            <Text numberOfLines={1} style={styles.modelType}>
              {this.props.name ? this.props.name : '###'}
            </Text>
            <Text numberOfLines={1} style={styles.colorVersion}>
              {this.props.colorway ? this.props.colorway : '###'}
            </Text>
          </View>

          <View style={styles.imageWrapper}>
            <Image
              source={
                this.props.imageUri
                  ? { uri: this.props.imageUri }
                  : require('../../../assets/images/placeholder/shoe-page-image.png')
              }
              style={styles.image}
              resizeMode="contain"
            />
          </View>
        </View>
        <View style={styles.marketDetailsContainer}>
          <View style={styles.lowestAskWrapper}>
            <Text style={styles.labelText}>LOWEST ASK:</Text>
            <Text style={styles.marketDetailValue}>
              {this.props.lowestAsk ? this.props.lowestAsk : 'None'}
            </Text>
          </View>
          <View style={styles.highestBidWrapper}>
            <Text style={styles.labelText}>HIGHEST BID:</Text>
            <Text style={styles.marketDetailValue}>
              {this.props.highestBid ? this.props.highestBid : 'None'}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  productDetailsContainer: {
    flex: 2,
  },
  imageWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  image: {
    flex: 1,
    width: 256,
    height: 192,
  },
  detailsWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: 10,
  },
  labelText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 20,
    color: 'black',
  },
  marketDetailValue: {
    ...fonts.UniformCondensed.bold,
    fontSize: 30,
    color: 'black',
  },
  brand: {
    alignSelf: 'center',
    fontFamily: fonts.UniformCondensed.regular,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  modelType: {
    alignSelf: 'center',
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 30,
    color: RhypeColors.darkBlue,
  },
  colorVersion: {
    alignSelf: 'center',
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  marketDetailsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lowestAskWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  highestBidWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
