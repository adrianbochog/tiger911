/**
 * Created by adrianperez on 18/04/2018.
 */
import React, { Component } from 'react';
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  SafeAreaView,
  WebView,
} from 'react-native';

import { connect } from 'react-redux';
import { Icon } from 'react-native-elements';

import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';
import { fetchProfileDetails } from '../../Actions/actionCreator';
import { fetchVerifyParams } from '../../Services/ProfileService';

class ShoeAskConfirmationFinancialSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      price: '',
      duration: '30',
      modalVisible: false,
      reminderModal: false,
      promptAddressModal: false,
      promptPaymentModal: false,
      paymentMethodComplete: true,
      shippingAddressComplete: true,
      isVerificationSubmitted: false,
    };
  }

  componentDidMount() {
    this.props.fetchProfileDetails({
      token: this.props.loginData.token,
    });
  }

  proceedAsk() {
    this.setState({
      // shippingAddressComplete: true,
      // paymentMethodComplete: true,
      modalVisible: false,
    });

    this.props.submitAsk({
      variantId: this.props.id,
      price: this.state.price,
      duration: this.state.duration,
    });
  }

  validateRequest = () => {
    const userDetailsFetched = this.props.userDetails
      .userDetailsFetched;

    if (userDetailsFetched) {
      const { data, userDetails } = this.props.userDetails;
      const billingAddress = userDetails.billing_address;
      const bankDetails = userDetails.bank_details;
      const { is_verified: isVerified } = data;

      if (billingAddress == null) {
        Alert.alert(
          'Error',
          'Please complete billing address before asking',
          [
            {
              text: 'OK',
              onPress: () => {
                setTimeout(
                  () =>
                    this.props.navigation.navigate('billingAddress'),
                  1000
                );
              },
            },
          ]
        );
        return;
      }
      if (bankDetails == null) {
        Alert.alert(
          'Error',
          'Please complete payment details before asking',
          [
            {
              text: 'OK',
              onPress: () => {
                setTimeout(
                  () =>
                    this.props.navigation.navigate('paymentDetails'),
                  1000
                );
              },
            },
          ]
        );
        return;
      }
      if (!(isVerified || this.state.isVerificationSubmitted)) {
        Alert.alert(
          '',
          'Thank you for trying out the bidding/selling functionality of Rhype. In order for your bid/ask to push thru, we would be requiring you to pay a small fee of $1.00 (roughly ₱55) via credit card or paypal to maintain the fair use policy of the platform. Take note that this amount would be reimbursed once a successful transaction pushed thru.',
          [
            {
              text: 'OK',
              onPress: () => {
                fetchVerifyParams(this.props.loginData.token).then(
                  ({ payment_url: paymentUrl, params }) => {
                    const query = Object.keys(params)
                      .map(key => {
                        return `${encodeURIComponent(
                          key
                        )}=${encodeURIComponent(params[key])}`;
                      })
                      .join('&');
                    const uri = `${paymentUrl}?${query}`;
                    this.setState({ paymentVerificationUri: uri });
                  }
                );
              },
            },
          ]
        );
        return;
      }
      if (this.state.price == null || Number(this.state.price) <= 0) {
        Alert.alert('Error', 'Please input a proper price');
        return;
      }

      this.setState({ modalVisible: true });
    }
  };

  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.container}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.reminderModal}
            onRequestClose={() => {
              this.setState({ reminderModal: false });
            }}>
            <View style={styles.reminderModal}>
              <View style={styles.smallerModal}>
                <View style={styles.exit}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ reminderModal: false });
                    }}>
                    <Text style={styles.exitText}>X</Text>
                  </TouchableOpacity>
                </View>

                <Text style={styles.textModal}>
                  {' '}
                  Please input needed values before placing ask.{' '}
                </Text>

                <TouchableOpacity
                  onPress={() => {
                    this.setState({ reminderModal: false });
                  }}>
                  <View style={styles.buttonWrapper}>
                    <View style={styles.okButton}>
                      <Text style={styles.confirm}>OK</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>

          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              this.setState({ modalVisible: false });
            }}>
            <View style={styles.askModal}>
              <View style={styles.modalContent}>
                <View style={styles.exit}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ modalVisible: false });
                    }}>
                    <Text style={styles.exitText}>X</Text>
                  </TouchableOpacity>
                </View>

                <View style={styles.askSpiel}>
                  <Text style={styles.textModal}>
                    {' '}
                    Are you sure you want to place ask?{' '}
                  </Text>
                  <View style={styles.detailsAskModal}>
                    <Text style={styles.label}> Price: </Text>
                    <Text style={styles.textDetails}>
                      {' '}
                      {this.state.price
                        ? this.state.price
                        : 'No price added'}{' '}
                    </Text>
                    <Text style={styles.label}> Duration: </Text>
                    <Text style={styles.textDetails}>
                      {' '}
                      {this.state.duration
                        ? this.state.duration
                        : 'No duration added'}{' '}
                    </Text>
                  </View>
                </View>

                <TouchableOpacity onPress={() => this.proceedAsk()}>
                  <View style={styles.buttonWrapper}>
                    <View style={styles.askButton}>
                      <Text style={styles.confirm}>YES</Text>
                    </View>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => {
                    this.setState({ modalVisible: false });
                  }}>
                  <View style={styles.buttonWrapper}>
                    <View style={styles.askButton}>
                      <Text style={styles.confirm}>NO</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Modal
            animationType="slide"
            visible={!!this.state.paymentVerificationUri}
            onRequestClose={() => {}}>
            <SafeAreaView
              style={{
                backgroundColor: RhypeColors.darkBlue,
                flex: 1,
              }}>
              <View
                style={{
                  backgroundColor: RhypeColors.darkBlue,
                  alignItems: 'flex-start',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ paymentVerificationUri: null });
                  }}
                  style={{ padding: 4 }}>
                  <Icon
                    name="chevron-left"
                    color={RhypeColors.copper}
                    size={34}
                  />
                </TouchableOpacity>
              </View>
              {this.state.paymentVerificationUri && (
                <WebView
                  onNavigationStateChange={e => {
                    if (e.url === 'http://dev.rhype.ph/login') {
                      this.props.fetchProfileDetails({
                        token: this.props.loginData.token,
                      });
                      this.setState({
                        paymentVerificationUri: null,
                        isVerificationSubmitted: true,
                      });
                      setTimeout(() => {
                        Alert.alert('', 'Payment successful.', [
                          {
                            text: 'OK',
                            onPress: () => {
                              this.validateRequest();
                            },
                          },
                        ]);
                      }, 2000);
                    }
                  }}
                  source={{
                    uri: this.state.paymentVerificationUri,
                  }}
                />
              )}
            </SafeAreaView>
          </Modal>
          <View style={styles.detailContainer}>
            <View style={styles.inputWrapper}>
              <View style={styles.askInputWrapper}>
                <Text style={styles.askCurrency}>PHP</Text>
                <View style={{ flex: 1, paddingLeft: 10 }}>
                  <TextInput
                    placeholderTextColor={RhypeColors.gray}
                    placeholder={'SELL AMOUNT'}
                    style={styles.sellAmount}
                    keyboardType="numeric"
                    onChangeText={price => this.setState({ price })}
                    value={this.state.price}
                  />
                </View>
              </View>
              <View style={styles.selectionWrapper}>
                <View style={styles.selectionItem}>
                  <Text style={styles.selectorText}>
                    ASK DURATION
                  </Text>
                  <View style={{ flexDirection: 'row' }}>
                    <TextInput
                      style={styles.durationAmount}
                      onChangeText={duration =>
                        this.setState({ duration })
                      }
                      value={this.state.duration}
                      keyboardType="numeric"
                      clearTextOnFocus
                    />
                  </View>
                </View>
                <TouchableOpacity
                  style={styles.selectionItem}
                  onPress={() =>
                    this.props.navigation.navigate('paymentDetails')
                  }>
                  <Text
                    style={
                      this.state.paymentMethodComplete === true
                        ? styles.selectorText
                        : styles.selectorTextError
                    }>
                    PAYMENT METHOD
                  </Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Text
                      style={[
                        styles.selectorText,
                        { color: RhypeColors.darkBlue },
                      ]}>
                      EDIT
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.selectionItem}
                  onPress={() =>
                    this.props.navigation.navigate('billingAddress')
                  }>
                  <Text
                    style={
                      this.state.shippingAddressComplete === true
                        ? styles.selectorText
                        : styles.selectorTextError
                    }>
                    BILLING ADDRESS
                  </Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Text
                      style={[
                        styles.selectorText,
                        { color: RhypeColors.darkBlue },
                      ]}>
                      EDIT
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.additionalDetailsContainer}>
                <View style={styles.additionalDetailItem}>
                  <Text style={styles.additionalDetailLabel}>
                    SHIPPING
                  </Text>
                  <Text style={styles.additionalDetailValue}>
                    250
                  </Text>
                </View>
                <View
                  style={[
                    styles.additionalDetailItem,
                    { borderTopWidth: 0, borderBottomWidth: 0 },
                  ]}>
                  <Text style={styles.additionalDetailLabel}>
                    AUTHENTICATION
                  </Text>
                  <Text style={styles.additionalDetailValue}>
                    FREE
                  </Text>
                </View>
                {false && (
                  <View style={styles.additionalDetailItem}>
                    <Text style={styles.additionalDetailLabel}>
                      ENTER PROMO CODE
                    </Text>
                    <Text style={styles.additionalDetailValue}>
                      -
                    </Text>
                  </View>
                )}
                <View style={styles.totalWrapper}>
                  <Text style={styles.totalLabel}>TOTAL</Text>
                  <Text style={styles.totalAmount}>
                    {250 + Number(this.state.price)}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        <TouchableOpacity
          style={styles.buttonPlaceAsk}
          onPress={() => this.validateRequest()}>
          <Text style={styles.buttonText}>PLACE ASK</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container: {
    flex: 9,
  },
  detailContainer: {
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  buttonWrapper: {
    flexDirection: 'row',
    height: 50,
    marginVertical: 10,
  },
  askButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.darkBlue,
    zIndex: 0,
  },
  okButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.darkBlue,
    zIndex: 0,
  },
  askButtonText: {
    fontFamily: fonts.UniformCondensed.regular,
    fontSize: 24,
    color: 'white',
  },
  circularOr: {
    fontFamily: fonts.UniformCondensed.regular,
    fontSize: 24,
    color: RhypeColors.darkBlue,
    textAlign: 'center',
  },
  circularView: {
    backgroundColor: RhypeColors.white,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: RhypeColors.white,
    paddingVertical: 10,
    width: 40,
    height: 40,
    zIndex: 1,
    position: 'absolute',
    top: 5,
    left: '45%',
    right: '40%',
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sellNowButtonText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 24,
    color: 'white',
  },
  sellNowButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 0,
  },
  sellNowButtonEnabled: {
    backgroundColor: RhypeColors.copper,
  },
  sellNowButtonDisabled: {
    backgroundColor: RhypeColors.gray,
  },
  askInputWrapper: {
    flexDirection: 'row',
  },
  askCurrency: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 36,
    color: RhypeColors.darkBlue,
  },
  sellAmount: {
    fontSize: 36,
    fontFamily: fonts.UniformCondensed.light,
    fontWeight: 'normal',
    marginBottom: 10,
    color: RhypeColors.darkBlue,
    borderBottomColor: RhypeColors.gray,
    borderBottomWidth: 1,
  },
  durationAmount: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.copper,
    paddingRight: 10,
  },
  additionalDetailsContainer: {},
  additionalDetailItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  additionalDetailLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  additionalDetailValue: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },

  totalWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    paddingTop: 10,
  },
  totalLabel: {
    ...fonts.UniformCondensed.bold,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  totalAmount: {
    ...fonts.UniformCondensed.bold,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  selectionWrapper: {
    marginVertical: 20,
  },
  selectionItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    paddingVertical: 4,
    borderTopWidth: 1,
    borderTopColor: RhypeColors.gray,
    borderBottomWidth: 1,
    borderBottomColor: RhypeColors.gray,
  },
  selectorText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  selectorTextError: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.red,
  },
  buttonPlaceAsk: {
    backgroundColor: RhypeColors.darkBlue,
    alignItems: 'center',
  },
  buttonText: {
    color: RhypeColors.white,
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    textAlign: 'center',
    paddingVertical: 10,
  },
  askModal: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  reminderModal: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  smallerModal: {
    width: 350,
    height: 150,
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: RhypeColors.white,
  },
  modalContent: {
    width: 350,
    height: 400,
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: RhypeColors.white,
  },
  askSpiel: {
    flex: 0.8,
    marginTop: 20,
    marginBottom: 30,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  detailsAskModal: {
    flex: 0.8,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  exit: {
    flex: 0.2,
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
  },
  exitText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.copper,
    marginHorizontal: 5,
    marginVertical: 5,
  },
  textModal: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    textAlign: 'center',
    color: RhypeColors.darkBlue,
  },
  label: {
    ...fonts.UniformCondensed.bold,
    fontSize: 22,
    textAlign: 'left',
    color: RhypeColors.darkBlue,
  },
  textDetails: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    textAlign: 'right',
    color: RhypeColors.darkBlue,
  },
  confirm: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    width: 100,
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.white,
    textAlign: 'center',
    paddingVertical: 10,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  userDetails: state.ProfileDataReducer,
});

const mapDispatchToProps = dispatch => ({
  fetchProfileDetails: userDetails =>
    dispatch(fetchProfileDetails(userDetails)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoeAskConfirmationFinancialSection);
