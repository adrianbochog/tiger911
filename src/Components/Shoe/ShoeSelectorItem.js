/**
 * Created by adrianperez on 21/04/2018.
 */
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';

export default class ShoeSelectorItem extends Component {
  state = { isSelected: this.props.isSelected };

  onPress() {
    this.toggleColor();
    this.props.updateShoeDetails(
      this.props.product.id,
      this.props.product.shoe_size
    );
    this.props.closeModal();
  }

  toggleColor() {
    this.setState({
      isSelected: !this.state.isSelected,
    });
  }

  isDisabled() {
    return !this.props.product;
  }

  render() {
    return (
      <TouchableOpacity
        style={[
          styles.shoeSizeButton,
          {
            backgroundColor: this.isDisabled()
              ? 'grey'
              : this.state.isSelected
                ? RhypeColors.copper
                : RhypeColors.darkBlue,
          },
        ]}
        onPress={() => this.onPress()}
        disabled={this.isDisabled()}>
        <Text style={styles.shoeSizeButtonText}>
          {this.props.value}
        </Text>
        <Text style={styles.detailButtonText}>
          {this.props.price || this.props.pricePlaceholder}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  shoeSizeButton: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 2,
    margin: 2,
    width: 75,
  },
  shoeSizeButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 22,
    color: 'white',
  },
  detailButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 14,
    color: 'white',
  },
});
