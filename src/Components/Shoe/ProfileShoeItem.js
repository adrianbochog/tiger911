/**
 * Created by adrianperez on 04/04/2018.
 */
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';
import { currencyFormatter } from '../../Utility/Utility';

export default class ProfileShoeItem extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.shoeDetailWrapper}>
          <View style={styles.imageWrapper}>
            <Image
              resizeMode="contain"
              source={
                this.props.imageUri
                  ? { uri: this.props.imageUri }
                  : require('../../../assets/images/placeholder/shoe-square.png')
              }
              style={styles.image}
            />
          </View>

          <View style={styles.detailsWrapper}>
            <View style={{ flex: 5 }}>
              <View style={styles.brandWrapper}>
                <Text style={styles.brand}>
                  {this.props.brand ? this.props.brand : 'BRAND'}
                </Text>
              </View>
              <View style={styles.modelTypeWrapper}>
                <Text style={styles.modelType} numberOfLines={2}>
                  {this.props.modelType
                    ? this.props.modelType
                    : 'MODEL/TYPE'}
                </Text>
              </View>
              <View style={styles.colorVersionWrapper}>
                <Text style={styles.colorVersion}>
                  {this.props.colorVersion
                    ? this.props.colorVersion
                    : 'COLOR/VERSION'}
                </Text>
              </View>
            </View>

            <View style={{ flex: 4 }}>
              <Text style={styles.status}>
                {this.props.status || ''}
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.marketDetailsWrapper}>
          <View style={styles.bidDetailsWrapper}>
            <Text style={styles.marketDetailLabel}>
              {this.props.marketDetailLabel || ' '}
            </Text>
            <Text style={styles.marketDetailAmount}>
              {currencyFormatter(this.props.marketDetailAmount)}
            </Text>
            <Text style={styles.viewLabel}>
              {this.props.viewLabel || ''}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 120,
    flexDirection: 'row',
    paddingTop: 10,
    paddingHorizontal: 20,
    marginBottom: -10,
  },
  imageWrapper: {
    height: 90,
    width: 90,
  },
  shoeDetailWrapper: {
    flex: 2,
    flexDirection: 'row',
  },
  image: {
    flex: 1,
    height: 90,
    width: 90,
  },
  detailsWrapper: {
    flex: 1,
    paddingLeft: 20,
    paddingTop: 10,
  },
  detailsTextWrapper: {
    // lineHeight:-20,
  },
  marketDetailsWrapper: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 0,
    margin: 0,
  },
  brandWrapper: {
    margin: 0,
    padding: 0,
  },
  brand: {
    ...fonts.UniformCondensed.bold,
    fontSize: 10,
    color: 'black',
    marginBottom: -5,
  },
  modelTypeWrapper: {
    padding: 0,
    margin: 0,
  },
  modelType: {
    ...fonts.UniformCondensed.bold,
    fontSize: 14,
    color: 'black',
    marginBottom: -5,
  },
  colorVersionWrapper: {
    margin: 0,
    padding: 0,
    paddingBottom: 5,
  },
  colorVersion: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 10,
    color: 'black',
  },
  status: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 10,
    color: 'black',
  },
  soldDetailsWrapper: {
    flex: 1,
  },
  soldLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 10,
    color: 'black',
  },
  soldAmount: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 10,
    color: 'black',
  },

  bidDetailsWrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    paddingTop: 10,
  },
  marketDetailLabel: {
    ...fonts.UniformCondensed.bold,
    fontSize: 10,
    marginBottom: -6,
    color: 'black',
  },
  marketDetailAmount: {
    ...fonts.UniformCondensed.bold,
    fontSize: 22,
    color: 'black',
  },
  viewLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 10,
    color: RhypeColors.copper,
  },
});
