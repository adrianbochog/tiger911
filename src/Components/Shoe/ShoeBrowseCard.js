/**
 * Created by adrianperez on 13/03/2018.
 */
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

import fonts from '../../Commons/RhypeFonts';

export default class ShoeCard extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imageWrapper}>
          <Image
            source={
              this.props.imageUri ||
              require('../../../assets/images/placeholder/shoe-square.png')
            }
            style={styles.image}
            resizeMode="cover"
          />
        </View>
        <View style={styles.contentWrapper}>
          <View style={styles.detailsWrapper}>
            <View style={styles.brandWrapper}>
              <Text numberOfLines={1} style={styles.brand}>
                {this.props.brand || 'BRAND'}
              </Text>
            </View>
            <View style={styles.modelTypeWrapper}>
              <Text numberOfLines={1} style={styles.modelType}>
                {this.props.modelType || 'MODEL/TYPE'}
              </Text>
            </View>
            <View style={styles.colorVersionWrapper}>
              <Text numberOfLines={1} style={styles.colorVersion}>
                {this.props.colorVersion || 'COLOR/VERSION'}
              </Text>
            </View>
          </View>

          <View style={styles.marketDetailsWrapper}>
            <View style={styles.soldDetailsWrapper}>
              <Text>
                <Text style={styles.soldLabel}>
                  SOLD
                  {'     '}
                </Text>
                <Text style={styles.soldAmount}>
                  {this.props.soldAmount || '###'}
                </Text>
              </Text>
            </View>
            <View style={styles.priceDetailsWrapper}>
              <Text style={styles.priceLabel}>PRICE</Text>
              <Text style={styles.priceAmount}>
                {this.props.priceAmount || '12,345'}
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 120,
    height: 200,
    // backgroundColor: 'yellow'
  },
  imageWrapper: {
    flex: 6,
  },
  contentWrapper: {
    flex: 5,
    padding: 0,
    justifyContent: 'flex-start',
  },
  image: {
    flex: 1,
    width: 120,
  },
  detailsWrapper: {
    flex: 1,
    justifyContent: 'space-evenly',
    padding: 0,
    margin: 0,
  },
  detailsTextWrapper: {
    // lineHeight:-20,
  },
  marketDetailsWrapper: {
    flex: 1,
    justifyContent: 'center',
    // backgroundColor: 'green',
    flexDirection: 'row',
    padding: 0,
    margin: 0,
    marginTop: -6,
  },
  brandWrapper: {
    margin: 0,
    padding: 0,
    // backgroundColor: 'green'
  },
  brand: {
    ...fonts.UniformCondensed.bold,
    fontSize: 10,
    // backgroundColor : 'red',
    marginBottom: -6,
  },
  modelTypeWrapper: {
    padding: 0,
    margin: 0,
  },
  modelType: {
    ...fonts.UniformCondensed.bold,
    fontSize: 14,
    marginBottom: -5,
    // backgroundColor : 'red'
  },
  colorVersionWrapper: {
    margin: 0,
    padding: 0,
    paddingBottom: 5,
  },
  colorVersion: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 12,
    // backgroundColor : 'red'
  },
  soldDetailsWrapper: {
    flex: 1,
  },
  soldLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 12,
  },
  soldAmount: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 12,
  },

  priceDetailsWrapper: {
    flex: 1,
    alignItems: 'flex-end',
  },
  priceLabel: {
    ...fonts.UniformCondensed.bold,
    fontSize: 12,
    marginBottom: -6,
  },
  priceAmount: {
    ...fonts.UniformCondensed.bold,
    fontSize: 22,
  },
});
