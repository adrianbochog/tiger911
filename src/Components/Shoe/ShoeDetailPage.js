/**
 * Created by adrianperez on 14/04/2018.
 */
import React, { Component } from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';
import Modal from 'react-native-modal';
import * as BrowseFilterData from '../../Config/Data/BrowseFilterData';
import ShoeCard from './ShoeCard';
import ShoeSelectorItem from './ShoeSelectorItem';

export default class ShoeDetailPage extends Component {
  render() {
    const data = this.props.other_variants || [];
    const image = this.props.images
      ? { uri: this.props.images.image }
      : {};
    const shoeSizeButtons = data.map(variant => (
      <ShoeSelectorItem
        key={variant.shoe_size}
        value={variant.shoe_size}
        product={variant}
        pricePlaceholder="###"
        price={variant.market_details.ask.lowest}
        isSelected={this.props.getShoeSize() === variant.shoe_size}
        closeModal={this.props.closeModal}
        updateShoeDetails={this.props.updateShoeDetails}
      />
    ));

    const lastSale =
      this.props.transaction_history &&
      this.props.transaction_history.length > 0 &&
      this.props.transaction_history[
        this.props.transaction_history.length - 1
      ].asking_price;

    // console.log("shoe detail", this.props, lastSale)
    return (
      <ScrollView style={styles.container}>
        <View style={styles.productDetailsContainer}>
          <View style={styles.productImageWrapper}>
            <Image
              source={image}
              style={styles.image}
              resizeMode="center"
            />
          </View>
          <View style={styles.productDetailsWrapper}>
            <View style={{ flex: 3 }}>
              <Text>
                <Text style={styles.typeStyle}>
                  {this.props.product && this.props.product.brand.name
                    ? this.props.product.brand.name
                    : '###'}
                </Text>
              </Text>
              <Text>
                <Text style={styles.brandModel}>
                  {this.props.product
                    ? this.props.product.name
                    : '###'}
                </Text>
              </Text>
              <Text>
                <Text>{'\n'}</Text>
                <Text style={styles.versionColor}>
                  {this.props.color ? this.props.color : '###'}
                </Text>
              </Text>
              <Text />

              <Text>
                <Text style={styles.retailPriceLabel}>
                  RETAIL PRICE:{' '}
                </Text>
                <Text style={styles.retailPriceValue}>
                  {this.props.retail_price || '###'}
                </Text>
              </Text>
              <Text>
                <Text style={styles.releaseDateLabel}>
                  RELEASE DATE:{' '}
                </Text>
                <Text style={styles.releaseDateValue}>
                  {this.props.release_date || '###'}
                </Text>
              </Text>
            </View>
            <TouchableOpacity
              style={styles.sizeDetailsWrapper}
              onPress={() => this.props.toggleModal()}>
              <Text style={styles.sizeLabel}>SIZE</Text>
              <Text style={styles.sizeValue}>
                {this.props.shoe_size}
              </Text>
              <Text style={styles.sizeAction}>CHOOSE SIZE</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.marketDetailsContainer}>
          <View style={styles.marketDetailsWrapper}>
            <Text style={styles.lastSaleLabel}>LAST SALE:</Text>
            {lastSale != null && lastSale != false ? (
              <Text style={styles.lastSaleValue}>
                PHP {lastSale || '###'}
              </Text>
            ) : (
              <Text style={styles.lastSaleValue}>None</Text>
            )}
            {false && (
              <Text style={styles.lastSaleNetChange}>
                {this.props.lastSaleNetChangeValue || '###'}(
                {this.props.lastSaleNetChangePercentage || '##'}
                %)
              </Text>
            )}
            {/* <Text style={styles.viewSalesLabel}>VIEW ALL SALES</Text> */}
          </View>
          <View style={styles.marketButtonsWrapper}>
            <TouchableOpacity
              style={styles.buyButton}
              onPress={() =>
                this.props.navigateToBidConfirmation({
                  id: this.props.id,
                  brand: this.props.brand,
                  name: this.props.name,
                  colorway: this.props.colorway,
                  retailPrice: this.props.retail_price,
                  imageUri: this.props.imageUri,
                })
              }>
              <Text style={styles.buttonText}>BUY</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.sellButton}
              onPress={() =>
                this.props.navigateToAskConfirmation({
                  id: this.props.id,
                  brand: this.props.brand,
                  name: this.props.name,
                  colorway: this.props.colorway,
                  retailPrice: this.props.retail_price,
                  imageUri: this.props.imageUri,
                })
              }>
              <Text style={styles.buttonText}>SELL</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={cardStyles.container}>
          <Text style={styles.typeStyle}>Related</Text>
          {Array.isArray(this.props.related) &&
          this.props.related.length > 0 ? (
            <FlatList
              horizontal
              data={this.props.related}
              initialNumToRender={4}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={cardStyles.cardButton}
                  onPress={() =>
                    this.props.navigation.navigate('shoeDetail', {
                      itemId: item.id,
                    })
                  }>
                  <ShoeCard
                    brand={item.product.brand.name}
                    modelType={item.product.name}
                    colorVersion={item.color}
                    soldAmount={item.sold}
                    priceAmount={item.market_details.lowest}
                    imageUri={item.images.image}
                  />
                </TouchableOpacity>
              )}
              keyExtractor={(item, index) => item.id.toString()}
            />
          ) : (
            <Text style={styles.priceAmount}>None</Text>
          )}
        </View>
        <View style={cardStyles.container}>
          <Text style={styles.typeStyle}>Latest Sales</Text>
          {Array.isArray(this.props.transaction_history) &&
          this.props.transaction_history.length > 0 ? (
            <FlatList
              data={this.props.transaction_history.slice(0, 10)}
              initialNumToRender={4}
              ListHeaderComponent={() => (
                <View style={styles.tableRow}>
                  <Text>Date</Text>
                  <Text>Price</Text>
                </View>
              )}
              renderItem={({ item }) => (
                <View style={styles.tableRow}>
                  <Text>{new Date(item.created).toDateString()}</Text>
                  <Text>{item.bid_price}</Text>
                </View>
              )}
              keyExtractor={(item, index) => item.created.toString()}
            />
          ) : (
            <Text style={styles.priceAmount}>None</Text>
          )}
        </View>
        <Modal isVisible={this.props.isModalVisible()}>
          <View style={styles.modalContainer}>
            <View style={styles.headerWrapper}>
              <Text style={styles.modalHeaderText}>CHOOSE SIZE</Text>
            </View>
            <View style={styles.contentWrapper}>
              <ScrollView>
                <View style={styles.sizeSelectorContainer}>
                  {shoeSizeButtons}
                </View>
              </ScrollView>
            </View>
          </View>
        </Modal>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
  },
  productDetailsContainer: {
    // backgroundColor: 'red',
    flex: 1,
    alignItems: 'center',
  },
  productDetailsWrapper: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'flex-start',
    flexDirection: 'row',
    marginBottom: 10,
  },
  productImageWrapper: {
    marginBottom: 10,
  },
  image: {
    width: 316,
    height: 256,
  },
  sizeDetailsWrapper: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.copper,
    paddingVertical: 10,
    borderRadius: 10,
  },
  sizeLabel: {
    fontFamily: fonts.UniformCondensed.regular,
    fontSize: 22,
    color: 'white',
  },
  sizeValue: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 36,
    color: 'white',
  },
  sizeAction: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 20,
    color: 'white',
  },
  brandModel: {
    fontFamily: fonts.UniformCondensed.regular,
    fontSize: 30,
    color: 'black',
  },
  typeStyle: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: 'black',
  },
  versionColor: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: 'black',
  },
  retailPriceLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 14,
    color: 'black',
  },
  retailPriceValue: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 14,
    color: 'black',
  },
  releaseDateLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 14,
    color: 'black',
  },
  releaseDateValue: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 14,
    color: 'black',
  },

  marketDetailsContainer: {
    flexDirection: 'row',
    flex: 1,
  },
  marketDetailsWrapper: {
    flex: 1,
    alignItems: 'flex-start',
  },
  marketButtonsWrapper: {
    flex: 1,
    alignItems: 'flex-end',
    paddingBottom: 20,
  },
  lastSaleLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: 'black',
  },
  lastSaleValue: {
    fontFamily: fonts.UniformCondensed.regular,
    fontSize: 30,
    color: 'black',
  },
  lastSaleNetChange: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: 'black',
    marginBottom: 15,
  },
  viewSalesLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: 'black',
  },
  buyButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: RhypeColors.darkBlue,
    borderRadius: 10,
    marginBottom: 10,
  },
  sellButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: RhypeColors.copper,
    borderRadius: 10,
  },
  buttonText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: 'white',
  },

  modalContainer: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  headerWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.copper,
  },
  contentWrapper: {
    flex: 8,
    paddingVertical: 10,
  },
  footerWrapper: {
    flex: 1,
  },
  closeModalButton: {
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },

  modalHeaderText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 28,
    color: 'white',
  },
  filterTypeHeaderText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 28,
    color: RhypeColors.darkBlue,
  },
  selectorButton: {
    marginHorizontal: 5,
    backgroundColor: RhypeColors.darkBlue,
    paddingHorizontal: 4,
  },
  selectorButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: 'white',
  },
  sizeSelectorContainer: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  sizeSelectorRow: {
    flexDirection: 'row',
    marginVertical: 2,
  },
  sizeButton: {
    width: 32,
    marginHorizontal: 3,
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sizeButtonText: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    color: 'white',
  },
  tableRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const cardStyles = StyleSheet.create({
  container: {
    marginTop: 10,
    justifyContent: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 36,
    color: RhypeColors.darkBlue,
  },
  cardButton: {
    marginRight: 10,
  },
});
