/**
 * Created by adrianperez on 13/03/2018.
 */
import React, { Component } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { currencyFormatter } from '../../Utility/Utility';

import fonts from '../../Commons/RhypeFonts';

export default class ShoeAskCard extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imageWrapper}>
          <Image
            source={
              this.props.imageUri
                ? { uri: this.props.imageUri }
                : require('../../../assets/images/placeholder/shoe-square.png')
            }
            style={styles.image}
            resizeMode="contain"
          />
        </View>
        <View style={styles.contentWrapper}>
          <View style={styles.detailsWrapper}>
            <Text numberOfLines={1} style={styles.brand}>
              {this.props.brand || 'BRAND'}
            </Text>
            <Text numberOfLines={1} style={styles.modelType}>
              {this.props.modelType || 'MODEL/TYPE'}
            </Text>
            <Text numberOfLines={1} style={styles.colorVersion}>
              {this.props.colorVersion || 'COLOR/VERSION'}
            </Text>
          </View>

          <View style={styles.marketDetailsWrapper}>
            <View style={styles.soldDetailsWrapper}>
              <Text style={styles.soldLabel}>
                {this.props.status.toUpperCase()}
              </Text>
              <Text style={styles.priceLabel}>ASK</Text>
            </View>
            <Text style={styles.priceAmount}>
              {currencyFormatter(this.props.priceAmount)}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: 110,
    height: 195,
    padding: 2.5,
  },
  imageWrapper: {
    flex: 6,
  },
  contentWrapper: {
    flex: 5,
    padding: 0,
    justifyContent: 'flex-start',
  },
  image: {
    flex: 1,
    width: 115,
  },
  detailsWrapper: {
    flex: 1,
    justifyContent: 'space-evenly',
    paddingVertical: 6,
  },
  detailsTextWrapper: {
    // lineHeight:-20,
  },
  marketDetailsWrapper: {
    flex: 1,
    justifyContent: 'center',
  },
  brand: {
    fontSize: 10,
    ...fonts.UniformCondensed.bold,
    color: 'black',
  },
  modelType: {
    ...fonts.UniformCondensed.bold,
    fontSize: 14,
    color: 'black',
  },
  colorVersion: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 12,
    color: 'black',
  },
  soldDetailsWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  soldLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 12,
    color: 'black',
  },
  soldAmount: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 12,
    color: 'black',
  },

  priceDetailsWrapper: {
    flex: 1,
  },
  priceLabel: {
    ...fonts.UniformCondensed.bold,
    fontSize: 12,
    color: 'black',
    alignSelf: 'flex-end',
  },
  priceAmount: {
    ...fonts.UniformCondensed.bold,
    fontSize: 20,
    color: 'black',
    alignSelf: 'flex-end',
    marginTop: -6,
  },
});
