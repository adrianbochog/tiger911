import React, { Component } from 'react';
import {
  Alert,
  Modal,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  WebView,
} from 'react-native';

import { connect } from 'react-redux';
import SwitchToggle from 'react-native-switch-toggle';
import { Icon } from 'react-native-elements';

import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';
import { fetchProfileDetails } from '../../Actions/actionCreator';
import {
  fetchTransactionConfig,
  postBuyNowData,
  postPayment,
} from '../../Services/TransactionService';
import PaymentOptions, {
  PaymentOptionPicker,
} from '../../PaymentOptions';
import { fetchVerifyParams } from '../../Services/ProfileService';

class ShoeConfirmationFinancialSection extends Component {
  constructor(props) {
    super(props);

    const paymentOption = props.user.payment_preference;

    this.state = {
      id: '',
      price: '',
      duration: '14',
      modalVisible: false,
      paymentVerificationUri: null,
      isBuyNow: false,
      shippingAddressComplete: false,
      reminderModal: false,
      successModal: false,
      shippingCost: 250,
      transactionPercentage: 0,
      vatPercentage: 12,
      buySource: null,
      paymentOption,
      isVerificationSubmitted: false,
    };
  }

  componentDidMount() {
    this.props.fetchProfileDetails({
      token: this.props.loginData.token,
    });

    fetchTransactionConfig().then(
      ({
        shipping_cost: shippingCost,
        transaction_percentage: transactionPercentage,
        vat_percentage: vatPercentage,
      }) =>
        this.setState({
          shippingCost,
          transactionPercentage,
          vatPercentage,
        })
    );
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  toggleIsBuyNow = () => {
    const price = this.state.isBuyNow ? '' : this.props.lowestAsk;
    this.setState({
      isBuyNow: !this.state.isBuyNow,
      price,
    });
  };

  submitBid = () => {
    this.props.submitBid({
      variantId: this.props.id,
      price: this.state.price,
      duration: this.state.duration,
    });
  };

  submitBuyNow = () => {};

  submit = () => {
    this.setState({
      modalVisible: false,
    });

    this.submitBid();
  };

  getPrice = () => {
    if (this.state.isBuyNow) {
      return this.props.lowestAsk;
    } else {
      return this.state.price;
    }
  };

  renderSubmitButton = () => {
    if (!this.state.isBuyNow) {
      return (
        <TouchableOpacity
          style={styles.buttonPlaceBid}
          onPress={() => this.validateRequest()}>
          <Text style={styles.buttonText}>PLACE BID</Text>
        </TouchableOpacity>
      );
    } else {
      const lowestAsk =
        this.props.lowestAsk &&
        this.props.lowestAsk &&
        parseInt(this.props.lowestAsk);
      const ask = this.state.price && parseInt(this.state.price);
      const enabled = lowestAsk && ask && ask >= lowestAsk;
      return (
        <TouchableOpacity
          style={[
            styles.buttonPlaceBid,
            enabled
              ? styles.buyNowButtonEnabled
              : styles.buyNowButtonDisabled,
          ]}
          disabled={!enabled}
          onPress={() => this.validateRequest()}>
          <Text style={styles.buttonText}>BUY NOW</Text>
        </TouchableOpacity>
      );
    }
  };

  validateRequest = () => {
    const userDetailsFetched = this.props.userDetails
      .userDetailsFetched;
    // console.log('details fetched', userDetailsFetched);
    if (userDetailsFetched) {
      const { data, userDetails } = this.props.userDetails;
      const { shipping_address: shippingAddress } = userDetails;
      const { is_verified: isVerified } = data;

      if (shippingAddress == null) {
        Alert.alert(
          'Error',
          'Please complete delivery address before bidding',
          [
            {
              text: 'OK',
              onPress: () => {
                setTimeout(
                  () =>
                    this.props.navigation.navigate('shippingAddress'),
                  1000
                );
              },
            },
          ]
        );
        return;
      }
      if (this.state.price == null || Number(this.state.price) <= 0) {
        Alert.alert('Error', 'Please input a proper price');
        return;
      }

      if (!(isVerified || this.state.isVerificationSubmitted)) {
        Alert.alert(
          '',
          'Thank you for trying out the bidding/selling functionality of Rhype. In order for your bid/ask to push thru, we would be requiring you to pay a small fee of $1.00 (roughly ₱55) via credit card or paypal to maintain the fair use policy of the platform. Take note that this amount would be reimbursed once a successful transaction pushed thru.',
          [
            {
              text: 'OK',
              onPress: () => {
                fetchVerifyParams(this.props.loginData.token).then(
                  ({ payment_url: paymentUrl, params }) => {
                    const query = Object.keys(params)
                      .map(key => {
                        return `${encodeURIComponent(
                          key
                        )}=${encodeURIComponent(params[key])}`;
                      })
                      .join('&');
                    const uri = `${paymentUrl}?${query}`;
                    this.setState({ paymentVerificationUri: uri });
                  }
                );
              },
            },
          ]
        );
        return;
      }

      if (this.state.isBuyNow) {
        const token = this.props.loginData.token;
        postBuyNowData({
          token,
          variant: this.props.id,
        }).then(async tx => {
          const buySource = await postPayment({
            token,
            paymentId: tx.id,
            paymentOption: this.state.paymentOption,
          });

          this.setState({ buySource, modalVisible: true });
        });
      } else {
        this.setState({ modalVisible: true });
      }
    }
  };

  render() {
    const {
      shippingCost,
      transactionPercentage,
      vatPercentage,
      isBuyNow,
      paymentOption,
      buySource,
    } = this.state;
    const transactionFee =
      (this.state.price * transactionPercentage) / 100;
    const vat = (this.state.price * vatPercentage) / 100;

    return (
      <View style={styles.wrapper}>
        <View style={styles.container}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.reminderModal}
            onRequestClose={() => {
              this.setState({ reminderModal: false });
            }}>
            <View style={styles.reminderModal}>
              <View style={styles.smallerModal}>
                <View style={styles.exit}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ reminderModal: false });
                    }}>
                    <Text style={styles.exitText}>X</Text>
                  </TouchableOpacity>
                </View>
                <Text style={styles.textModal}>
                  {'Please input Delivery Address.'}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ reminderModal: false });
                  }}>
                  <View style={styles.buttonWrapper}>
                    <View style={styles.okButton}>
                      <Text style={styles.confirm}>OK</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              this.setState({
                modalVisible: false,
                buySource: null,
              });
            }}>
            {isBuyNow && buySource ? (
              <View
                style={{ width: '100%', height: '100%', flex: 1 }}>
                <SafeAreaView
                  style={{ backgroundColor: RhypeColors.darkBlue }}>
                  <View
                    style={{
                      backgroundColor: RhypeColors.darkBlue,
                      alignItems: 'flex-start',
                      justifyContent: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState(
                          { buySource: null, modalVisible: false },
                          () =>
                            setTimeout(() => {
                              Alert.alert('', 'Payment cancelled.');
                            }, 1000)
                        );
                      }}
                      style={{ padding: 4 }}>
                      <Icon
                        name="chevron-left"
                        color={RhypeColors.copper}
                        size={34}
                      />
                    </TouchableOpacity>
                  </View>
                </SafeAreaView>
                <WebView
                  style={{ width: '100%', height: '100%', flex: 1 }}
                  onNavigationStateChange={e => {
                    if (e.url === 'http://uat.rhype.ph/') {
                      this.setState({
                        buySource: null,
                        modalVisible: false,
                      });
                      setTimeout(() => {
                        Alert.alert('', 'Payment successful.');
                      }, 1000);
                    }
                  }}
                  source={buySource}
                />
              </View>
            ) : (
              <View style={styles.bidModal}>
                <View style={styles.exit}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setModalVisible(!this.state.modalVisible);
                    }}>
                    <Text style={styles.exitText}>X</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.askSpiel}>
                  <Text style={styles.textModal}>
                    {'Are you sure you want to place bid?'}
                  </Text>
                  <View style={styles.detailsAskModal}>
                    <Text style={styles.label}> Price: </Text>
                    <Text style={styles.textDetails}>
                      {this.getPrice()
                        ? this.getPrice()
                        : 'No price added'}
                    </Text>
                    <View>
                      <Text style={styles.label}> Duration: </Text>
                      <Text style={styles.textDetails}>
                        {this.state.duration
                          ? this.state.duration
                          : 'No duration added'}
                      </Text>
                    </View>
                  </View>
                </View>
                <TouchableOpacity
                  onPress={() => this.submit()}
                  style={styles.buttonWrapper}>
                  <View style={styles.askButton}>
                    <Text style={styles.confirm}>YES</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ modalVisible: false });
                  }}
                  style={styles.buttonWrapper}>
                  <View style={styles.askButton}>
                    <Text style={styles.confirm}>NO</Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </Modal>
          <Modal
            animationType="slide"
            visible={!!this.state.paymentVerificationUri}
            onRequestClose={() => {}}>
            <SafeAreaView
              style={{
                backgroundColor: RhypeColors.darkBlue,
                flex: 1,
              }}>
              <View
                style={{
                  backgroundColor: RhypeColors.darkBlue,
                  alignItems: 'flex-start',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ paymentVerificationUri: null });
                  }}
                  style={{ padding: 4 }}>
                  <Icon
                    name="chevron-left"
                    color={RhypeColors.copper}
                    size={34}
                  />
                </TouchableOpacity>
              </View>
              {this.state.paymentVerificationUri && (
                <WebView
                  onNavigationStateChange={e => {
                    if (e.url === 'http://dev.rhype.ph/login') {
                      this.props.fetchProfileDetails({
                        token: this.props.loginData.token,
                      });
                      this.setState({
                        paymentVerificationUri: null,
                        isVerificationSubmitted: true,
                      });
                      setTimeout(() => {
                        Alert.alert('', 'Payment successful.', [
                          {
                            text: 'OK',
                            onPress: () => {
                              this.validateRequest();
                            },
                          },
                        ]);
                      }, 2000);
                    }
                  }}
                  source={{
                    uri: this.state.paymentVerificationUri,
                  }}
                />
              )}
            </SafeAreaView>
          </Modal>
          <View style={styles.switchWrapper}>
            <View style={styles.switchLabelWrapper}>
              <Text style={styles.switchLabel}>BID</Text>
            </View>
            <SwitchToggle
              switchOn={isBuyNow}
              onPress={this.toggleIsBuyNow}
              circleColorOn={'white'}
              circleColorOff={'white'}
              backgroundColorOn={RhypeColors.copper}
              backgroundColorOff={RhypeColors.darkBlue}
              disabled
            />
            <View style={styles.switchLabelWrapper}>
              <Text style={styles.switchLabel}>BUY NOW</Text>
            </View>
          </View>
          <View style={styles.detailContainer}>
            <View style={styles.inputWrapper}>
              <View style={styles.bidInputWrapper}>
                <Text style={styles.bidCurrency}>PHP</Text>
                <View style={{ flex: 1, paddingLeft: 10 }}>
                  <TextInput
                    placeholderTextColor={RhypeColors.gray}
                    placeholder={'AMOUNT'}
                    style={styles.bidAmount}
                    onChangeText={price => this.setState({ price })}
                    value={this.state.price}
                    keyboardType="numeric"
                    editable={!isBuyNow}
                  />
                </View>
              </View>
              <View style={styles.selectionWrapper}>
                {isBuyNow ? (
                  <TouchableOpacity
                    style={styles.selectionItem}
                    onPress={() =>
                      PaymentOptionPicker(paymentOption).then(
                        paymentOption => {
                          this.setState({ paymentOption });
                        }
                      )
                    }>
                    <Text style={styles.selectorText}>
                      PAYMENT OPTION
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text
                        style={[
                          styles.selectorText,
                          { color: RhypeColors.darkBlue },
                        ]}>
                        {paymentOption}
                      </Text>
                    </View>
                  </TouchableOpacity>
                ) : (
                  <View style={styles.selectionItem}>
                    <Text style={styles.selectorText}>
                      BID DURATION
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                      <TextInput
                        style={styles.durationAmount}
                        onChangeText={duration =>
                          this.setState({ duration })
                        }
                        value={this.state.duration}
                        keyboardType="numeric"
                        clearTextOnFocus
                      />
                    </View>
                  </View>
                )}
                <TouchableOpacity
                  style={styles.selectionItem}
                  onPress={() => this.props.navigate('Shipping')}>
                  <Text style={styles.selectorText}>
                    DELIVERY ADDRESS
                  </Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Text
                      style={[
                        styles.selectorText,
                        { color: RhypeColors.darkBlue },
                      ]}>
                      EDIT
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.additionalDetailsContainer}>
                <View style={styles.additionalDetailItem}>
                  <Text style={styles.additionalDetailLabel}>
                    {`VAT ${vatPercentage.toFixed(2)}%`}
                  </Text>
                  <Text style={styles.additionalDetailValue}>
                    {vat}
                  </Text>
                </View>
                <View style={styles.additionalDetailItem}>
                  <Text style={styles.additionalDetailLabel}>
                    SHIPPING
                  </Text>
                  <Text style={styles.additionalDetailValue}>
                    {shippingCost}
                  </Text>
                </View>
                <View style={styles.additionalDetailItem}>
                  <Text style={styles.additionalDetailLabel}>
                    TRANSACTION FEE
                  </Text>
                  <Text style={styles.additionalDetailValue}>
                    {transactionFee}
                  </Text>
                </View>
                <View
                  style={[
                    styles.additionalDetailItem,
                    { borderTopWidth: 0, borderBottomWidth: 0 },
                  ]}>
                  <Text style={styles.additionalDetailLabel}>
                    AUTHENTICATION
                  </Text>
                  <Text style={styles.additionalDetailValue}>
                    FREE
                  </Text>
                </View>
                {false && (
                  <View style={styles.additionalDetailItem}>
                    <Text style={styles.additionalDetailLabel}>
                      ENTER PROMO CODE
                    </Text>
                    <Text style={styles.additionalDetailValue}>
                      -
                    </Text>
                  </View>
                )}
                <View style={styles.totalWrapper}>
                  <Text style={styles.totalLabel}>TOTAL</Text>
                  <Text style={styles.totalAmount}>
                    {shippingCost +
                      transactionFee +
                      vat +
                      Number(this.state.price)}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        {this.renderSubmitButton()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
  buttonContainer: {
    flex: 1,
  },
  detailContainer: {
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  buttonWrapper: {
    flexDirection: 'row',
    height: 50,
    marginVertical: 10,
  },
  bidButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.darkBlue,
    zIndex: 0,
  },
  bidButtonText: {
    fontFamily: fonts.UniformCondensed.regular,
    fontSize: 24,
    color: 'white',
  },
  circularOr: {
    fontFamily: fonts.UniformCondensed.regular,
    fontSize: 24,
    color: RhypeColors.darkBlue,
    textAlign: 'center',
  },
  circularView: {
    backgroundColor: RhypeColors.white,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: RhypeColors.white,
    paddingVertical: 10,
    width: 40,
    height: 40,
    zIndex: 1,
    position: 'absolute',
    top: 5,
    left: '45%',
    right: '40%',
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buyNowButtonText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 24,
    color: 'white',
  },
  buyNowButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 0,
  },
  buyNowButtonEnabled: {
    backgroundColor: RhypeColors.copper,
  },
  buyNowButtonDisabled: {
    backgroundColor: RhypeColors.gray,
  },
  bidInputWrapper: {
    flexDirection: 'row',
  },
  bidCurrency: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 36,
    color: RhypeColors.darkBlue,
  },
  bidAmount: {
    fontSize: 36,
    fontFamily: fonts.UniformCondensed.light,
    fontWeight: 'normal',
    marginBottom: 10,
    color: RhypeColors.darkBlue,
    borderBottomColor: RhypeColors.gray,
    borderBottomWidth: 1,
  },
  durationPicker: {
    height: 50,
    width: 50,
  },
  durationAmount: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.copper,
    paddingRight: 10,
  },
  additionalDetailsContainer: {},
  additionalDetailItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  additionalDetailLabel: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  additionalDetailValue: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  totalWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    paddingTop: 10,
  },
  totalLabel: {
    ...fonts.UniformCondensed.bold,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  totalAmount: {
    ...fonts.UniformCondensed.bold,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  selectionWrapper: {
    marginVertical: 20,
  },
  selectionItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'stretch',
    paddingVertical: 4,
    borderTopWidth: 1,
    borderTopColor: RhypeColors.gray,
    borderBottomWidth: 1,
    borderBottomColor: RhypeColors.gray,
  },
  selectorText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.darkBlue,
  },
  buttonPlaceBid: {
    backgroundColor: RhypeColors.darkBlue,
    alignItems: 'center',
  },
  buttonBuyNow: {
    backgroundColor: RhypeColors.copper,
    alignItems: 'center',
  },
  buttonText: {
    color: RhypeColors.white,
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    textAlign: 'center',
    paddingVertical: 10,
  },
  bidModal: {
    flex: 1,
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal: 10,
  },
  bidSpiel: {
    flex: 0.8,
  },
  switchWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'space-evenly',
    width: 300,
    marginVertical: 20,
  },
  switchLabelWrapper: {
    justifyContent: 'center',
    width: 100,
  },
  switchLabel: {
    ...fonts.UniformCondensed.bold,
    fontSize: 18,
    textAlign: 'center',
    color: RhypeColors.darkBlue,
  },
  reminderModal: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  smallerModal: {
    width: 300,
    height: 150,
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: RhypeColors.white,
  },
  modalContent: {
    width: 350,
    height: 400,
    paddingHorizontal: 10,
    paddingVertical: 10,
    backgroundColor: RhypeColors.white,
  },
  askSpiel: {
    flex: 0.8,
    marginTop: 20,
    marginBottom: 30,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  detailsAskModal: {
    flex: 0.8,
    marginHorizontal: 10,
    alignItems: 'center',
  },
  exit: {
    flex: 0.2,
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
  },
  exitText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.copper,
    marginHorizontal: 5,
    marginVertical: 5,
  },
  textModal: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    textAlign: 'center',
    color: RhypeColors.darkBlue,
  },
  label: {
    ...fonts.UniformCondensed.bold,
    fontSize: 22,
    textAlign: 'left',
    color: RhypeColors.darkBlue,
  },
  textDetails: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    textAlign: 'right',
    color: RhypeColors.darkBlue,
  },
  confirm: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    width: 100,
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 22,
    color: RhypeColors.white,
    textAlign: 'center',
    paddingVertical: 10,
  },
  askButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.darkBlue,
    zIndex: 0,
  },
  okButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.darkBlue,
    zIndex: 0,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  userDetails: state.ProfileDataReducer,
});

const mapDispatchToProps = dispatch => ({
  fetchProfileDetails: userDetails =>
    dispatch(fetchProfileDetails(userDetails)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoeConfirmationFinancialSection);
