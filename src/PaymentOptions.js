import Picker from 'react-native-picker';

// const options = ['CARD', 'PAYPAL'];
const options = ['CARD'];

export default options;

export function PaymentOptionPicker(defaultValue = options[0]) {
  return new Promise((resolve, reject) => {
    Picker.init({
      pickerData: options,
      selectedValue: [defaultValue],
      pickerTitleText: 'Select Payment Option',
      onPickerConfirm: ([opt]) => {
        resolve(opt);
      },
    });

    Picker.show();
  });
}
