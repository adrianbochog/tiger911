import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';

import {
  addNavigationHelpers,
  NavigationActions,
} from 'react-navigation';
import { createReduxBoundAddListener } from 'react-navigation-redux-helpers';

import NavigationStack from './navigationStack';

const addListener = createReduxBoundAddListener('root');

class AppNavigation extends Component {
  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.onBackPress
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.onBackPress
    );
  }

  onBackPress = () => {
    const { dispatch, nav } = this.props;

    const curr = nav.routes[nav.index];

    if (curr && curr.index === 0) {
      BackHandler.exitApp();
      return false;
    }

    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    const { nav, dispatch, isLoggedIn } = this.props;
    const test = addNavigationHelpers({
      dispatch,
      addListener,
      state: nav,
    });
    return <NavigationStack navigation={test} />;
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.LoginReducer.isLoggedIn,
    nav: state.nav,
  };
};

export default connect(mapStateToProps)(AppNavigation);
