import { StackNavigator, SwitchNavigator } from 'react-navigation';

import React from 'react';
import Login from '../Screens/LoginScreen';
import Welcome from '../Screens/WelcomeScreen';
import EmailShoeSize from '../Screens/EmailShoeSize';
import { ProfileStack } from './ProfileNavigation';
import { HomeStack } from './HomeNavigation';
import RegistrationSuccessScreen from '../Screens/RegistrationSuccessScreen';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import * as RhypeColors from '../Commons/RhypeColors';
import RegistrationScreenPrimary from '../Screens/RegistrationScreenPrimary';
import RegistrationScreenSecondary from '../Screens/RegistrationScreenSecondary';

const WelcomeStack = StackNavigator(
  {
    welcome: {
      screen: Welcome,
    },
    login: {
      screen: Login,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
        headerStyle: { backgroundColor: RhypeColors.darkBlue },
      }),
    },
    registrationPrimary: {
      screen: RegistrationScreenPrimary,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon
              name="chevron-left"
              color={RhypeColors.copper}
              size={34}
            />
          </TouchableOpacity>
        ),
        headerStyle: { backgroundColor: RhypeColors.darkBlue },
      }),
    },
    registrationSecondary: {
      screen: RegistrationScreenSecondary,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon
              name="chevron-left"
              color={RhypeColors.copper}
              size={34}
            />
          </TouchableOpacity>
        ),
        headerStyle: { backgroundColor: RhypeColors.darkBlue },
      }),
    },
    registrationSuccess: {
      screen: RegistrationSuccessScreen,
      navigationOptions: {
        title: 'Registration Success',
        header: null,
      },
    },
    emailShoeSize: {
      screen: EmailShoeSize,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon
              name="chevron-left"
              color={RhypeColors.copper}
              size={34}
            />
          </TouchableOpacity>
        ),
        headerStyle: { backgroundColor: RhypeColors.darkBlue },
      }),
    },
  },
  {
    headerMode: 'screen',
  }
);

const navigator = SwitchNavigator({
  welcomeScreens: {
    screen: WelcomeStack,
  },
  mainScreens: {
    screen: HomeStack,
    navigationOptions: {
      header: null,
    },
  },
});

export default navigator;
