/**
 * Created by adrianperez on 05/04/2018.
 */
import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View,
} from 'react-native';
import {
  DrawerItems,
  DrawerNavigator,
  NavigationActions,
  StackNavigator,
  TabNavigator,
  TabBarBottom,
} from 'react-navigation';
import LogoutScreen from '../Components/Logout';
import {
  ProfileFollowingAndCollectionTabs,
  ProfileStack,
  ProfileTransactionTabs,
} from './ProfileNavigation';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';
import LogoTitle from '../Components/LogoTitle';
import ProfileButton from '../Components/ProfileButton';
import FavoritesScreen from '../Screens/FavoritesScreen';
import SettingsScreen from '../Screens/Profile/SettingsScreen';
import SupportScreen from '../Screens/Profile/SupportScreen';
import BrowseScreen from '../Screens/BrowseScreen';
import HomeScreen from '../Screens/HomeScreen';
import ShoeDetailScreen from '../Screens/Product/ShoeDetailScreen';
import Shipping from '../Screens/Shipping';
import BillingAddress from '../Screens/BillingAddress';
import PaymentDetails from '../Screens/PaymentDetails';
import { Icon } from 'react-native-elements';
import ShoeBidConfirmationScreen from '../Screens/Product/ShoeBidConfirmationScreen';
import ShoeAskConfirmationScreen from '../Screens/Product/ShoeAskConfirmationScreen';
import BidSuccessScreen from '../Screens/Product/BidSuccessScreen';
import AskSuccessScreen from '../Screens/Product/AskSuccessScreen';
import ProfileScreen from '../Screens/Profile/ProfileScreen';
import SearchScreen from '../Screens/Profile/SearchScreen';
import BuyNowSuccessScreen from '../Screens/Product/BuyNowSuccessScreen';
import AcceptBidSuccessScreen from '../Screens/Product/AcceptBidSuccessScreen';

const homeImage = require('../../assets/images/root-tab-home.png');
const homeAltImage = require('../../assets/images/root-tab-home-alt.png');
const browseImage = require('../../assets/images/root-tab-browse.png');
const browseAltImage = require('../../assets/images/root-tab-browse-alt.png');
const txnsImage = require('../../assets/images/root-tab-txns.png');
const txnsAltImage = require('../../assets/images/root-tab-txns-alt.png');
const searchImage = require('../../assets/images/root-tab-search.png');
const searchAltImage = require('../../assets/images/root-tab-search-alt.png');

const styles = StyleSheet.create({
  header: {
    backgroundColor: RhypeColors.darkBlue,
  },
  titleStyle: {
    color: RhypeColors.white,
    fontSize: 20,
    textAlign: 'center',
    fontFamily: fonts.UniformCondensed.regular,
    fontWeight: 'normal',
  },
  drawerItemStyle: {
    paddingVertical: 0,
  },
  drawerLabelStyle: {
    color: RhypeColors.darkBlue,
    fontFamily: fonts.UniformCondensed.light,
    fontWeight: 'normal',
    fontSize: 15,
  },
  labelStyle: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 12,
  },
  tabStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  bar: {
    borderRadius: 1,
    backgroundColor: RhypeColors.white,
  },
  indicatorStyle: {
    backgroundColor: RhypeColors.copper,
    height: 2,
  },
});

const drawerButton = navigation => (
  <Text
    style={{ padding: 5, color: RhypeColors.white }}
    onPress={() => navigation.navigate('DrawerToggle')}>
    Menu
  </Text>
);

const shoeScreens = {
  shoeDetail: {
    screen: ShoeDetailScreen,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Icon
            name="chevron-left"
            color={RhypeColors.copper}
            size={34}
          />
        </TouchableOpacity>
      ),
      headerStyle: styles.header,
      headerTitle: <LogoTitle />,
    }),
  },
  shoeBidConfirmation: {
    screen: ShoeBidConfirmationScreen,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Icon
            name="chevron-left"
            color={RhypeColors.copper}
            size={34}
          />
        </TouchableOpacity>
      ),
      headerStyle: styles.header,
      headerTitle: <LogoTitle />,
    }),
  },
  shoeAskConfirmation: {
    screen: ShoeAskConfirmationScreen,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Icon
            name="chevron-left"
            color={RhypeColors.copper}
            size={34}
          />
        </TouchableOpacity>
      ),
      headerStyle: styles.header,
      headerTitle: <LogoTitle />,
    }),
  },
  bidSuccess: {
    screen: BidSuccessScreen,
    navigationOptions: {
      headerMode: 'none',
    },
  },
  buyNowSuccess: {
    screen: BuyNowSuccessScreen,
    navigationOptions: {
      headerMode: 'none',
    },
  },
  acceptBidSuccess: {
    screen: AcceptBidSuccessScreen,
    navigationOptions: {
      headerMode: 'none',
    },
  },
  askSuccess: {
    screen: AskSuccessScreen,
    navigationOptions: {
      headerMode: 'none',
    },
  },
};

const Tabs = TabNavigator(
  {
    homeStack: {
      screen: StackNavigator(
        {
          homeScreen: {
            screen: HomeScreen,
          },
          ...shoeScreens,
        },
        {
          navigationOptions: ({ navigation }) => ({
            gesturesEnabled: false,
            headerLeft: <ProfileButton navigation={navigation} />,
            headerStyle: styles.header,
            headerTitle: <LogoTitle />,
          }),
        }
      ),
      navigationOptions: {
        tabBarLabel: 'HOME',
        title: 'Home',
      },
    },
    browseStack: {
      screen: StackNavigator(
        {
          browse: {
            screen: BrowseScreen,
          },
          ...shoeScreens,
        },
        {
          navigationOptions: ({ navigation }) => ({
            gesturesEnabled: false,
            headerLeft: <ProfileButton navigation={navigation} />,
            headerStyle: styles.header,
            headerTitle: <LogoTitle />,
          }),
        }
      ),
      navigationOptions: {
        tabBarLabel: 'BROWSE',
        title: 'Browse',
      },
    },
    transactions: {
      screen: StackNavigator(
        {
          transactionStack: {
            screen: ProfileTransactionTabs,
          },
        },
        {
          navigationOptions: ({ navigation }) => ({
            gesturesEnabled: false,
            headerLeft: <ProfileButton navigation={navigation} />,
            headerStyle: styles.header,
            headerTitle: <LogoTitle />,
          }),
        }
      ),
      navigationOptions: {
        tabBarLabel: 'TXNS',
        title: 'TXNS',
      },
    },
    searchStack: {
      screen: StackNavigator(
        {
          searchScreen: {
            screen: SearchScreen,
          },
          ...shoeScreens,
        },
        {
          navigationOptions: ({ navigation }) => ({
            gesturesEnabled: false,
            headerLeft: <ProfileButton navigation={navigation} />,
            headerStyle: styles.header,
            headerTitle: <LogoTitle />,
          }),
        }
      ),
      navigationOptions: {
        tabBarLabel: 'SEARCH',
        title: 'Search',
      },
    },
    // example: {
    //   screen: StackNavigator({
    //     example: {
    //       screen: ExampleScreen
    //     }
    //   }, {
    //     navigationOptions: ({navigation}) => ({
    //       gesturesEnabled: false,
    //       headerLeft: <ProfileButton navigation={navigation}/>,
    //       headerStyle: styles.header,
    //       headerTitle: <LogoTitle />
    //     })
    //   }),
    //   navigationOptions: {
    //     tabBarLabel: "Example",
    //     title: "Example"
    //   }
    // },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let tab = homeImage;
        switch (routeName) {
          case 'homeStack':
            tab = focused ? homeImage : homeAltImage;
            break;
          case 'browseStack':
            tab = focused ? browseImage : browseAltImage;
            break;
          case 'transactions':
            tab = focused ? txnsImage : txnsAltImage;
            break;
          case 'searchStack':
            tab = focused ? searchImage : searchAltImage;
            break;
        }
        return (
          <Image
            displayName="image"
            style={{ width: 24, height: 24 }}
            source={tab}
          />
        );
      },
    }),
    initialRouteName: 'homeStack',
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      labelStyle: styles.labelStyle,
      indicatorStyle: styles.indicatorStyle,
      tabStyle: styles.tabStyle,
      style: styles.bar,
      activeTintColor: RhypeColors.copper,
      inactiveTintColor: RhypeColors.darkBlue,
      // activeBackgroundColor: RhypeColors.copper
    },
  }
);

const Drawer = DrawerNavigator(
  {
    homeTabs: {
      screen: Tabs,
      navigationOptions: {
        title: 'HOME',
      },
    },
    profileSettings: {
      screen: StackNavigator(
        {
          home: {
            screen: ProfileScreen,
          },
        },
        {
          navigationOptions: ({ navigation }) => ({
            gesturesEnabled: false,
            headerLeft: <ProfileButton navigation={navigation} />,
            headerStyle: styles.header,
            headerTitle: <LogoTitle />,
          }),
        }
      ),
      navigationOptions: {
        title: 'PROFILE',
      },
    },
    favorites: {
      screen: StackNavigator(
        {
          home: {
            screen: FavoritesScreen,
          },
          ...shoeScreens,
        },
        {
          navigationOptions: ({ navigation }) => ({
            gesturesEnabled: false,
            headerLeft: <ProfileButton navigation={navigation} />,
            headerStyle: styles.header,
            headerTitle: <LogoTitle />,
          }),
        }
      ),
      navigationOptions: {
        title: 'FAVORITES',
      },
    },
    // followingAndCollection: {
    //   screen: StackNavigator({
    //     home: {
    //       screen: ProfileFollowingAndCollectionTabs
    //     }
    //   }, {
    //     navigationOptions: ({navigation}) => ({
    //       gesturesEnabled: false,
    //       headerLeft: <ProfileButton navigation={navigation}/>,
    //       headerStyle: styles.header,
    //       headerTitle: <LogoTitle/>
    //     })
    //   }),
    //   navigationOptions: {
    //     title: "FOLLOWING/COLLECTION"
    //   }
    // },
    settings: {
      screen: StackNavigator(
        {
          home: {
            screen: SettingsScreen,
          },
        },
        {
          navigationOptions: ({ navigation }) => ({
            gesturesEnabled: false,
            headerLeft: <ProfileButton navigation={navigation} />,
            headerStyle: styles.header,
            headerTitle: <LogoTitle />,
          }),
        }
      ),
      navigationOptions: {
        title: 'SETTINGS',
      },
    },
    support: {
      screen: StackNavigator(
        {
          home: {
            screen: SupportScreen,
          },
        },
        {
          navigationOptions: ({ navigation }) => ({
            gesturesEnabled: false,
            headerLeft: <ProfileButton navigation={navigation} />,
            headerStyle: styles.header,
            headerTitle: <LogoTitle />,
          }),
        }
      ),
      navigationOptions: {
        title: 'SUPPORT',
      },
    },
    logout: {
      screen: StackNavigator(
        {
          home: {
            screen: LogoutScreen,
          },
        },
        {
          navigationOptions: ({ navigation }) => ({
            gesturesEnabled: false,
            headerLeft: <ProfileButton navigation={navigation} />,
            headerStyle: styles.header,
            headerTitle: <LogoTitle />,
          }),
        }
      ),
      navigationOptions: {
        title: 'LOGOUT',
      },
    },
  },
  {
    contentComponent: props => (
      <ScrollView style={{ flex: 1, paddingTop: 40 }}>
        <DrawerItems
          {...props}
          itemStyle={styles.drawerItemStyle}
          labelStyle={styles.drawerLabelStyle}
          onItemPress={navigation => {
            if (navigation.focused === false) {
              const navigateAction = NavigationActions.navigate({
                routeName: navigation.route.routeName,
              });
              props.navigation.dispatch(navigateAction);
            }
          }}
        />
      </ScrollView>
    ),
  }
);

// export const HomeStack = StackNavigator({
//
//   drawer: {
//     screen: Drawer
//   }
// }, {
//   navigationOptions: ({navigation}) => ({
//     gesturesEnabled: false,
//     headerLeft: <ProfileButton navigation={navigation} />,
//     headerStyle: styles.header,
//     headerTitle: <LogoTitle />
//
//   })
//
// })

export const HomeStack = StackNavigator(
  {
    homeScreens: {
      screen: Drawer,
    },
    shippingAddress: {
      screen: Shipping,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon
              name="chevron-left"
              color={RhypeColors.copper}
              size={34}
            />
          </TouchableOpacity>
        ),
        headerStyle: styles.header,
        headerTitle: <LogoTitle />,
      }),
    },
    billingAddress: {
      screen: BillingAddress,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon
              name="chevron-left"
              color={RhypeColors.copper}
              size={34}
            />
          </TouchableOpacity>
        ),
        headerStyle: styles.header,
        headerTitle: <LogoTitle />,
      }),
    },
    paymentDetails: {
      screen: PaymentDetails,
      navigationOptions: ({ navigation }) => ({
        gesturesEnabled: false,
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon
              name="chevron-left"
              color={RhypeColors.copper}
              size={34}
            />
          </TouchableOpacity>
        ),
        headerStyle: styles.header,
        headerTitle: <LogoTitle />,
      }),
    },
  },
  {
    navigationOptions: {
      header: null,
    },
    mode: 'modal',
  }
);

// navigationOptions:  ({navigation}) => ({
//   gesturesEnabled: false,
//   headerLeft: <TouchableOpacity onPress={()=>navigation.goBack('browse')}><Text>BACK</Text> </TouchableOpacity>,
//   headerStyle: styles.header,
//   headerTitle: <LogoTitle />
// })
