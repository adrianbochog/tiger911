/**
 * Created by adrianperez on 30/03/2018.
 */
import { Platform, StyleSheet } from 'react-native';
import { TabNavigator } from 'react-navigation';
import BuyingBidScreen from '../Screens/Profile/Buying/BuyingBidScreen';
import BuyingPendingScreen from '../Screens/Profile/Buying/BuyingPendingScreen';
import BuyingHistoryScreen from '../Screens/Profile/Buying/BuyingHistoryScreen';
import SellingSalesScreen from '../Screens/Profile/Selling/SellingSalesScreen';
import SellingPendingScreen from '../Screens/Profile/Selling/SellingPendingScreen';
import SellingHistoryScreen from '../Screens/Profile/Selling/SellingHistoryScreen';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';
import FollowingScreen from '../Screens/Profile/FollowingScreen';
import CollectionScreen from '../Screens/Profile/CollectionScreen';

const style = StyleSheet.create({
  labelStyle: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 20,
  },
  tabStyle: {
    backgroundColor: RhypeColors.white,
  },
  bar: {
    borderRadius: 1,
    ...Platform.select({
      ios: {
        backgroundColor: RhypeColors.copper,
        borderBottomWidth: 0.5,
      },
      android: {
        backgroundColor: RhypeColors.white,
      },
    }),
  },
  indicatorStyle: {
    backgroundColor: RhypeColors.copper,
    height: 2,
  },
});

const tabBarOptions = {
  labelStyle: style.labelStyle,
  indicatorStyle: style.indicatorStyle,
  activeTintColor: RhypeColors.copper,
  inactiveTintColor: RhypeColors.darkBlue,
  activeBackgroundColor: RhypeColors.white,
  inactiveBackgroundColor: RhypeColors.white,
  // tabStyle: style.tabStyle,
  style: style.bar,
};

const ProfileBuyingTabs = TabNavigator(
  {
    pending: {
      screen: BuyingPendingScreen,
      navigationOptions: {
        tabBarLabel: 'ACTIVE',
      },
    },
    bids: {
      screen: BuyingBidScreen,
      navigationOptions: {
        tabBarLabel: 'ACCEPTED',
      },
    },
    history: {
      screen: BuyingHistoryScreen,
      navigationOptions: {
        tabBarLabel: 'HISTORY',
      },
    },
  },
  {
    tabBarPosition: 'top',
    tabBarOptions,
    swipeEnabled: false,
    animationEnabled: false,
  }
);

const ProfileSellingTabs = TabNavigator(
  {
    pending: {
      screen: SellingPendingScreen,
      navigationOptions: {
        title: 'OPEN',
      },
    },
    sales: {
      screen: SellingSalesScreen,
      navigationOptions: {
        title: 'CLOSED',
      },
    },
    history: {
      screen: SellingHistoryScreen,
      navigationOptions: {
        title: 'HISTORY',
      },
    },
  },
  {
    tabBarPosition: 'top',
    tabBarOptions,
    swipeEnabled: false,
    animationEnabled: false,
  }
);

const ProfileTransactionTabs = TabNavigator(
  {
    buying: {
      screen: ProfileBuyingTabs,
      navigationOptions: {
        tabBarLabel: 'BIDS',
      },
    },
    selling: {
      screen: ProfileSellingTabs,
      navigationOptions: {
        tabBarLabel: 'ASKS',
      },
    },
  },
  {
    tabBarPosition: 'bottom',
    tabBarOptions,
    swipeEnabled: false,
    animationEnabled: false,
  }
);

const ProfileFollowingAndCollectionTabs = TabNavigator(
  {
    following: {
      screen: FollowingScreen,
      navigationOptions: {
        title: 'FOLLOWING',
      },
    },
    collection: {
      screen: CollectionScreen,
      navigationOptions: {
        title: 'COLLECTION',
      },
    },
  },
  {
    tabBarPosition: 'bottom',
    tabBarOptions,
    swipeEnabled: false,
    animationEnabled: false,
  }
);
export {
  ProfileBuyingTabs,
  ProfileSellingTabs,
  ProfileTransactionTabs,
  ProfileFollowingAndCollectionTabs,
};
