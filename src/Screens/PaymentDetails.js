import React, { Component } from 'react';
import { Alert, View } from 'react-native';
import { connect } from 'react-redux';

import PaymentInput from '../Components/PaymentInput';
import * as ProfileService from '../Services/ProfileService';
import { fetchProfileDetails } from '../Actions/actionCreator';

class PaymentDetails extends Component {
  constructor(props) {
    super(props);

    const { bankName, accountName, accountNumber } = props;

    this.state = {
      bankName,
      accountName,
      accountNumber,
    };

    this.updateState = this.updateState.bind(this);
  }

  componentDidMount() {
    this.props.fetchProfileDetails({
      token: this.props.loginData.token,
    });
  }

  componentWillReceiveProps(props) {
    const {
      userDetailsFetched,
      userDetails,
      isFetchingUserDetails,
    } = props.userDetails;

    if (userDetailsFetched && !isFetchingUserDetails) {
      const bankDetails = userDetails.bank_details;
      if (bankDetails) {
        const {
          bank_name: bankName,
          account_name: accountName,
          account_number: accountNumber,
        } = bankDetails;
        this.setState({
          bankName,
          accountName,
          accountNumber,
        });
      }
    }
  }

  updateState(field) {
    return text => {
      const obj = {};
      obj[field] = text;
      this.setState(obj);
    };
  }

  updatePaymentDetails() {
    const { bankName, accountName, accountNumber } = this.state;

    let createOrUpdate = ProfileService.updatePaymentDetails;
    if (
      this.props.userDetails.userDetailsFetched &&
      this.props.userDetails.userDetails.bank_details == null
    ) {
      createOrUpdate = ProfileService.createPaymentDetails;
    }

    createOrUpdate({
      token: this.props.loginData.token,
      bankName,
      accountName,
      accountNumber,
    })
      .then(() => {
        this.props.navigation.pop();
        this.props.fetchProfileDetails({
          token: this.props.loginData.token,
        });
      })
      .catch(err => {
        console.log(err);
        Alert.alert('Error', "Can't update payment details");
      });
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <PaymentInput
          bankName={this.state.bankName}
          accountName={this.state.accountName}
          accountNumber={this.state.accountNumber}
          onChange={this.updateState}
          onClose={() => this.props.navigation.pop()}
          onSubmit={() => this.updatePaymentDetails()}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  userDetails: state.ProfileDataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchProfileDetails: userDetails =>
      dispatch(fetchProfileDetails(userDetails)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentDetails);
