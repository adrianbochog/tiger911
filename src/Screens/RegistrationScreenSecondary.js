/**
 * Created by adrianperez on 16/04/2018.
 */
import React from 'react';
import {
  Picker,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import RhypeTextInput from '../Components/RhypeTextInput';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';
import ValidationComponent from 'react-native-form-validator';
import { connect } from 'react-redux';
import { postRegistrationData } from '../Actions/actionCreator';
import * as RhypeValidations from '../Commons/RhypeValidations';
import GenderSelector from '../Components/GenderSelector';

class RegistrationScreenSecondary extends ValidationComponent {
  state = {
    firstName: '',
    lastName: '',
    contactNumber: '',
    shoeSize: '',
    gender: '',
  };

  _onSubmit() {
    // Call ValidationComponent validate method
    const { primaryData } = this.props.registrationData;
    console.log('validating');
    console.log(this.state);
    this.validate({
      firstName: RhypeValidations.firstName,
      lastName: RhypeValidations.lastName,
      contactNumber: RhypeValidations.contactNumber,
      shoeSize: RhypeValidations.shoeSize,
    });
    if (this.isFormValid()) {
      this.props.postRegistrationData({
        username: primaryData.username,
        email: primaryData.email,
        password: primaryData.password,

        firstName: this.state.firstName,
        lastName: this.state.lastName,
        contactNumber: this.state.contactNumber,
        shoeSize: this.state.shoeSize,
        gender: this.state.gender,
      });
    } else {
      this.forceUpdate();
      console.log(this.getErrorMessages());
    }
  }

  render() {
    return (
      <KeyboardAvoidingView
        style={styles.root}
        behavior={Platform.select({ ios: 'padding', android: null })}
        keyboardVerticalOffset={Platform.select({
          ios: 50,
          android: 0,
        })}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          contentContainerStyle={{ paddingBottom: 20 }}>
          <View style={styles.logoContainer}>
            <Image
              source={require('../../assets/images/logo_dark.png')}
              style={styles.logo}
              resizeMode="contain"
            />
          </View>

          <View style={styles.formContainer}>
            <View style={styles.container}>
              <View style={styles.inputContainer}>
                <RhypeTextInput
                  ref="firstName"
                  placeholder="FIRST NAME"
                  onChangeText={firstName => {
                    this.setState({ firstName });
                  }}
                  onBlur={() => {
                    this.validate({
                      firstName: RhypeValidations.firstName,
                    });
                    this.forceUpdate();
                  }}
                />
                {this.isFieldInError('firstName') &&
                  this.getErrorsInField('firstName').map(
                    errorMessage => (
                      <Text
                        style={styles.errorText}
                        key={errorMessage}>
                        {errorMessage}
                      </Text>
                    )
                  )}

                <RhypeTextInput
                  ref="lastName"
                  placeholder="LAST NAME"
                  onChangeText={lastName => {
                    this.setState({ lastName });
                  }}
                  onBlur={() => {
                    this.validate({
                      lastName: RhypeValidations.lastName,
                    });
                    this.forceUpdate();
                  }}
                />
                {this.isFieldInError('lastName') &&
                  this.getErrorsInField('lastName').map(
                    errorMessage => (
                      <Text
                        style={styles.errorText}
                        key={errorMessage}>
                        {errorMessage}
                      </Text>
                    )
                  )}

                <RhypeTextInput
                  ref="contactNumber"
                  placeholder="CONTACT NUMBER"
                  onChangeText={contactNumber => {
                    this.setState({ contactNumber });
                  }}
                  onBlur={() => {
                    this.validate({
                      contactNumber: RhypeValidations.contactNumber,
                    });
                    this.forceUpdate();
                  }}
                  keyboardType="numeric"
                />
                {this.isFieldInError('contactNumber') &&
                  this.getErrorsInField('contactNumber').map(
                    errorMessage => (
                      <Text
                        style={styles.errorText}
                        key={errorMessage}>
                        {errorMessage}
                      </Text>
                    )
                  )}

                <RhypeTextInput
                  ref="shoeSize"
                  placeholder="SHOE SIZE"
                  onChangeText={shoeSize => {
                    this.setState({ shoeSize }, () => {
                      this.validate({
                        shoeSize: RhypeValidations.shoeSize,
                      });
                      this.forceUpdate();
                    });
                  }}
                  keyboardType="numeric"
                />
                {this.isFieldInError('shoeSize') &&
                  this.getErrorsInField('shoeSize').map(
                    errorMessage => (
                      <Text
                        style={styles.errorText}
                        key={errorMessage}>
                        {errorMessage}
                      </Text>
                    )
                  )}
                <GenderSelector
                  value={this.state.gender}
                  onChange={g => this.setState({ gender: g })}
                />
              </View>
              <TouchableOpacity
                style={styles.button}
                onPress={() => this._onSubmit()}>
                <Text style={styles.buttonText}>SUBMIT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
  },
  inputContainer: {},
  button: {
    backgroundColor: RhypeColors.darkBlue,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 22,
    textAlign: 'center',
    fontFamily: fonts.UniformCondensed.light,
  },
  errorText: {
    color: 'red',
    fontSize: 12,
    fontFamily: fonts.UniformCondensed.light,
  },
  root: {
    flex: 1,
    backgroundColor: RhypeColors.white,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingLeft: 10,
    paddingRight: 10,
  },
  logoContainer: {
    marginTop: 64,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    maxWidth: 180,
    paddingTop: 100,
  },
  formContainer: {},
});

const mapStateToProps = state => ({
  registrationData: state.RegistrationReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    postRegistrationData: data =>
      dispatch(postRegistrationData(data)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationScreenSecondary);
