import React, { Component } from 'react';
import { Alert, View } from 'react-native';
import { connect } from 'react-redux';

import AddressInput from '../Components/AddressInput';
import * as ProfileService from '../Services/ProfileService';
import { fetchProfileDetails } from '../Actions/actionCreator';

class Shipping extends Component {
  constructor(props) {
    super(props);

    const {
      particulars,
      street,
      area,
      city,
      region,
      zipCode,
    } = props;

    this.state = {
      particulars,
      street,
      area,
      city,
      region,
      zipCode,
    };

    this.updateState = this.updateState.bind(this);
  }

  componentDidMount() {
    this.props.fetchProfileDetails({
      token: this.props.loginData.token,
    });
  }

  componentWillReceiveProps(props) {
    const {
      userDetailsFetched,
      userDetails,
      isFetchingUserDetails,
    } = props.userDetails;

    if (userDetailsFetched && !isFetchingUserDetails) {
      // const {
      //   addresses,
      // } = userDetails
      // const shippingAddress = addresses.find(a => a.name === 'SHIPPING')
      const shippingAddress = userDetails.shipping_address;
      if (shippingAddress) {
        const {
          area,
          city,
          particulars,
          region,
          street,
          zip_code: zipCode,
        } = shippingAddress;
        this.setState({
          area,
          city,
          particulars,
          region,
          street,
          zipCode,
        });
      }
    }
  }

  updateState(field) {
    return text => {
      const obj = {};
      obj[field] = text;
      this.setState(obj);
    };
  }

  updateAddress() {
    const {
      particulars,
      street,
      area,
      city,
      region,
      zipCode,
    } = this.state;

    let createOrUpdate = ProfileService.updateShippingAddressDetails;
    if (
      this.props.userDetails.userDetailsFetched &&
      this.props.userDetails.userDetails.shipping_address == null
    ) {
      createOrUpdate = ProfileService.createShippingAddressDetails;
    }

    createOrUpdate({
      token: this.props.loginData.token,
      particulars,
      street,
      area,
      city,
      region,
      zipCode,
    })
      .then(() => {
        this.props.navigation.pop();
        this.props.fetchProfileDetails({
          token: this.props.loginData.token,
        });
      })
      .catch(err => {
        console.log(err);
        Alert.alert('Error', "Can't update address");
      });
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AddressInput
          label="DELIVERY"
          particulars={this.state.particulars}
          street={this.state.street}
          city={this.state.city}
          region={this.state.region}
          zipCode={this.state.zipCode}
          area={this.state.area}
          onChange={this.updateState}
          onClose={() => this.props.navigation.pop()}
          onSubmit={() => this.updateAddress()}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  userDetails: state.ProfileDataReducer,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchProfileDetails: userDetails =>
      dispatch(fetchProfileDetails(userDetails)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Shipping);
