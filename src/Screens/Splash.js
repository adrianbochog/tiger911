import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

import * as RhypeColors from '../Commons/RhypeColors';

import AppNavigation from '../Navigation';

export default class Splash extends React.Component {
  constructor(props) {
    super(props);

    this.state = { go: false };
  }
  componentWillReceiveProps(props) {
    if (props.bootstrapped) {
      if (__DEV__) {
        this.setState({ go: true });
        return;
      }

      setTimeout(() => {
        this.setState({ go: true });
      }, 2000);
    }
  }

  render() {
    if (this.state.go) {
      return <AppNavigation />;
    }

    return (
      <View style={styles.root}>
        <View style={styles.logoContainer}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logo}
            resizeMode="contain"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingLeft: 10,
    paddingRight: 10,
  },
  logoContainer: {
    flex: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    maxWidth: 180,
    paddingTop: 100,
  },
});
