/**
 * Created by adrianperez on 16/04/2018.
 */
import React from 'react';
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import RhypeTextInput from '../Components/RhypeTextInput';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';
import ValidationComponent from 'react-native-form-validator';
import { connect } from 'react-redux';
import {
  postRegistrationData,
  registerSecondary,
} from '../Actions/actionCreator';
import * as RhypeValidations from '../Commons/RhypeValidations';

class RegistrationScreenPrimary extends ValidationComponent {
  state = {
    username: '',
    email: '',
    password: '',
  };

  // register = (data) => {
  //   this.props.postRegistrationData(data)
  // }

  _onSubmit = () => {
    // Call ValidationComponent validate method
    this.validate({
      username: RhypeValidations.username,
      password: RhypeValidations.password,
      email: RhypeValidations.email,
    });
    if (this.isFormValid()) {
      this.props.registerSecondary(this.state);
      this.props.navigation.navigate('registrationSecondary');
    } else {
      this.forceUpdate();
      console.log(this.getErrorMessages());
    }
  };

  _isFormValid = () => {
    return this.isFormValid();
  };

  render() {
    const isFormValid = this._isFormValid();
    return (
      <KeyboardAvoidingView
        style={styles.root}
        behavior={Platform.select({ ios: 'padding', android: null })}
        keyboardVerticalOffset={Platform.select({
          ios: 50,
          android: 0,
        })}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          contentContainerStyle={{ paddingBottom: 20 }}>
          <View style={styles.logoContainer}>
            <Image
              source={require('../../assets/images/logo_dark.png')}
              style={styles.logo}
              resizeMode="contain"
            />
          </View>

          <View style={styles.formContainer}>
            <View style={styles.container}>
              <View style={styles.inputContainer}>
                <RhypeTextInput
                  ref="username"
                  placeholder="USERNAME"
                  autoCapitalize="none"
                  onChangeText={username => {
                    this.setState({ username });
                  }}
                  onBlur={() => {
                    this.validate({
                      username: RhypeValidations.username,
                    });
                    this.forceUpdate();
                  }}
                  value={this.state.username}
                />
                {this.isFieldInError('username') &&
                  this.getErrorsInField('username').map(
                    errorMessage => (
                      <Text
                        style={styles.errorText}
                        key={errorMessage}>
                        {errorMessage}
                      </Text>
                    )
                  )}
                <RhypeTextInput
                  ref="email"
                  placeholder="EMAIL"
                  autoCapitalize="none"
                  onChangeText={email => {
                    this.setState({ email });
                  }}
                  onBlur={() => {
                    this.validate({ email: RhypeValidations.email });
                    this.forceUpdate();
                  }}
                  value={this.state.email}
                />
                {this.isFieldInError('email') &&
                  this.getErrorsInField('email').map(errorMessage => (
                    <Text style={styles.errorText} key={errorMessage}>
                      {errorMessage}
                    </Text>
                  ))}

                <RhypeTextInput
                  ref="password"
                  placeholder="PASSWORD"
                  secureTextEntry
                  autoCapitalize="none"
                  onChangeText={password => {
                    this.setState({ password });
                    this.forceUpdate();
                  }}
                  onBlur={() => {
                    this.validate({
                      password: RhypeValidations.password,
                    });
                    this.forceUpdate();
                  }}
                  value={this.state.password}
                />
                {this.isFieldInError('password') &&
                  this.getErrorsInField('password').map(
                    errorMessage => (
                      <Text
                        style={styles.errorText}
                        key={errorMessage}>
                        {errorMessage}
                      </Text>
                    )
                  )}
              </View>
              {/* <Text> */}
              {/* {this.getErrorMessages()} */}
              {/* </Text> */}
            </View>
          </View>
          <TouchableOpacity
            style={[styles.button]}
            onPress={() => this._onSubmit()}>
            <Text style={styles.buttonText}>NEXT</Text>
          </TouchableOpacity>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
  },
  inputContainer: {},
  button: {
    backgroundColor: RhypeColors.darkBlue,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 22,
    textAlign: 'center',
    fontFamily: fonts.UniformCondensed.light,
  },
  errorText: {
    color: 'red',
    fontSize: 12,
    fontFamily: fonts.UniformCondensed.light,
  },
  root: {
    flex: 1,
    backgroundColor: RhypeColors.white,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingLeft: 10,
    paddingRight: 10,
  },
  logoContainer: {
    marginVertical: 64,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    maxWidth: 180,
    paddingTop: 100,
  },
  formContainer: {
    marginBottom: 80,
  },
});

function mapDispatchToProps(dispatch) {
  return {
    registerSecondary: data => dispatch(registerSecondary(data)),
    postRegistrationData: data =>
      dispatch(postRegistrationData(data)),
  };
}

export default connect(
  null,
  mapDispatchToProps
)(RegistrationScreenPrimary);
