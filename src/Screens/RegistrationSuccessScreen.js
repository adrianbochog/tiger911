import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';

class RegistrationSuccessScreen extends Component {
  navigate = () => {
    const navigateToScreen = NavigationActions.navigate({
      routeName: 'welcome',
    });
    this.props.navigation.dispatch(navigateToScreen);
  };

  render() {
    const { navigation } = this.props;
    console.log(navigation);
    return (
      <View style={styles.container}>
        <Icon
          name="check"
          type="evilicon"
          color={RhypeColors.darkBlue}
          size={200}
        />
        <Text style={styles.messageHeader}>
          Registration SUCCESSFUL
        </Text>
        <Text style={styles.messageBody}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
          do eiusmod tempor incididunt ut labore et dolore magna
          aliqua.
        </Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.popToTop();
            navigation.navigate('login');
          }}>
          <Text style={styles.buttonText}>CONTINUE LOGIN</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  messageHeader: {
    ...fonts.UniformCondensed.bold,
    fontSize: 30,
  },
  messageBody: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.copper,
    padding: 10,
    marginTop: 40,
    borderRadius: 10,
  },
  buttonText: {
    fontFamily: fonts.UniformCondensed.regular,
    fontSize: 24,
    color: RhypeColors.white,
  },
});

export default RegistrationSuccessScreen;
