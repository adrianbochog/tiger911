/**
 * Created by adrianperez on 07/04/2018.
 */
import React, { Component } from 'react';
import BrowseContainer from '../Containers/BrowseContainer';
export default class BrowseScreen extends Component {
  render() {
    return <BrowseContainer navigation={this.props.navigation} />;
  }
}
