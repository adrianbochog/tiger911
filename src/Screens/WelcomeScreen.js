import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { connect } from 'react-redux';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import {
  login,
  register,
  registerPrimary,
  registerSecondary,
  fbLoginSuccesss,
} from '../Actions/actionCreator';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';

class WelcomeScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    const { isLoggedIn, navigation } = this.props;
    if (isLoggedIn) {
      navigation.navigate('mainScreens');
    } else {
      LoginManager.logOut();
    }
  }

  loginWithFb = () => {
    LoginManager.logInWithReadPermissions(['public_profile']).then(
      result => {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            const token = `Bearer facebook ${data.accessToken.toString()}`;
            this.props.fbLoginSuccesss({ token });
          });
        }
      },
      error => {
        console.log('Login fail with error: ' + error);
      }
    );
  };

  render() {
    const { navigation } = this.props;

    return (
      <View style={styles.root}>
        <View style={styles.logoContainer}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logo}
            resizeMode="contain"
          />
        </View>

        <View style={styles.contentContainer}>
          <Text
            style={[styles.textStyles, { color: RhypeColors.copper }]}
            onPress={() =>
              navigation.navigate('registrationPrimary')
            }>
            SIGN UP
          </Text>

          <Text
            style={[styles.textStyles, { color: RhypeColors.white }]}
            onPress={() => navigation.navigate('login')}>
            ALREADY HAVE AN ACCOUNT?
          </Text>

          <TouchableOpacity
            style={styles.button}
            onPress={this.loginWithFb}>
            <Text style={[styles.textStyles, { marginTop: 10 }]}>
              CONNECT WITH FACEBOOK
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingLeft: 10,
    paddingRight: 10,
  },
  logoContainer: {
    flex: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentContainer: {
    flex: 3,
  },
  button: {
    flex: 1,
    backgroundColor: RhypeColors.white,
    maxHeight: 50,
    marginBottom: 20,
  },
  logo: {
    maxWidth: 180,
    paddingTop: 100,
  },
  textStyles: {
    flex: 1,
    fontSize: 22,
    fontFamily: fonts.UniformCondensed.light,
    textAlign: 'center',
  },
  touchableStyles: {
    marginTop: 15,
    paddingVertical: 10,
  },
});

function mapStateToProps(state) {
  return {
    isLoggedIn: state.LoginReducer.isLoggedIn,
  };
}

const mapDispatchToProps = {
  login,
  registerPrimary,
  fbLoginSuccesss,
};

const Welcome = connect(
  mapStateToProps,
  mapDispatchToProps
)(WelcomeScreen);

export default Welcome;
