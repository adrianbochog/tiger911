/**
 * Created by adrianperez on 03/04/2018.
 */
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import SellingHistoryContainer from '../../../Containers/Profile/Selling/SellingHistoryContainer';

export default class SellingHistoryScreen extends Component {
  render() {
    return <SellingHistoryContainer />;
  }
}
