/**
 * Created by adrianperez on 03/04/2018.
 */
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import SellingSalesContainer from '../../../Containers/Profile/Selling/SellingSalesContainer';

export default class SellingSalesScreen extends Component {
  render() {
    return <SellingSalesContainer />;
  }
}
