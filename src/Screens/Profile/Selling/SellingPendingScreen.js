/**
 * Created by adrianperez on 03/04/2018.
 */
import React, { Component } from 'react';
import SellingPendingContainer from '../../../Containers/Profile/Selling/SellingPendingContainer';

export default class SellingPendingScreen extends Component {
  render() {
    return <SellingPendingContainer />;
  }
}
