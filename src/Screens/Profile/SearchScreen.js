/**
 * Created by adrianperez on 08/05/2018.
 */
import React, { Component } from 'react';
import SearchContainer from '../../Containers/SearchContainer';

export default class SearchScreen extends Component {
  render() {
    return <SearchContainer {...this.props} />;
  }
}
