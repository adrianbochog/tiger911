/**
 * Created by adrianperez on 30/03/2018.
 */
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import BuyingPendingContainer from '../../../Containers/Profile/Buying/BuyingPendingContainer';

export default class BuyingPendingScreen extends Component {
  render() {
    return <BuyingPendingContainer />;
  }
}
