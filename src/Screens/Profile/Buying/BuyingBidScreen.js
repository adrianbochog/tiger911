/**
 * Created by adrianperez on 30/03/2018.
 */
import React, { Component } from 'react';
import BuyingBidContainer from '../../../Containers/Profile/Buying/BuyingBidContainer';

export default class BuyingBidScreen extends Component {
  render() {
    const props = this.props;
    return <BuyingBidContainer {...props} />;
  }
}
