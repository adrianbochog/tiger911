/**
 * Created by adrianperez on 30/03/2018.
 */
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import BuyingHistoryContainer from '../../../Containers/Profile/Buying/BuyingHistoryContainer';

export default class BuyingHistoryScreen extends Component {
  render() {
    return <BuyingHistoryContainer />;
  }
}
