/**
 * Created by adrianperez on 30/03/2018.
 */

import React, { Component } from 'react';
import ProfileContainer from '../../Containers/Profile/ProfileContainer';

export default class ProfileScreen extends Component {
  render() {
    return <ProfileContainer navigation={this.props.navigation} />;
  }
}
