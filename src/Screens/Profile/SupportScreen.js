/**
 * Created by adrianperez on 03/04/2018.
 */
import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import * as RhypeColors from '../../Commons/RhypeColors';

class SupportScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
          Please contact our Rhype staff for any inquiries regarding
          your purchase.
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    alignContent: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  text: {
    justifyContent: 'center',
    alignSelf: 'center',
    paddingVertical: 20,
    paddingHorizontal: 20,
    color: RhypeColors.darkBlue,
  },
});

export default SupportScreen;
