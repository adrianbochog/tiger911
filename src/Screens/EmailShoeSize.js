import React from 'react';
import {
  Picker,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import ValidationComponent from 'react-native-form-validator';

import RhypeTextInput from '../Components/RhypeTextInput';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';
import * as RhypeValidations from '../Commons/RhypeValidations';
import * as ProfileService from '../Services/ProfileService';
import { sendLoginDataSuccess } from '../Actions/actionCreator';

class EmailShoeSize extends ValidationComponent {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      shoeSize: '',
    };
  }

  submit = () => {
    const token = this.props.navigation.state.params.token;
    ProfileService.updateAccount({
      shoe_size: this.state.shoeSize,
      user: {
        email: this.state.email,
      },
      token,
    }).then(profile => {
      if (profile.shoe_size && profile.user && profile.user.email) {
        this.props.sendLoginDataSuccess({ token });
      }
    });
  };

  render() {
    return (
      <KeyboardAvoidingView
        style={styles.root}
        behavior={Platform.select({ ios: 'padding', android: null })}
        keyboardVerticalOffset={Platform.select({
          ios: 50,
          android: 0,
        })}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          contentContainerStyle={{ paddingBottom: 20 }}>
          <View style={styles.logoContainer}>
            <Image
              source={require('../../assets/images/logo_dark.png')}
              style={styles.logo}
              resizeMode="contain"
            />
          </View>

          <View style={styles.formContainer}>
            <View style={styles.container}>
              <View style={styles.inputContainer}>
                <RhypeTextInput
                  ref="email"
                  placeholder="EMAIL"
                  autoCapitalize="none"
                  onChangeText={email => {
                    this.setState({ email });
                  }}
                  onBlur={() => {
                    this.validate({ email: RhypeValidations.email });
                    this.forceUpdate();
                  }}
                  value={this.state.email}
                />
                {this.isFieldInError('email') &&
                  this.getErrorsInField('email').map(errorMessage => (
                    <Text style={styles.errorText} key={errorMessage}>
                      {errorMessage}
                    </Text>
                  ))}
                <RhypeTextInput
                  ref="shoeSize"
                  placeholder="SHOE SIZE"
                  onChangeText={shoeSize => {
                    this.setState({ shoeSize }, () => {
                      this.validate({
                        shoeSize: RhypeValidations.shoeSize,
                      });
                      this.forceUpdate();
                    });
                  }}
                  keyboardType="numeric"
                />
                {this.isFieldInError('shoeSize') &&
                  this.getErrorsInField('shoeSize').map(
                    errorMessage => (
                      <Text
                        style={styles.errorText}
                        key={errorMessage}>
                        {errorMessage}
                      </Text>
                    )
                  )}
              </View>
              <TouchableOpacity
                style={styles.button}
                onPress={this.submit}>
                <Text style={styles.buttonText}>SUBMIT</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
  },
  inputContainer: {},
  button: {
    backgroundColor: RhypeColors.darkBlue,
    height: 50,
    marginTop: 64,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 22,
    textAlign: 'center',
    fontFamily: fonts.UniformCondensed.light,
  },
  errorText: {
    color: 'red',
    fontSize: 12,
    fontFamily: fonts.UniformCondensed.light,
  },
  root: {
    flex: 1,
    backgroundColor: RhypeColors.white,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingLeft: 10,
    paddingRight: 10,
  },
  logoContainer: {
    marginTop: 64,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    maxWidth: 180,
    paddingTop: 100,
  },
  formContainer: {},
});

const mapDispatchToProps = {
  sendLoginDataSuccess,
};

export default connect(null, mapDispatchToProps)(EmailShoeSize);
