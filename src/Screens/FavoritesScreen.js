import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect } from 'react-redux';

import ShoeCard from '../Components/Shoe/ShoeCard';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';
import { fetchFavorites } from '../Actions/actionCreator';

class BrowseContainer extends Component {
  componentDidMount() {
    this.props.fetchFavorites(this.props.loginData.token);
  }

  render() {
    const {
      favorites: { data, isFetching },
    } = this.props;
    if (data) {
      return (
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.headerText}>FAVORITES</Text>
          </View>
          <FlatList
            columnWrapperStyle={{ justifyContent: 'space-evenly' }}
            numColumns={3}
            data={data}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('shoeDetail', {
                    itemId: item.id,
                  })
                }>
                <ShoeCard
                  brand={item.product.brand.name}
                  modelType={item.product.name}
                  colorVersion={item.color}
                  priceAmount={item.retail_price}
                  imageUri={item.images.image}
                  soldAmount={item.retail_price}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => item.id}
            onRefresh={() =>
              this.props.fetchFavorites(this.props.loginData.token)
            }
            refreshing={isFetching}
          />
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'white',
          }}>
          <ActivityIndicator
            size="large"
            color={RhypeColors.darkBlue}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
    paddingTop: 20,
  },
  header: {
    flexDirection: 'row',
    paddingHorizontal: 5,
    justifyContent: 'space-between',
  },
  headerText: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 36,
    color: RhypeColors.darkBlue,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
  favorites: state.ProfileDataReducer.favorites,
});

function mapDispatchToProps(dispatch) {
  return {
    fetchFavorites: token => dispatch(fetchFavorites(token)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BrowseContainer);
