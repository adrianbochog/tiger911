/**
 * Created by jeunesseburce on 09/03/2018.
 */
import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';

import { Icon } from 'react-native-elements';

import ProductDetailContainer from '../../Containers/ProductDetailContainer';
import * as RhypeColors from '../../Commons/RhypeColors';

export default class ShoeDetailScreen extends Component {
  // change naming later
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerRight: params.onFavorite && (
        <TouchableOpacity
          onPress={() => {
            params.onFavorite();
          }}
          style={{ marginRight: 8 }}>
          <Icon
            name={params.isFavorite ? 'favorite' : 'favorite-border'}
            color={RhypeColors.copper}
            size={34}
          />
        </TouchableOpacity>
      ),
    };
  };

  render() {
    return (
      <View style={{ backgroundColor: 'white', flex: 1 }}>
        <ProductDetailContainer {...this.props} />
      </View>
    );
  }
}
