/**
 * Created by adrianperez on 25/04/2018.
 */
import React, { Component } from 'react';
import ShoeAskConfirmationContainer from '../../Containers/Shoe/ShoeAskConfirmationContainer';

export default class ShoeAskConfirmationScreen extends Component {
  render() {
    return <ShoeAskConfirmationContainer {...this.props} />;
  }
}
