/**
 * Created by adrianperez on 18/04/2018.
 */
import React, { Component } from 'react';
import ShoeBidConfirmationContainer from '../../Containers/Shoe/ShoeBidConfirmationContainer';

export default class ShoeBidConfirmationScreen extends Component {
  render() {
    return <ShoeBidConfirmationContainer {...this.props} />;
  }
}
