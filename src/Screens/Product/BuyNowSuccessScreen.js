/**
 * Created by adrianperez on 25/04/2018.
 */
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import * as RhypeColors from '../../Commons/RhypeColors';
import fonts from '../../Commons/RhypeFonts';
import { NavigationActions } from 'react-navigation';

export default class BidSuccessScreen extends Component {
  navigate = () => {
    const navigateToScreen = NavigationActions.navigate({
      routeName: 'browse',
    });
    this.props.navigation.dispatch(navigateToScreen);
  };

  render() {
    return (
      <View style={styles.container}>
        <Icon
          name="check"
          type="evilicon"
          color={RhypeColors.darkBlue}
          size={200}
        />
        <Text style={styles.messageHeader}>BUY NOW SUCCESSFUL</Text>
        <Text style={styles.messageBody}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
          do eiusmod tempor incididunt ut labore et dolore magna
          aliqua.
        </Text>
        <TouchableOpacity
          style={styles.button}
          onPress={this.navigate}>
          <Text style={styles.buttonText}>CONTINUE BROWSING</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  messageHeader: {
    ...fonts.UniformCondensed.bold,
    fontSize: 30,
  },
  messageBody: {
    fontFamily: fonts.UniformCondensed.light,
    fontSize: 18,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: RhypeColors.copper,
    padding: 10,
    marginTop: 40,
    borderRadius: 10,
  },
  buttonText: {
    fontFamily: fonts.UniformCondensed.regular,
    fontSize: 24,
    color: 'white',
  },
});
