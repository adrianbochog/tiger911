/**
 * Created by adrianperez on 17/04/2018.
 */
import React, { Component } from 'react';
import ProductBidContainer from '../../Containers/ProductBidContainer';

export default class ProductBidScreen extends Component {
  render() {
    return <ProductBidContainer />;
  }
}
