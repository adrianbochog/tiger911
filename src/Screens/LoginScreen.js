import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { postLoginData } from '../Actions/actionCreator';
import * as RhypeColors from '../Commons/RhypeColors';
import fonts from '../Commons/RhypeFonts';
import RhypeTextInput from '../Components/RhypeTextInput';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  onSubmit() {
    this.props.postLoginData(this.state);
  }

  _renderErrorMessage() {
    if (this.props.loginData.isError) {
      const errorMessages = this.props.loginData.errorMessages;
      const errorMessage =
        errorMessages.length > 0 ? errorMessages[0] : '';
      return (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>{errorMessage}</Text>
        </View>
      );
    } else {
      return <View />;
    }
  }

  _renderButtonContent() {
    if (this.props.loginData.isSending) {
      return (
        <ActivityIndicator
          size="large"
          color={RhypeColors.darkBlue}
        />
      );
    } else {
      return <Text style={styles.textStyles}>SIGN IN</Text>;
    }
  }

  render() {
    const errorView = this._renderErrorMessage();
    const buttonContent = this._renderButtonContent();
    return (
      <KeyboardAvoidingView
        style={styles.root}
        behavior={Platform.select({ ios: 'padding', android: null })}
        keyboardVerticalOffset={Platform.select({
          ios: 50,
          android: -300,
        })}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          contentContainerStyle={{ paddingBottom: 20 }}>
          <View style={styles.logoContainer}>
            <Image
              source={require('../../assets/images/logo.png')}
              style={styles.logo}
              resizeMode="contain"
            />
          </View>
          <View style={styles.contentContainer}>
            {errorView}
            <View style={styles.inputContainer}>
              <RhypeTextInput
                placeholder="USERNAME"
                isOnDark
                onChangeText={username => this.setState({ username })}
                value={this.state.username}
                autoCapitalize="none"
              />
              <RhypeTextInput
                placeholder="PASSWORD"
                isOnDark
                secureTextEntry
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
                autoCapitalize="none"
              />
            </View>

            <TouchableOpacity
              style={styles.button}
              disabled={this.props.isSending}
              onPress={() => this.onSubmit()}>
              {buttonContent}
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: RhypeColors.darkBlue,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingLeft: 10,
    paddingRight: 10,
  },
  logoContainer: {
    marginVertical: 64,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentContainer: {
    justifyContent: 'space-around',
  },
  inputContainer: {
    justifyContent: 'flex-start',
    marginBottom: 80,
  },
  button: {
    height: 50,
    backgroundColor: RhypeColors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    maxWidth: 180,
    paddingTop: 100,
  },
  textStyles: {
    fontSize: 20,
    fontFamily: fonts.UniformCondensed.light,
    textAlign: 'center',
  },
  errorContainer: {
    backgroundColor: 'pink',
    alignItems: 'center',
    justifyContent: 'center',
  },

  errorText: {
    color: 'red',
    fontSize: 14,
    fontFamily: fonts.UniformCondensed.light,
  },
});

const mapStateToProps = state => ({
  loginData: state.LoginReducer,
});

const mapDispatchToProps = dispatch => ({
  postLoginData: data => dispatch(postLoginData(data)),
});
// function mapDispatchToProps(dispatch) {
//   return {
//     postRegistrationData: (data) => dispatch(postRegistrationData(data))
//   }
// }
const Login = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);

export default Login;
