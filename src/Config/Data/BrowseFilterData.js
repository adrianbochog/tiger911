/**
 * Created by adrianperez on 21/04/2018.
 */

export const yearData = [
  2000,
  2001,
  2002,
  2003,
  2004,
  2005,
  2006,
  2007,
  2008,
  2009,
  2010,
  2011,
  2012,
  2013,
  2014,
  2015,
  2016,
  2017,
  2018,
];

export const shoeSizeData = [];
for (let x = 3; x < 18; x = x + 0.5) {
  shoeSizeData.push(x);
}

export const brandAndModelData = [
  {
    brandName: 'adidas',
    models: ['Ez Yeezy', '361 Oz', 'Pringle Z', 'Hakuna Matata'],
  },
  {
    brandName: 'New Balance',
    models: ['247 Sport', '123 Seven', '456 Haduk'],
  },
  { brandName: 'Nike', models: ['Hamog', 'Air', 'Vapor'] },
];
