const numeral = require('numeral');

const currencyFormatter = currency => {
  if (currency) {
    const formatCurrency = numeral(currency).format('0,0.00');
    return formatCurrency;
  } else {
    return '0.00';
  }
};

export { currencyFormatter };
