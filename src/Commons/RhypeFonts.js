import { Platform } from 'react-native';

const fonts = {
  UniformCondensed: {
    regular: Platform.select({
      android: 'UniformCondensed-Regular',
      ios: 'Uniform Condensed',
    }),
    bold: Platform.select({
      android: {
        fontFamily: 'UniformCondensed-Bold',
      },
      ios: {
        fontFamily: 'Uniform Condensed',
        fontWeight: 'bold',
      },
    }),
    light: Platform.select({
      android: 'UniformCondensedLight-Regular',
      ios: 'Uniform Condensed Light',
    }),
  },
};

export default fonts;
