/**
 * Created by adrianperez on 14/04/2018.
 */

const brandMapping = {
  1: 'adidas',
  2: 'New Balance',
  3: 'Nike',
};

export function getBrand(id) {
  return brandMapping[id];
}
