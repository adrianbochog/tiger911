export const firstName = {
  minlength: 2,
  maxlength: 50,
  required: true,
};
export const lastName = {
  minlength: 1,
  maxlength: 50,
  required: true,
};
export const username = {
  minlength: 5,
  maxlength: 50,
  required: true,
};
export const password = {
  minlength: 5,
  maxlength: 50,
  required: true,
};
export const email = { email: true, required: true };
export const contactNumber = {
  numbers: true,
  minlength: 10,
  maxlength: 12,
  required: true,
};
export const shoeSize = {
  numbers: true,
  minLength: 1,
  maxlength: 4,
  required: true,
};
export const gender = { required: true };
