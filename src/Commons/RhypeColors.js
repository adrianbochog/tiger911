/**
 * Created by adrianperez on 14/03/2018.
 */

const darkBlue = 'rgba(13,22,40,1)';
const copper = 'rgba(169,132,19,1)';
const gray = 'rgba(215,215,215,1)';
const white = '#FFFFFF';
const black = '#000000';
const mediumgray = '#f6f6f6';
const red = '#E73536';

export { darkBlue, copper, gray, white, black, mediumgray, red };
