import axios from 'axios/index';

// This will fetch all variants
export const fetchAllVariantsData = variantsData => {
  const data = variantsData || {};
  return new Promise((resolve, reject) => {
    axios
      .request({
        method: 'get',
        url: '/variants/',
        baseURL: 'http://106.187.42.59:80/api/v2',
        params: {
          shoe_size: data.shoeSize,
          condition: 'BRAND_NEW',
        },
      })
      .then(response => {
        // console.log(JSON.stringify(response.data))
        return resolve(response.data.results);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

// This will fetch variants by Id
export const fetchVariantsDataById = variantsData => {
  const data = variantsData || {};
  return new Promise((resolve, reject) => {
    fetch(
      'http://106.187.42.59:80/api/v2/variants/' + data.id + '/',
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
      }
    )
      .then(response => {
        return response.json();
      })
      .then(responseJson => {
        // console.log("variant detail response: " + JSON.stringify(responseJson))
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

// This will fetch variants by Name
export const fetchVariantsDataByName = variantsData => {
  const data = variantsData || {};
  return new Promise((resolve, reject) => {
    fetch(
      'http://106.187.42.59:80/api/v2/variants/?name=' + data.name,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          // 'Authorization': 'Token ' + data.token
        },
      }
    )
      .then(response => {
        // console.log('dasdas:' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('variants data response: ' + responseJson)
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

// This will fetch variants by Color
export const fetchVariantsDataByColor = variantsData => {
  const data = variantsData || {};
  return new Promise((resolve, reject) => {
    fetch(
      'http://106.187.42.59:80/api/v2/variants/?color=' + data.color,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
      }
    )
      .then(response => {
        // console.log("dasdas:" + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('variants data response: ' + responseJson)
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

// This will fetch variants by Size
export const fetchVariantsDataBySize = variantsData => {
  const data = variantsData || {};
  return new Promise((resolve, reject) => {
    fetch(
      'http://106.187.42.59:80/api/v2/variants/?size=' + data.size,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
      }
    )
      .then(response => {
        // console.log("dasdas:" + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        console.log('variants data response: ' + responseJson);
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

// This will fetch variants by Shoe Size
export const fetchVariantsDataByShoeSize = variantsData => {
  const data = variantsData || {};
  return new Promise((resolve, reject) => {
    fetch(
      'http://106.187.42.59:80/api/v2/variants/?shoe_size=' +
        data.shoeSize +
        '&condition=BRAND_NEW',
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          // 'Authorization': 'Token ' + data.token
        },
      }
    )
      .then(response => {
        // console.log('dasdas:' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('variants data response: ' + responseJson)
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

// This will fetch variants by Movement
export const fetchVariantsDataByMovement = variantsData => {
  const data = variantsData || {};
  return new Promise((resolve, reject) => {
    fetch(
      'http://106.187.42.59:80/api/v2/variants/?movement=' +
        data.movement,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          // 'Authorization': 'Token ' + data.token
        },
      }
    )
      .then(response => {
        console.log('dasdas:' + JSON.stringify(response));
        return response.json();
      })
      .then(responseJson => {
        console.log('variants data response: ' + responseJson);
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

// This will fetch variants by Condition
export const fetchVariantsDataByCondition = variantsData => {
  const data = variantsData || {};
  return new Promise((resolve, reject) => {
    fetch(
      'http://106.187.42.59:80/api/v2/variants/?condition=' +
        data.condition,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          // 'Authorization': 'Token ' + data.token
        },
      }
    )
      .then(response => {
        console.log('dasdas:' + JSON.stringify(response));
        return response.json();
      })
      .then(responseJson => {
        console.log('variants data response: ' + responseJson);
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export function fetchVariantsDataByQuery(d) {
  const data = d || {};
  const { search, shoeSize, token } = data;
  return new Promise((resolve, reject) => {
    let ok = false;
    axios
      .request({
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
        url: '/variants/',
        baseURL: 'http://106.187.42.59:80/api/v2',
        params: {
          search,
          shoe_size: shoeSize,
          condition: 'BRAND_NEW',
        },
      })
      .then(response => {
        if (response.status === 200) ok = true;
        return response.data;
      })
      .then(json => {
        if (ok) resolve(json.results);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
}
