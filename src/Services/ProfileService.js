export const postRegistrationData = userData => {
  const data = userData || {};
  console.log('registration data', data);
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch('http://106.187.42.59/api/v2/profile/create/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user: {
          username: data.username,
          password: data.password,
          first_name: data.firstName,
          last_name: data.lastName,
          email: data.email,
        },
        gender: data.gender,
        shoe_size: data.shoeSize,
        contact_number: data.contactNumber,
      }),
    })
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const postLoginData = userData => {
  const data = userData || {};
  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59:80/api/v2/login/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: data.username,
        password: data.password,
      }),
    })
      .then(response => {
        console.log('response: ' + JSON.stringify(response));
        return response.json();
      })
      .then(responseJson => {
        console.log('responseJson' + JSON.stringify(responseJson));
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const fetchProfileData = userData => {
  const data = userData || {};
  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59:80/api/v2/profile/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
    })
      .then(response => {
        // console.log('dasdas:' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('profle data response: ' + responseJson)
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const fetchProfileDetails = userDetails => {
  const data = userDetails || {};
  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59:80/api/v2/profile/details/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
    })
      .then(response => {
        // console.log('dasdas:' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('profle data response: ', responseJson)
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const fetchProfileAddresses = userData => {
  const data = userData || {};
  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59:80/api/v2/profile/addresses/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
    })
      .then(response => {
        // console.log('dasdas:' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('profle data response: ' + responseJson)
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const postProfileAddresses = userData => {
  const data = userData || {};
  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59:80/api/v2/profile/addresses/add/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
      body: JSON.stringify({
        name: data.address_data.name,
        street: data.address_data.street,
        area: data.address_data.area,
        city: data.address_data.city,
        region: data.address_data.region,
        zip_code: data.address_data.zip_code,
        is_default: data.address_data.is_default,
      }),
    })
      .then(response => {
        // console.log('response: ' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('responseJson' + JSON.stringify(responseJson))
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const putProfileAddresses = userData => {
  const data = userData || {};
  return new Promise((resolve, reject) => {
    fetch(
      'http://106.187.42.59:80/api/v2/profile/addresses/1/edit/',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
        body: JSON.stringify({
          name: data.address_data.name,
          street: data.address_data.street,
          area: data.address_data.area,
          city: data.address_data.city,
          region: data.address_data.region,
          zip_code: data.address_data.zip_code,
        }),
      }
    )
      .then(response => {
        // console.log('response: ' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('responseJson' + JSON.stringify(responseJson))
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const postPaymentData = paymentData => {
  const data = paymentData || {};
  return new Promise((resolve, reject) => {
    fetch(
      'http://106.187.42.59:80/api/v2/profile/bank-details/add/',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
        body: JSON.stringify({
          account_name: data.payment_data.account_name,
          account_number: data.payment_data.account_number,
          bank_name: data.payment_data.bank_name,
        }),
      }
    )
      .then(response => {
        // console.log('response: ' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('responseJson' + JSON.stringify(responseJson))
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const putPaymentData = paymentData => {
  const data = paymentData || {};
  return new Promise((resolve, reject) => {
    fetch(
      'http://106.187.42.59:80/api/v2/profile/bank-details/edit/',
      {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
        body: JSON.stringify({
          account_name: data.payment_data.account_name,
          account_number: data.payment_data.account_number,
          bank_name: data.payment_data.bank_name,
        }),
      }
    )
      .then(response => {
        // console.log('response: ' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('responseJson' + JSON.stringify(responseJson))
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const fetchPaymentData = paymentData => {
  const data = paymentData || {};
  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59:80/api/v2/profile/bank-details/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
    })
      .then(response => {
        // console.log('response: ' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('responseJson' + JSON.stringify(responseJson))
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const updateAccountDetails = account => {
  const data = account || {};
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch('http://106.187.42.59/api/v2/profile/update/', {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
      body: JSON.stringify({
        user: {
          first_name: data.firstName,
          last_name: data.lastName,
          email: data.email,
        },
        contact_number: data.contactNumber,
      }),
    })
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const fetchFavorites = token => {
  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59/api/v2/profile/favorites/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => {
        // console.log('response: ' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        // console.log('responseJson' + JSON.stringify(responseJson))
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const putFavorites = (favorites, token) => {
  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59/api/v2/profile/favorites/add/', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({
        favorites,
      }),
    })
      .then(response => {
        console.log(response);
        // console.log('response: ' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        console.log(responseJson);
        // console.log('responseJson' + JSON.stringify(responseJson))
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const removeFavorites = (favorites, token) => {
  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59/api/v2/profile/favorites/remove/', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({
        favorites,
      }),
    })
      .then(response => {
        console.log(response);
        // console.log('response: ' + JSON.stringify(response))
        return response.json();
      })
      .then(responseJson => {
        console.log(responseJson);
        // console.log('responseJson' + JSON.stringify(responseJson))
        return resolve(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const updateBillingAddressDetails = account => {
  const data = account || {};
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch(
      'http://106.187.42.59/api/v2/profile/addresses/billing/edit/',
      {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
        body: JSON.stringify({
          particulars: data.particulars,
          street: data.street,
          area: data.area,
          city: data.city,
          region: data.region,
          zip_code: data.zipCode,
        }),
      }
    )
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const updateShippingAddressDetails = account => {
  const data = account || {};
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch(
      'http://106.187.42.59/api/v2/profile/addresses/shipping/edit/',
      {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
        body: JSON.stringify({
          particulars: data.particulars,
          street: data.street,
          area: data.area,
          city: data.city,
          region: data.region,
          zip_code: data.zipCode,
        }),
      }
    )
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const createBillingAddressDetails = account => {
  const data = account || {};
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch(
      'http://106.187.42.59/api/v2/profile/addresses/billing/add/',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
        body: JSON.stringify({
          particulars: data.particulars,
          street: data.street,
          area: data.area,
          city: data.city,
          region: data.region,
          zip_code: data.zipCode,
        }),
      }
    )
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const createShippingAddressDetails = account => {
  const data = account || {};
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch(
      'http://106.187.42.59/api/v2/profile/addresses/shipping/add/',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
        body: JSON.stringify({
          particulars: data.particulars,
          street: data.street,
          area: data.area,
          city: data.city,
          region: data.region,
          zip_code: data.zipCode,
        }),
      }
    )
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const updatePaymentDetails = account => {
  const data = account || {};
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch('http://106.187.42.59/api/v2/profile/bank-details/edit/', {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
      body: JSON.stringify({
        bank_name: data.bankName,
        account_name: data.accountName,
        account_number: data.accountNumber,
      }),
    })
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const createPaymentDetails = account => {
  const data = account || {};
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch('http://106.187.42.59/api/v2/profile/bank-details/add/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
      body: JSON.stringify({
        bank_name: data.bankName,
        account_name: data.accountName,
        account_number: data.accountNumber,
      }),
    })
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const updateAccount = account => {
  const data = account || {};
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch('http://106.187.42.59/api/v2/profile/update/', {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
      body: JSON.stringify(data),
    })
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const fetchVerifyParams = token =>
  fetch('http://106.187.42.59/api/v2/profile/verify/params/', {
    headers: {
      'Content-Type': 'application/json',
      Authorization: token,
    },
  })
    .then(r => r.json())
    .catch(console.log);
