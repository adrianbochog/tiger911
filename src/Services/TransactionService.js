/**
 * Created by jeunesseburce on 04/08/2018.
 */

import axios from 'axios';

export const postBidData = bidData => {
  const data = bidData || {};
  // console.log('bid data:' + JSON.stringify(bidData))
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch('http://106.187.42.59/api/v2/bidding-price/create/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
      body: JSON.stringify({
        price: data.price,
        variant: data.variantId,
        days_expiry: data.duration,
      }),
    })
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const postBuyNowData = bidData => {
  const data = bidData || {};
  // console.log('bid data:' + JSON.stringify(bidData))
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch('http://106.187.42.59/api/v2/order-transaction/buy-now/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: bidData.token,
      },
      body: JSON.stringify({
        variant: data.variant,
      }),
    })
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const postAskData = askData => {
  const data = askData || {};
  // console.log('ask data:' + JSON.stringify(askData))
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch('http://106.187.42.59/api/v2/asking-price/create/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: data.token,
      },
      body: JSON.stringify({
        price: data.price,
        variant: data.variantId,
        days_expiry: data.duration,
      }),
    })
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const postAcceptBidData = askData => {
  const data = askData || {};
  console.log('ask data:' + JSON.stringify(askData));
  return new Promise((resolve, reject) => {
    let ok = false;
    fetch(
      'http://106.187.42.59/api/v2/order-transaction/accept-bid/',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: data.token,
        },
        body: JSON.stringify({
          bid: data.bid,
          ask: data.ask,
        }),
      }
    )
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const fetchAskData = askData => {
  const data = askData || {};
  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59/api/v2/asking-price/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        // 'Authorization': data.token
      },
    })
      .then(response => {
        // console.log('response: ' + JSON.stringify(response))
        return resolve(response.json());
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const fetchRecentAskData = askData => {
  const data = askData || {};
  return new Promise((resolve, reject) => {
    axios
      .request({
        method: 'get',
        url: '/asking-price/',
        baseURL: 'http://106.187.42.59/api/v2',
        params: {
          condition: 'BRAND_NEW',
          shoe_size: data.shoeSize,
        },
      })
      .then(response => {
        // console.log(JSON.stringify(response.data))
        return resolve(response.data);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const fetchUserAskData = askData => {
  const data = askData || {};
  // console.log('product bid data params:' + JSON.stringify(askData))
  return new Promise((resolve, reject) => {
    axios
      .request({
        method: 'get',
        url: '/asking-price/',
        baseURL: 'http://106.187.42.59/api/v2',
        headers: {
          Authorization: data.token,
        },
        params: {
          role: 's',
          status: askData.status,
          order_by: '-created',
        },
      })
      .then(response => {
        // console.log(JSON.stringify(response.data))
        return resolve(response.data.asking_prices);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const fetchBidData = bidData => {
  const data = bidData || {};
  // console.log("fetch bid data", data)
  return new Promise((resolve, reject) => {
    axios
      .request({
        method: 'get',
        url: '/bidding-price/',
        baseURL: 'http://106.187.42.59/api/v2',
        params: {
          condition: 'BRAND_NEW',
          shoe_size: data.shoeSize,
          id: data.productId,
        },
      })
      .then(response => {
        // console.log(JSON.stringify(response.data))
        return resolve(response.data);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const fetchUserBidData = bidData => {
  const data = bidData || {};
  return new Promise((resolve, reject) => {
    axios
      .request({
        method: 'get',
        url: '/bidding-price/',
        baseURL: 'http://106.187.42.59/api/v2',
        headers: {
          Authorization: data.token,
        },
        params: {
          role: 'b',
          // 'variant': data.id,
          status: data.status,
          order_by: '-created',
        },
      })
      .then(response => {
        // console.log(JSON.stringify(response.data))
        return resolve(response.data.bidding_prices);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const fetchUserMatchingBidData = bidData => {
  const data = bidData || {};
  // console.log('product bid data params:' + JSON.stringify(bidData))
  return new Promise((resolve, reject) => {
    axios
      .request({
        method: 'get',
        url: '/bidding-price/',
        baseURL: 'http://106.187.42.59/api/v2',
        headers: {
          Authorization: data.token,
        },
        params: {
          role: 's',
          variant: data.id,
          status: 'Active',
        },
      })
      .then(response => {
        return resolve(response.data.bidding_prices);
      })
      .catch(error => {
        console.error(error);
      });
  });
};

export const fetchTransactionConfig = () =>
  fetch('http://106.187.42.59/api/v2/config/')
    .then(r => r.json())
    .catch(console.log);

export const fetchPaymentDetails = payment => {
  const { paymentId, token } = payment || {};

  if (
    paymentId == null ||
    paymentId < 0 ||
    token == null ||
    token == ''
  ) {
    console.error('Invalid params: ', payment);
    return;
  }

  return new Promise((resolve, reject) => {
    fetch('http://106.187.42.59/api/v2/' + paymentId + '/params', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => {
        if (response.ok) ok = true;
        return response.json();
      })
      .then(json => {
        if (ok) resolve(json);
        else reject(json);
      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
  });
};

export const postPayment = async payment => {
  const { paymentId, paymentOption, token } = payment || {};

  if (
    paymentId == null ||
    paymentId < 0 ||
    paymentOption == null ||
    paymentOption == '' ||
    token == null ||
    token == ''
  ) {
    console.error('Invalid params: ', payment);
    return;
  }

  // const {
  //   payment_url: paymentURL,
  //   params,
  // } = await fetchPaymentDetails(payment);

  // return {
  //   uri: paymentURL,
  //   method: 'POST',
  //   headers: {
  //     'Content-Type': 'application/json',
  //   },
  //   body: JSON.stringify(params),
  // };
  //
  return {
    uri: `http://106.187.42.59/payments/${paymentId}/two_checkout/`,
  };
};
