import { applyMiddleware, combineReducers, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import thunk from 'redux-thunk';
import storage from 'redux-persist/es/storage';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';

import nav from './src/Reducers/navigationReducer';
import loginReducer from './src/Reducers/loginReducer';
import BrowseDataReducer from './src/Reducers/browseDataReducer';
import ShoeDataReducer from './src/Reducers/shoeDataReducer';
import RegistrationReducer from './src/Reducers/registrationReducer';
import BrowseDataFilterReducer from './src/Reducers/browseDataFilterReducer';
import AskDataReducer from './src/Reducers/askDataReducer';
import PendingAskDataReducer from './src/Reducers/pendingAskDataReducer';
import AskHistoryDataReducer from './src/Reducers/askHistoryDataReducer';

import BidDataReducer from './src/Reducers/bidDataReducer';
import UserBidDataReducer from './src/Reducers/userBidDataReducer';
import BidHistoryDataReducer from './src/Reducers/bidHistoryDataReducer';
import ProductBidDataReducer from './src/Reducers/productBidDataReducer';
import ProfileDataReducer from './src/Reducers/profileDataReducer';
import BuyNowReducer from './src/Reducers/buyNowReducer';
import AcceptBidReducer from './src/Reducers/acceptBidReducer';
import ShoeSizeDataReducer from './src/Reducers/shoeSizeDataReducer';
import PaymentDataReducer from './src/Reducers/paymentDataReducer';

const config = {
  key: 'root',
  storage,
};

const config1 = {
  key: 'primary',
  storage,
};

// This will persist all the reducers, but I don't want to persist navigation state, so instead will use persistReducer.
// const rootReducer = persistCombineReducers(config, reducer)

const LoginReducer = persistReducer(config1, loginReducer);

const rootReducer = combineReducers({
  nav,
  LoginReducer,
  BrowseDataReducer,
  ShoeDataReducer,
  RegistrationReducer,
  BrowseDataFilterReducer,
  AskDataReducer,
  PendingAskDataReducer,
  AskHistoryDataReducer,
  BidDataReducer,
  UserBidDataReducer,
  BidHistoryDataReducer,
  ProductBidDataReducer,
  ProfileDataReducer,
  BuyNowReducer,
  AcceptBidReducer,
  ShoeSizeDataReducer,
  PaymentDataReducer,
});

const navMiddleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav
);

function configureStore() {
  const store = createStore(
    rootReducer,
    applyMiddleware(thunk, navMiddleware)
  );
  const persistor = persistStore(store);
  return { persistor, store };
}

export default configureStore;
